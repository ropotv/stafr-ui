#!/usr/bin/env bash

export SERVER_IP="164.90.202.55"
export DOCKERFILE="Dockerfile.production"
export DOCKERHUB_TAG="prod-ui"
export DOCKERHUB_REPOSITORY="ropotv/stafr"
export ENV_FILES="./env/production"

source deploy/util_build.sh
source deploy/util_deploy.sh

