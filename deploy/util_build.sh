#!/usr/bin/env bash

DOCKERFILE="$DOCKERFILE"
DOCKERHUB_PASSWORD="$DOCKERHUB_PASSWORD"
DOCKERHUB_USERNAME="$DOCKERHUB_USERNAME"
DOCKERHUB_TAG="$DOCKERHUB_TAG"
DOCKERHUB_REPOSITORY="$DOCKERHUB_REPOSITORY"

TAR_FILE="$DOCKERHUB_TAG.tar"

docker build . -f "$DOCKERFILE" -t "$DOCKERHUB_TAG"
docker save "$DOCKERHUB_TAG" --output "$TAR_FILE"

echo "$DOCKERHUB_PASSWORD" | docker login --username "$DOCKERHUB_USERNAME" --password-stdin
docker load --input "$TAR_FILE"
docker tag "$DOCKERHUB_TAG" "$DOCKERHUB_REPOSITORY:$DOCKERHUB_TAG"
docker push "$DOCKERHUB_REPOSITORY:$DOCKERHUB_TAG"

rm "$TAR_FILE"
