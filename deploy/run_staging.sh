#!/usr/bin/env bash

export SERVER_IP="164.92.222.89"
export DOCKERFILE="Dockerfile.staging"
export DOCKERHUB_TAG="staging-ui"
export DOCKERHUB_REPOSITORY="ropotv/stafr"
export ENV_FILES="./env/staging"

source deploy/util_build.sh
source deploy/util_deploy.sh


