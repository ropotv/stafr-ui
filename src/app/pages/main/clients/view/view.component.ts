import {
  ChangeDetectionStrategy,
  Component,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

import { BrowserUtils, IClient } from '@stafr/core';
import { ClientsService } from '@stafr/api';
import { AlertService, SliderComponent, ToastService } from '@stafr/modules';
import { AppEvents } from '@stafr/app.events';

@Component({
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ViewClientComponent {
  public client$: Observable<IClient>;

  @ViewChild(SliderComponent)
  private _slider: SliderComponent;

  constructor(
    route: ActivatedRoute,
    private _clientSvc: ClientsService,
    private _browserUtils: BrowserUtils,
    private _toast: ToastService,
    private _translate: TranslateService,
    private _alert: AlertService,
    private _events: AppEvents
  ) {
    this.client$ = route.paramMap.pipe(
      map((p) => Number(p.get('number'))),
      switchMap((number) => this._clientSvc.getOne(number))
    );
  }

  public onContactCopy(contact: string): void {
    this._browserUtils.copyToClipboard(contact);
  }

  public onContactEmail(contact: string): void {
    this._browserUtils.openEmail(contact);
  }

  public onContactUrl(contact: string): void {
    this._browserUtils.openInNewTab(contact);
  }

  public async onClientDelete(client: IClient): Promise<void> {
    this._alert.open({
      caption: this._translate.instant('clients.view.delete.alert.caption'),
      message: this._translate.instant('clients.view.delete.alert.message'),
      buttons: [
        {
          type: 'gray-outlined',
          text: this._translate.instant('clients.view.delete.alert.btn-no'),
          clicked: () => this._alert.close(),
        },
        {
          type: 'warn',
          text: this._translate.instant('clients.view.delete.alert.btn-yes'),
          clicked: async () => {
            await this._clientSvc.delete(client.number).toPromise();
            this._slider.close();
            this._alert.close();
            this._events.publish('clients');
            this._toast.info(
              this._translate.instant('common.success'),
              this._translate.instant('clients.view.delete.success', {
                name: client.name,
              })
            );
          },
        },
      ],
      closed: () => {},
    });
  }
}
