import {
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';
import { Router } from '@angular/router';

import { ITableRowClick, TableSource } from '@stafr/modules';
import { IClient } from '@stafr/core';
import { ClientsService } from '@stafr/api';

@Component({
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClientsComponent {
  public source$: TableSource<IClient>;

  constructor(private _service: ClientsService, private _router: Router) {
    this.source$ = (p) => this._service.getMany(p);
  }

  public onRowClick(event: ITableRowClick<IClient>): void {
    this._router.navigate(['clients', event.data.number]);
  }
}
