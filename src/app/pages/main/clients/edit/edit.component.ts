import {
  ChangeDetectionStrategy,
  Component,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { ClientsService } from '@stafr/api';
import { IClientForm } from '@stafr/sections';
import { IDialog, ToastService } from '@stafr/modules';
import { AppEvents } from '@stafr/app.events';

@Component({
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditClientComponent {
  public form$: Observable<FormGroup>;

  @ViewChild('dialog')
  private _dialog: IDialog;

  constructor(
    private _service: ClientsService,
    private _toast: ToastService,
    private _translate: TranslateService,
    private _events: AppEvents,
    route: ActivatedRoute,
    fb: FormBuilder
  ) {
    this.form$ = route.parent.paramMap.pipe(
      map((p) => Number(p.get('number'))),
      switchMap((number) => this._service.getOne(number)),
      map((c) =>
        fb.group({
          number: c.number,
          name: c.name,
          contact: c.contact,
          company: c.company,
        })
      )
    );
  }

  public async onSubmit(data: IClientForm): Promise<void> {
    await this._service
      .update(data.number, {
        name: data.name,
        company: data.company,
        contact: data.contact,
      })
      .toPromise();

    this._events.publish('clients', 'client');
    this._toast.success(
      this._translate.instant('common.success'),
      this._translate.instant('clients.edit.success', { name: data.name })
    );
    this._dialog.close();
  }
}
