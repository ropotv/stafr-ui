import {
  ChangeDetectionStrategy,
  Component,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { IClientForm } from '@stafr/sections';
import { ClientsService } from '@stafr/api';
import { AppEvents } from '@stafr/app.events';
import { FormSection, IDialog, ToastService } from '@stafr/modules';

@Component({
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateClientComponent {
  public form: FormGroup;
  public pageForm: FormGroup;

  @ViewChild('dialog')
  private _dialog: IDialog;

  @ViewChild('formComponent')
  private _clientForm: FormSection<IClientForm>;

  constructor(
    private _service: ClientsService,
    private _toast: ToastService,
    private _translate: TranslateService,
    private _events: AppEvents,
    fb: FormBuilder
  ) {
    this.pageForm = fb.group({ createAnother: false });
    this.form = fb.group({
      number: null,
      name: '',
      contact: '',
      company: '',
    });
  }

  public async onSubmit(data: IClientForm): Promise<void> {
    const { createAnother } = this.pageForm.getRawValue();

    await this._service
      .create({
        name: data.name,
        company: data.company,
        contact: data.contact,
      })
      .toPromise();

    this._events.publish('clients');
    this._toast.success(
      this._translate.instant('common.success'),
      this._translate.instant('clients.create.success', { name: data.name })
    );

    if (createAnother) {
      this._clientForm.unLock();
      this._clientForm.reset();
      this._clientForm.markAsSubmitted(false);
    } else {
      this._dialog.close();
    }
  }
}
