import { Routes } from '@angular/router';
import { RbacGuard } from '@stafr/modules';
import { AccountRole, UserRole } from '@stafr/core';

import { ClientsComponent } from './clients.component';
import { CreateClientComponent } from './create';
import { ViewClientComponent } from './view';
import { EditClientComponent } from './edit';

export const ClientsRoutes: Routes = [
  {
    path: '',
    component: ClientsComponent,
    canActivate: [RbacGuard],
    data: {
      userRoles: [UserRole.Account],
      accountRoles: [AccountRole.Admin, AccountRole.Manager],
    },
    children: [
      {
        path: 'create',
        component: CreateClientComponent,
      },
      {
        path: ':number',
        component: ViewClientComponent,
        children: [
          {
            path: 'edit',
            component: EditClientComponent,
          },
        ],
      },
    ],
  },
];
