import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import {
  ButtonModule,
  CardModule,
  ContractUtilsModule,
  CurrencyUtilsModule,
  DateUtilsModule,
  DialogModule,
  SliderModule,
  StafrFormsModule,
  StatusBoxModule,
  StringUtilsModule,
  TableModule,
  TimeUtilsModule,
} from '@stafr/modules';
import { ClientFormModule } from '@stafr/sections';

import { ClientsComponent } from './clients.component';
import { ClientsRoutes } from './clients.routes';
import { CreateClientComponent } from './create';
import { ViewClientComponent } from './view';
import { EditClientComponent } from './edit';

@NgModule({
  imports: [
    RouterModule.forChild(ClientsRoutes),
    CommonModule,
    CardModule,
    TableModule,
    ButtonModule,
    DialogModule,
    ClientFormModule,
    StafrFormsModule,
    StatusBoxModule,
    ContractUtilsModule,
    CurrencyUtilsModule,
    SliderModule,
    DateUtilsModule,
    TimeUtilsModule,
    TranslateModule,
    StringUtilsModule,
  ],
  declarations: [
    ClientsComponent,
    CreateClientComponent,
    ViewClientComponent,
    EditClientComponent,
  ],
})
export class ClientsModule {}
