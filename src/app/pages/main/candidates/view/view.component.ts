import {
  ChangeDetectionStrategy,
  Component,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { merge, Observable, Subject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { map, switchMap, tap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

import {
  AccountRole,
  BrowserUtils,
  CandidateStatus,
  ICandidate,
  ICandidateAttachment,
  ICandidateComment,
  IJwtPrincipal,
} from '@stafr/core';
import {
  AlertService,
  extensionUtil,
  SliderComponent,
  StatusBoxColors,
  TextBoxComponent,
  ToastService,
} from '@stafr/modules';
import {
  CandidatesAttachmentsProvider,
  CandidatesCommentsProvider,
  CandidatesService,
} from '@stafr/api';
import { AppStore } from '@stafr/app.store';
import { AppEvents } from '@stafr/app.events';

@Component({
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ViewCandidateComponent {
  public AccountRole = AccountRole;
  public colors = StatusBoxColors;

  public principal$: Observable<IJwtPrincipal>;
  public candidate$: Observable<ICandidate>;
  public canEdit: boolean;
  private _onCandidate$ = new Subject<ICandidate>();

  @ViewChild(SliderComponent)
  private _slider: SliderComponent;

  constructor(
    private _candidateAttachments: CandidatesAttachmentsProvider,
    private _candidateSvc: CandidatesService,
    private _candidateComment: CandidatesCommentsProvider,
    private _browserUtils: BrowserUtils,
    private _route: ActivatedRoute,
    private _store: AppStore,
    private _toast: ToastService,
    private _translate: TranslateService,
    private _alert: AlertService,
    private _events: AppEvents
  ) {
    this.principal$ = this._store.state$.pipe(map((s) => s.principal));
    this.candidate$ = merge(
      this._onCandidate$,
      this._route.paramMap.pipe(
        map((p) => Number(p.get('number'))),
        switchMap((number) => this._candidateSvc.getOne(number))
      )
    ).pipe(
      tap((candidate) => {
        const { principal } = this._store.get;
        const isHR = principal.accountRole === AccountRole.HR;
        const isNotAuthor = principal.accountId !== candidate.author?.id;
        const isWorking = candidate.status === CandidateStatus.Employee;
        this.canEdit = !(isHR && isNotAuthor) && !isWorking;
      })
    );
  }

  public async onAttachmentDownload(
    candidate: ICandidate,
    attachment: ICandidateAttachment
  ): Promise<void> {
    const res = await this._candidateAttachments
      .getOne(candidate.number, attachment.id)
      .toPromise();
    const name = attachment.originalName;
    const extension = extensionUtil(name);
    this._browserUtils.download(name, extension, res.data);
  }

  public async onCommentSubmit(
    textBox: TextBoxComponent,
    candidate: ICandidate
  ): Promise<void> {
    if (!textBox.value) {
      return;
    }

    const comment = await this._candidateComment
      .create(candidate.number, { comment: textBox.value })
      .toPromise();
    candidate.comments = [comment, ...candidate.comments];
    textBox.value = null;
    this._onCandidate$.next(candidate);
  }

  public onContactCopy(contact: string): void {
    this._browserUtils.copyToClipboard(contact);
  }

  public onContactEmail(contact: string): void {
    this._browserUtils.openEmail(contact);
  }

  public onContactUrl(contact: string): void {
    this._browserUtils.openInNewTab(contact);
  }

  public async onCandidateDelete(candidate: ICandidate): Promise<void> {
    this._alert.open({
      caption: this._translate.instant('candidates.view.delete.alert.caption'),
      message: this._translate.instant('candidates.view.delete.alert.message'),
      buttons: [
        {
          type: 'gray-outlined',
          text: this._translate.instant('candidates.view.delete.alert.btn-no'),
          clicked: () => this._alert.close(),
        },
        {
          type: 'warn',
          text: this._translate.instant('candidates.view.delete.alert.btn-yes'),
          clicked: async () => {
            await this._candidateSvc.delete(candidate.number).toPromise();
            this._slider.close();
            this._alert.close();
            this._events.publish('candidates');
            this._toast.info(
              this._translate.instant('common.success'),
              this._translate.instant('candidates.view.delete.success', {
                name: candidate.name,
              })
            );
          },
        },
      ],
      closed: () => {},
    });
  }

  public async onCommentDelete(
    candidate: ICandidate,
    comment: ICandidateComment
  ): Promise<void> {
    this._alert.open({
      caption: this._translate.instant(
        'candidates.view.comments.delete.alert.caption'
      ),
      message: this._translate.instant(
        'candidates.view.comments.delete.alert.message'
      ),
      buttons: [
        {
          type: 'gray-outlined',
          text: this._translate.instant(
            'candidates.view.comments.delete.alert.btn-no'
          ),
          clicked: () => this._alert.close(),
        },
        {
          type: 'warn',
          text: this._translate.instant(
            'candidates.view.comments.delete.alert.btn-yes'
          ),
          clicked: async () => {
            await this._candidateComment
              .delete(candidate.number, comment.id)
              .toPromise();
            this._onCandidate$.next({
              ...candidate,
              comments: candidate.comments.filter((c) => c.id !== comment.id),
            });
            this._alert.close();
            this._toast.info(
              this._translate.instant('common.success'),
              this._translate.instant(
                'candidates.view.comments.delete.success',
                { name: candidate.name }
              )
            );
          },
        },
      ],
      closed: () => {},
    });
  }
}
