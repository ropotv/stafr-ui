import {
  ChangeDetectionStrategy,
  Component,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';

import { ICandidateForm } from '@stafr/sections';
import {
  AccountService,
  CandidatesAttachmentsProvider,
  CandidatesService,
} from '@stafr/api';
import { AppEvents } from '@stafr/app.events';
import { FormSection, IDialog, ToastService } from '@stafr/modules';
import { AppStore } from '@stafr/app.store';
import { CandidateStatus, CountryType, findFrequent } from '@stafr/core';

@Component({
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateCandidateComponent {
  public form$: Observable<FormGroup>;
  public pageForm: FormGroup;

  @ViewChild('dialog')
  private _dialog: IDialog;

  @ViewChild('formComponent')
  private _candidateForm: FormSection<ICandidateForm>;

  constructor(
    private _candidateAttachments: CandidatesAttachmentsProvider,
    private _service: CandidatesService,
    private _toast: ToastService,
    private _translate: TranslateService,
    private _store: AppStore,
    private _account: AccountService,
    private _events: AppEvents,
    fb: FormBuilder
  ) {
    this.pageForm = fb.group({ createAnother: false });
    this.form$ = this._store.state$.pipe(
      map((s) => {
        const frequent = findFrequent(s.settings.preferredCountries);
        const enums = Object.values(CountryType);
        const top = frequent.filter((c) => enums.some((e) => e === c));

        return fb.group({
          number: null,
          name: '',
          status: CandidateStatus.Unknown,
          assigneeId: s.principal.accountId,
          contact: '',
          country: top.first(),
          skills: fb.array([]),
          profession: '',
          experience: null,
          rate: { amount: 0, currency: this._store.currency },
          attachments: {
            files: [],
            attachments: [],
            prevAttachments: [],
          },
          skill: '',
        });
      })
    );
  }

  public async onSubmit(data: ICandidateForm): Promise<void> {
    const { createAnother } = this.pageForm.getRawValue();

    const candidate = await this._service
      .create({
        name: data.name,
        assigneeId: data.assigneeId,
        contact: data.contact,
        country: data.country,
        status: data.status,
        profession: data.profession,
        experience: data.experience,
        rateAmount: data.rate.amount?.toString(),
        rateCurrency: data.rate.currency,
        skills: data.skills,
      })
      .toPromise();

    /** Upload attachments **/ {
      if (data.attachments.files?.length) {
        await this._candidateAttachments
          .create(candidate.number, data.attachments.files)
          .toPromise();
      }
    }

    /** Update account preferred country **/ {
      await this._account
        .updateSettings({ preferredCountry: data.country })
        .toPromise();
    }

    this._events.publish('candidates');
    this._toast.success(
      this._translate.instant('common.success'),
      this._translate.instant('candidates.create.success', { name: data.name })
    );

    if (createAnother) {
      const prev = this._candidateForm.formValue;
      this._candidateForm.unLock();
      this._candidateForm.reset();
      this._candidateForm.markAsSubmitted(false);
      this._candidateForm.form.patchValue({
        assigneeId: prev.assigneeId,
        country: prev.country,
        status: prev.status,
      });
      const skills: FormArray = this._candidateForm.form.get('skills') as any;
      skills.clear();
    } else {
      this._dialog.close();
    }
  }
}
