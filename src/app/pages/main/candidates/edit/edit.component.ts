import {
  ChangeDetectionStrategy,
  Component,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import {
  AccountService,
  CandidatesAttachmentsProvider,
  CandidatesService,
} from '@stafr/api';
import { ICandidateForm } from '@stafr/sections';
import { IDialog, SlugifyUtilsService, ToastService } from '@stafr/modules';
import { AppEvents } from '@stafr/app.events';

@Component({
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditCandidateComponent {
  public form$: Observable<FormGroup>;

  @ViewChild('dialog')
  private _dialog: IDialog;

  constructor(
    private _candidateAttachments: CandidatesAttachmentsProvider,
    private _service: CandidatesService,
    private _toast: ToastService,
    private _translate: TranslateService,
    private _account: AccountService,
    private _events: AppEvents,
    route: ActivatedRoute,
    slugify: SlugifyUtilsService,
    fb: FormBuilder
  ) {
    this.form$ = route.parent.paramMap.pipe(
      map((p) => Number(p.get('number'))),
      switchMap((number) => this._service.getOne(number)),
      map((c) =>
        fb.group({
          number: c.number,
          name: c.name,
          status: c.status,
          assigneeId: c.assignee.id,
          contact: c.contact,
          country: c.country,
          skills: fb.array(c.skills),
          profession: c.profession,
          experience: c.experience,
          rate: { amount: Number(c.rateAmount), currency: c.rateCurrency },
          attachments: {
            files: [],
            attachments: c.attachments,
            prevAttachments: c.attachments,
          },
          skill: '',
        })
      )
    );
  }

  public async onSubmit(data: ICandidateForm): Promise<void> {
    await this._service
      .update(data.number, {
        name: data.name,
        assigneeId: data.assigneeId,
        contact: data.contact,
        country: data.country,
        status: data.status,
        profession: data.profession,
        experience: data.experience,
        rateAmount: data.rate.amount?.toString(),
        rateCurrency: data.rate.currency,
        skills: data.skills,
      })
      .toPromise();

    /** Upload attachments **/ {
      if (data.attachments.files?.length) {
        await this._candidateAttachments
          .create(data.number, data.attachments.files)
          .toPromise();
      }
    }

    /** Delete attachments **/ {
      const ids = data.attachments.attachments.map((a) => a.id);
      for (const attachment of data.attachments.prevAttachments) {
        if (!ids.includes(attachment.id)) {
          await this._candidateAttachments
            .delete(data.number, attachment.id)
            .toPromise();
        }
      }
    }

    /** Update account preferred country **/ {
      await this._account
        .updateSettings({ preferredCountry: data.country })
        .toPromise();
    }

    this._events.publish('candidates', 'candidate');
    this._toast.success(
      this._translate.instant('common.success'),
      this._translate.instant('candidates.edit.success', { name: data.name })
    );
    this._dialog.close();
  }
}
