import { Routes } from '@angular/router';

import { RbacGuard } from '@stafr/modules';
import { AccountRole, UserRole } from '@stafr/core';

import { CandidatesComponent } from './candidates.component';
import { ViewCandidateComponent } from './view';
import { CreateCandidateComponent } from './create';
import { EditCandidateComponent } from './edit';

export const CandidatesRoutes: Routes = [
  {
    path: '',
    component: CandidatesComponent,
    canActivate: [RbacGuard],
    data: {
      userRoles: [UserRole.Account],
      accountRoles: [AccountRole.Admin, AccountRole.Manager, AccountRole.HR],
    },
    children: [
      {
        path: 'create',
        component: CreateCandidateComponent,
      },
      {
        path: ':number',
        component: ViewCandidateComponent,
        children: [
          {
            path: 'edit',
            component: EditCandidateComponent,
          },
        ],
      },
    ],
  },
];
