import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import {
  AvatarModule,
  ButtonModule,
  CardModule,
  ContractUtilsModule,
  CurrencyUtilsModule,
  DateUtilsModule,
  DialogModule,
  ExtensionUtilsModule,
  MapyModule,
  SliderModule,
  SlugifyUtilsModule,
  StafrFormsModule,
  StatusBoxModule,
  StringUtilsModule,
  TableModule,
  TimeUtilsModule,
} from '@stafr/modules';
import { CandidateFormModule } from '@stafr/sections';

import { CandidatesRoutes } from './candidates.routes';
import { CandidatesComponent } from './candidates.component';
import { ViewCandidateComponent } from './view';
import { CreateCandidateComponent } from './create';
import { EditCandidateComponent } from './edit';

@NgModule({
  imports: [
    RouterModule.forChild(CandidatesRoutes),
    CommonModule,
    TableModule,
    AvatarModule,
    CardModule,
    StatusBoxModule,
    ButtonModule,
    SliderModule,
    CurrencyUtilsModule,
    ContractUtilsModule,
    ExtensionUtilsModule,
    SlugifyUtilsModule,
    StafrFormsModule,
    DateUtilsModule,
    TranslateModule,
    DialogModule,
    CandidateFormModule,
    TimeUtilsModule,
    StringUtilsModule,
    MapyModule,
  ],
  declarations: [
    CandidatesComponent,
    ViewCandidateComponent,
    CreateCandidateComponent,
    EditCandidateComponent,
  ],
})
export class CandidatesModule {}
