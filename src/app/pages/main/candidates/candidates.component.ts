import {
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { KeyValue } from '@angular/common';
import { map } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

import {
  ITableRowClick,
  TableSource,
  TranslateEnums,
  TranslateUtilsService,
} from '@stafr/modules';
import { CandidateStatus, ICandidate } from '@stafr/core';
import { CandidatesService } from '@stafr/api';

@Component({
  templateUrl: './candidates.component.html',
  styleUrls: ['./candidates.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CandidatesComponent {
  public statuses$: Observable<KeyValue<CandidateStatus, string>[]> =
    this._translateUtils.change$.pipe(
      map(() => [
        {
          value: CandidateStatus.Unavailable,
          key: this._translateSvc.instant(
            `enum.${CandidateStatus.Unavailable}`
          ),
        },
        {
          value: CandidateStatus.Unknown,
          key: this._translateSvc.instant(`enum.${CandidateStatus.Unknown}`),
        },
        {
          value: CandidateStatus.Interested,
          key: this._translateSvc.instant(`enum.${CandidateStatus.Interested}`),
        },
        {
          value: CandidateStatus.InReview,
          key: this._translateSvc.instant(`enum.${CandidateStatus.InReview}`),
        },
        {
          value: CandidateStatus.Employee,
          key: this._translateSvc.instant(`enum.${CandidateStatus.Employee}`),
        },
      ])
    );
  public countries$ = this._translateEnums.countries$;
  public source$: TableSource<ICandidate>;

  constructor(
    private _service: CandidatesService,
    private _router: Router,
    private _translateUtils: TranslateUtilsService,
    private _translateSvc: TranslateService,
    private _translateEnums: TranslateEnums
  ) {
    this.source$ = (p) => this._service.getMany(p);
  }

  public onRowClick(event: ITableRowClick<ICandidate>): void {
    this._router.navigate(['candidates', event.data.number]);
  }
}
