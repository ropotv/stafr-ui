import {
  ChangeDetectionStrategy,
  Component,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { TeamService } from '@stafr/api';
import { ITeamMemberForm } from '@stafr/sections';
import { IDialog, ToastService } from '@stafr/modules';
import { AppEvents } from '@stafr/app.events';

@Component({
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditTeamMemberComponent {
  public form$: Observable<FormGroup>;

  @ViewChild('dialog')
  private _dialog: IDialog;

  constructor(
    private _service: TeamService,
    private _toast: ToastService,
    private _translate: TranslateService,
    private _events: AppEvents,
    route: ActivatedRoute,
    fb: FormBuilder
  ) {
    this.form$ = route.parent.paramMap.pipe(
      map((p) => Number(p.get('id'))),
      switchMap((id) => this._service.getOne(id)),
      map((a) =>
        fb.group({
          id: a.id,
          fullName: a.fullName,
          role: a.role,
        })
      )
    );
  }

  public async onSubmit(data: ITeamMemberForm): Promise<void> {
    await this._service
      .update(
        {
          fullName: data.fullName,
          role: data.role,
        },
        data.id
      )
      .toPromise();

    this._events.publish('team', 'team-member');
    this._toast.success(
      this._translate.instant('common.success'),
      this._translate.instant('team.edit.success', { name: data.fullName })
    );
    this._dialog.close();
  }
}
