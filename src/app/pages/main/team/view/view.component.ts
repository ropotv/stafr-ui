import {
  ChangeDetectionStrategy,
  Component,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

import { IAccount, IJwtPrincipal } from '@stafr/core';
import {
  AlertService,
  SliderComponent,
  StatusBoxColors,
  ToastService,
} from '@stafr/modules';
import { TeamService } from '@stafr/api';
import { AppStore } from '@stafr/app.store';
import { AppEvents } from '@stafr/app.events';

@Component({
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ViewTeamMemberComponent {
  public colors = StatusBoxColors;
  public member$: Observable<IAccount>;
  public principal$: Observable<IJwtPrincipal>;

  @ViewChild(SliderComponent)
  private _slider: SliderComponent;

  constructor(
    private _teamSvc: TeamService,
    private _toast: ToastService,
    private _translate: TranslateService,
    private _alert: AlertService,
    private _events: AppEvents,
    route: ActivatedRoute,
    store: AppStore
  ) {
    this.member$ = route.paramMap.pipe(
      map((p) => Number(p.get('id'))),
      switchMap((id) => this._teamSvc.getOne(id))
    );
    this.principal$ = store.state$.pipe(map((s) => s.principal));
  }

  public async onDelete(member: IAccount): Promise<void> {
    this._alert.open({
      caption: this._translate.instant('team.view.delete.alert.caption'),
      message: this._translate.instant('team.view.delete.alert.message'),
      buttons: [
        {
          type: 'gray-outlined',
          text: this._translate.instant('team.view.delete.alert.btn-no'),
          clicked: () => this._alert.close(),
        },
        {
          type: 'warn',
          text: this._translate.instant('team.view.delete.alert.btn-yes'),
          clicked: async () => {
            await this._teamSvc.delete(member.id).toPromise();
            this._slider.close();
            this._alert.close();
            this._events.publish('team');
            this._toast.success(
              this._translate.instant('common.success'),
              this._translate.instant('team.view.delete.success', {
                name: member.fullName,
              })
            );
          },
        },
      ],
      closed: () => {},
    });
  }

  public async onBlock(member: IAccount): Promise<void> {
    this._alert.open({
      caption: this._translate.instant('team.view.block.alert.caption'),
      message: this._translate.instant('team.view.block.alert.message'),
      buttons: [
        {
          type: 'gray-outlined',
          text: this._translate.instant('team.view.block.alert.btn-no'),
          clicked: () => this._alert.close(),
        },
        {
          type: 'warn',
          text: this._translate.instant('team.view.block.alert.btn-yes'),
          clicked: async () => {
            await this._teamSvc.block(member.id).toPromise();
            this._alert.close();
            this._events.publish('team-member', 'team');
            this._toast.info(
              this._translate.instant('common.success'),
              this._translate.instant('team.view.block.success', {
                name: member.fullName,
              })
            );
          },
        },
      ],
      closed: () => {},
    });
  }

  public async onUnBlock(member: IAccount): Promise<void> {
    this._alert.open({
      caption: this._translate.instant('team.view.unblock.alert.caption'),
      message: this._translate.instant('team.view.unblock.alert.message'),
      buttons: [
        {
          type: 'gray-outlined',
          text: this._translate.instant('team.view.unblock.alert.btn-no'),
          clicked: () => this._alert.close(),
        },
        {
          type: 'warn',
          text: this._translate.instant('team.view.unblock.alert.btn-yes'),
          clicked: async () => {
            await this._teamSvc.unBlock(member.id).toPromise();
            this._alert.close();
            this._events.publish('team-member', 'team');
            this._toast.info(
              this._translate.instant('common.success'),
              this._translate.instant('team.view.unblock.success', {
                name: member.fullName,
              })
            );
          },
        },
      ],
      closed: () => {},
    });
  }
}
