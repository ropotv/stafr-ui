import {
  ChangeDetectionStrategy,
  Component,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { IInviteForm } from '@stafr/sections';
import { AppEvents } from '@stafr/app.events';
import { IDialog, ToastService } from '@stafr/modules';
import { InvitationService } from '@stafr/api';
import { AccountRole } from '@stafr/core';

@Component({
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateInvitationComponent {
  public form: FormGroup;

  constructor(
    private _service: InvitationService,
    private _toast: ToastService,
    private _translate: TranslateService,
    private _events: AppEvents,
    fb: FormBuilder
  ) {
    this.form = fb.group({
      email: '',
      role: AccountRole.HR,
    });
  }

  @ViewChild('dialog')
  private _dialog: IDialog;

  public async onSubmit(data: IInviteForm): Promise<void> {
    await this._service
      .create({
        email: data.email,
        role: data.role,
      })
      .toPromise();

    this._events.publish('invitations');
    this._toast.success(
      this._translate.instant('common.success'),
      this._translate.instant('team.invitations-create.success', {
        email: data.email,
      })
    );

    this._dialog.close();
  }
}
