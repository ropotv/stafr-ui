import {
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

import { IInvitation } from '@stafr/core';
import { InvitationService } from '@stafr/api';
import { AlertService, ToastService } from '@stafr/modules';
import { AppEvents } from '@stafr/app.events';

@Component({
  templateUrl: './invitations.component.html',
  styleUrls: ['./invitations.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InvitationsComponent {
  public invitations$: Observable<IInvitation[]>;

  constructor(
    private _service: InvitationService,
    private _toast: ToastService,
    private _translate: TranslateService,
    private _alert: AlertService,
    private _events: AppEvents
  ) {
    this.invitations$ = this._service.getMany().pipe(map(([res]) => res));
  }

  public async resend(invitation: IInvitation): Promise<void> {
    this._alert.open({
      caption: this._translate.instant('team.invitations.resend.alert.caption'),
      message: this._translate.instant('team.invitations.resend.alert.message'),
      buttons: [
        {
          type: 'gray-outlined',
          text: this._translate.instant('team.invitations.resend.alert.btn-no'),
          clicked: () => this._alert.close(),
        },
        {
          type: 'warn',
          text: this._translate.instant(
            'team.invitations.resend.alert.btn-yes'
          ),
          clicked: async () => {
            await this._service.resend(invitation.id).toPromise();
            this._alert.close();
            this._toast.info(
              this._translate.instant('common.success'),
              this._translate.instant('team.invitations.resend.success', {
                email: invitation.email,
              })
            );
          },
        },
      ],
      closed: () => {},
    });
  }

  public async delete(invitation: IInvitation): Promise<void> {
    this._alert.open({
      caption: this._translate.instant('team.invitations.delete.alert.caption'),
      message: this._translate.instant('team.invitations.delete.alert.message'),
      buttons: [
        {
          type: 'gray-outlined',
          text: this._translate.instant('team.invitations.delete.alert.btn-no'),
          clicked: () => this._alert.close(),
        },
        {
          type: 'warn',
          text: this._translate.instant(
            'team.invitations.delete.alert.btn-yes'
          ),
          clicked: async () => {
            await this._service.delete(invitation.id).toPromise();
            this._alert.close();
            this._events.publish('invitations');
            this._toast.warning(
              this._translate.instant('common.success'),
              this._translate.instant('team.invitations.delete.success', {
                email: invitation.email,
              })
            );
          },
        },
      ],
      closed: () => {},
    });
  }
}
