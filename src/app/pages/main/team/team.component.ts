import {
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { KeyValue } from '@angular/common';
import { map } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

import {
  ITableRowClick,
  TableSource,
  TranslateUtilsService,
} from '@stafr/modules';
import { AccountRole, IAccount, UserRole } from '@stafr/core';
import { TeamService } from '@stafr/api';

@Component({
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TeamComponent {
  public UserRole = UserRole;
  public AccountRole = AccountRole;
  public roles$: Observable<KeyValue<AccountRole, string>[]> =
    this._translateUtils.change$.pipe(
      map(() => [
        {
          value: AccountRole.Admin,
          key: this._translateSvc.instant(`enum.${AccountRole.Admin}`),
        },
        {
          value: AccountRole.Manager,
          key: this._translateSvc.instant(`enum.${AccountRole.Manager}`),
        },
        {
          value: AccountRole.HR,
          key: this._translateSvc.instant(`enum.${AccountRole.HR}`),
        },
      ])
    );
  public isBlockedItems$: Observable<KeyValue<string, string>[]> =
    this._translateUtils.change$.pipe(
      map(() => [
        {
          value: 'true',
          key: this._translateSvc.instant('team.list.blocked.true'),
        },
        {
          value: 'false',
          key: this._translateSvc.instant('team.list.blocked.false'),
        },
      ])
    );
  public source$: TableSource<IAccount>;

  constructor(
    private _service: TeamService,
    private _router: Router,
    private _translateUtils: TranslateUtilsService,
    private _translateSvc: TranslateService
  ) {
    this.source$ = (p) => this._service.getMany(p);
  }

  public onRowClick(event: ITableRowClick<IAccount>): void {
    this._router.navigate(['team', event.data.id]);
  }
}
