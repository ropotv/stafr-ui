import { Routes } from '@angular/router';

import { RbacGuard } from '@stafr/modules';
import { AccountRole, UserRole } from '@stafr/core';

import { TeamComponent } from './team.component';
import { CreateInvitationComponent, InvitationsComponent } from './invitations';
import { ViewTeamMemberComponent } from './view';
import { EditTeamMemberComponent } from './edit';

export const TeamRoutes: Routes = [
  {
    path: '',
    component: TeamComponent,
    canActivate: [RbacGuard],
    data: {
      userRoles: [UserRole.Account],
      accountRoles: [AccountRole.Admin],
    },
    children: [
      {
        path: 'invitations',
        component: InvitationsComponent,
        children: [
          {
            path: 'create',
            component: CreateInvitationComponent,
          },
        ],
      },
      {
        path: ':id',
        component: ViewTeamMemberComponent,
        children: [
          {
            path: 'edit',
            component: EditTeamMemberComponent,
          },
        ],
      },
    ],
  },
];
