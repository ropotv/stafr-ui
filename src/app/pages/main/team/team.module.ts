import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import {
  AccountBoxModule,
  ButtonModule,
  CardModule,
  DateUtilsModule,
  DialogModule,
  RbacModule,
  SliderModule,
  StafrFormsModule,
  StatusBoxModule,
  TableModule,
} from '@stafr/modules';
import { InviteFormModule, TeamMemberFormModule } from '@stafr/sections';

import { TeamRoutes } from './team.routes';
import { TeamComponent } from './team.component';
import { ViewTeamMemberComponent } from './view';
import { CreateInvitationComponent, InvitationsComponent } from './invitations';
import { EditTeamMemberComponent } from './edit';

@NgModule({
  imports: [
    RouterModule.forChild(TeamRoutes),
    CommonModule,
    CardModule,
    TableModule,
    ButtonModule,
    StatusBoxModule,
    DialogModule,
    StafrFormsModule,
    SliderModule,
    InviteFormModule,
    AccountBoxModule,
    DateUtilsModule,
    TeamMemberFormModule,
    RbacModule,
    TranslateModule,
  ],
  declarations: [
    TeamComponent,
    InvitationsComponent,
    CreateInvitationComponent,
    ViewTeamMemberComponent,
    EditTeamMemberComponent,
  ],
})
export class TeamModule {}
