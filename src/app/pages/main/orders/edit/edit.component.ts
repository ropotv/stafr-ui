import {
  ChangeDetectionStrategy,
  Component,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { OrdersAttachmentsProvider, OrdersService } from '@stafr/api';
import { IOrderForm } from '@stafr/sections';
import { IDialog, SlugifyUtilsService, ToastService } from '@stafr/modules';
import { AppEvents } from '@stafr/app.events';
import { IOrder } from '@stafr/core';

@Component({
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditOrderComponent {
  public form$: Observable<FormGroup>;

  @ViewChild('dialog')
  private _dialog: IDialog;

  constructor(
    private _orderAttachments: OrdersAttachmentsProvider,
    private _service: OrdersService,
    private _toast: ToastService,
    private _translate: TranslateService,
    private _events: AppEvents,
    route: ActivatedRoute,
    slugify: SlugifyUtilsService,
    fb: FormBuilder
  ) {
    this.form$ = route.parent.paramMap.pipe(
      map((p) => Number(p.get('number'))),
      switchMap((number) => this._service.getOne(number)),
      map((o: IOrder) =>
        fb.group({
          number: o.number,
          clientNo: o.client.number,
          rate: { amount: Number(o.rateAmount), currency: o.rateCurrency },
          profession: o.profession,
          skills: fb.array(o.skills),
          experience: o.experience,
          attachments: {
            files: [],
            attachments: o.attachments,
            prevAttachments: o.attachments,
          },
          skill: '',
        })
      )
    );
  }

  public async onSubmit(data: IOrderForm): Promise<void> {
    await this._service
      .update(data.number, {
        rateAmount: data.rate.amount?.toString(),
        rateCurrency: data.rate.currency,
        profession: data.profession,
        experience: data.experience,
        skills: data.skills,
        clientNo: data.clientNo,
      })
      .toPromise();

    /** Upload attachments **/ {
      if (data.attachments.files?.length) {
        await this._orderAttachments
          .create(data.number, data.attachments.files)
          .toPromise();
      }
    }

    /** Delete attachments **/ {
      const ids = data.attachments.attachments.map((a) => a.id);
      for (const attachment of data.attachments.prevAttachments) {
        if (!ids.includes(attachment.id)) {
          await this._orderAttachments
            .delete(data.number, attachment.id)
            .toPromise();
        }
      }
    }

    this._events.publish('orders', 'order');
    this._toast.success(
      this._translate.instant('common.success'),
      this._translate.instant('orders.edit.success', {
        profession: data.profession,
      })
    );
    this._dialog.close();
  }
}
