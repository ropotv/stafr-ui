import {
  ChangeDetectionStrategy,
  Component,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';

import { IOrderForm } from '@stafr/sections';
import { OrdersAttachmentsProvider, OrdersService } from '@stafr/api';
import { AppEvents } from '@stafr/app.events';
import { FormSection, IDialog, ToastService } from '@stafr/modules';
import { AppStore } from '@stafr/app.store';

@Component({
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateOrderComponent {
  public form: FormGroup;
  public pageForm: FormGroup;

  @ViewChild('dialog')
  private _dialog: IDialog;

  @ViewChild('formComponent')
  private _orderForm: FormSection<IOrderForm>;

  constructor(
    private _orderAttachments: OrdersAttachmentsProvider,
    private _service: OrdersService,
    private _toast: ToastService,
    private _translate: TranslateService,
    private _fb: FormBuilder,
    private _events: AppEvents,
    store: AppStore
  ) {
    this.pageForm = this._fb.group({ createAnother: false });
    this.form = this._fb.group({
      number: null,
      clientNo: null,
      rate: { amount: null, currency: store.currency },
      profession: '',
      experience: null,
      skills: this._fb.array([]),
      attachments: { files: [], attachments: [], prevAttachments: [] },
      skill: '',
    });
  }

  public async onSubmit(data: IOrderForm): Promise<void> {
    const { createAnother } = this.pageForm.getRawValue();

    const order = await this._service
      .create({
        rateAmount: data.rate.amount?.toString(),
        rateCurrency: data.rate.currency,
        profession: data.profession,
        experience: data.experience,
        skills: data.skills,
        clientNo: data.clientNo,
      })
      .toPromise();

    /** Upload attachments **/ {
      if (data.attachments.files?.length) {
        await this._orderAttachments
          .create(order.number, data.attachments.files)
          .toPromise();
      }
    }

    this._events.publish('orders');
    this._toast.success(
      this._translate.instant('common.success'),
      this._translate.instant('orders.create.success', {
        profession: data.profession,
      })
    );

    if (createAnother) {
      const prev = this._orderForm.formValue;
      this._orderForm.unLock();
      this._orderForm.reset();
      this._orderForm.markAsSubmitted(false);
      this._orderForm.form.patchValue({ clientNo: prev.clientNo });
      const skills: FormArray = this._orderForm.form.get('skills') as any;
      skills.clear();
    } else {
      this._dialog.close();
    }
  }
}
