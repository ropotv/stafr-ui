import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import {
  AccountBoxModule,
  ButtonModule,
  CardModule,
  ContractUtilsModule,
  CurrencyUtilsModule,
  DateUtilsModule,
  DialogModule,
  ExtensionUtilsModule,
  RbacModule,
  SliderModule,
  SlugifyUtilsModule,
  StafrFormsModule,
  StatusBoxModule,
  TableModule,
  TimeUtilsModule,
} from '@stafr/modules';
import { OrderFormModule } from '@stafr/sections';

import { OrdersRoutes } from './orders.routes';
import { OrdersComponent } from './orders.component';
import { CreateOrderComponent } from './create';
import { ViewOrderComponent } from './view';
import { EditOrderComponent } from './edit';

@NgModule({
  imports: [
    RouterModule.forChild(OrdersRoutes),
    CommonModule,
    CardModule,
    TableModule,
    ButtonModule,
    StatusBoxModule,
    AccountBoxModule,
    CurrencyUtilsModule,
    DialogModule,
    OrderFormModule,
    StafrFormsModule,
    SliderModule,
    DateUtilsModule,
    TranslateModule,
    ContractUtilsModule,
    ExtensionUtilsModule,
    SlugifyUtilsModule,
    RbacModule,
    TimeUtilsModule,
  ],
  declarations: [
    OrdersComponent,
    CreateOrderComponent,
    ViewOrderComponent,
    EditOrderComponent,
  ],
})
export class OrdersModule {}
