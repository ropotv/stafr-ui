import {
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { KeyValue } from '@angular/common';
import { map } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

import {
  ITableRowClick,
  TableSource,
  TranslateUtilsService,
} from '@stafr/modules';
import { AccountRole, IOrder, OrderStatus, UserRole } from '@stafr/core';
import { OrdersService } from '@stafr/api';

@Component({
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrdersComponent {
  public UserRole = UserRole;
  public AccountRole = AccountRole;
  public statuses$: Observable<KeyValue<OrderStatus, string>[]> =
    this._translateUtils.change$.pipe(
      map(() => [
        {
          value: OrderStatus.Active,
          key: this._translateSvc.instant(`enum.${OrderStatus.Active}`),
        },
        {
          value: OrderStatus.Done,
          key: this._translateSvc.instant(`enum.${OrderStatus.Done}`),
        },
      ])
    );
  public source$: TableSource<IOrder>;

  constructor(
    private _service: OrdersService,
    private _router: Router,
    private _translateUtils: TranslateUtilsService,
    private _translateSvc: TranslateService
  ) {
    this.source$ = (p) => this._service.getMany(p);
  }

  public onRowClick(event: ITableRowClick<IOrder>): void {
    this._router.navigate(['orders', event.data.number]);
  }
}
