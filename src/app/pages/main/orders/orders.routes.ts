import { Routes } from '@angular/router';

import { OrdersComponent } from './orders.component';
import { CreateOrderComponent } from './create';
import { ViewOrderComponent } from './view';
import { EditOrderComponent } from './edit';
import { RbacGuard } from '@stafr/modules';
import { AccountRole, UserRole } from '@stafr/core';

export const OrdersRoutes: Routes = [
  {
    path: '',
    component: OrdersComponent,
    canActivate: [RbacGuard],
    data: {
      userRoles: [UserRole.Account],
      accountRoles: [AccountRole.Admin, AccountRole.Manager, AccountRole.HR],
    },
    children: [
      {
        path: 'create',
        component: CreateOrderComponent,
        canActivate: [RbacGuard],
        data: {
          userRoles: [UserRole.Account],
          accountRoles: [AccountRole.Admin, AccountRole.Manager],
        },
      },
      {
        path: ':number',
        component: ViewOrderComponent,
        children: [
          {
            path: 'edit',
            component: EditOrderComponent,
            canActivate: [RbacGuard],
            data: {
              userRoles: [UserRole.Account],
              accountRoles: [AccountRole.Admin, AccountRole.Manager],
            },
          },
        ],
      },
    ],
  },
];
