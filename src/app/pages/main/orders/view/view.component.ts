import {
  ChangeDetectionStrategy,
  Component,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

import {
  AccountRole,
  BrowserUtils,
  IOrder,
  IOrderAttachment,
  OrderStatus,
  UserRole,
} from '@stafr/core';
import {
  AlertService,
  extensionUtil,
  SliderComponent,
  StatusBoxColors,
  ToastService,
} from '@stafr/modules';
import { OrdersAttachmentsProvider, OrdersService } from '@stafr/api';
import { AppEvents } from '@stafr/app.events';

@Component({
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ViewOrderComponent {
  public UserRole = UserRole;
  public AccountRole = AccountRole;
  public OrderStatus = OrderStatus;

  public colors = StatusBoxColors;
  public order$: Observable<IOrder>;

  @ViewChild(SliderComponent)
  private _slider: SliderComponent;

  constructor(
    private _orderAttachments: OrdersAttachmentsProvider,
    private _orderSvc: OrdersService,
    private _browserUtils: BrowserUtils,
    private _route: ActivatedRoute,
    private _toast: ToastService,
    private _translate: TranslateService,
    private _alert: AlertService,
    private _events: AppEvents
  ) {
    this.order$ = this._route.paramMap.pipe(
      map((p) => Number(p.get('number'))),
      switchMap((number) => this._orderSvc.getOne(number))
    );
  }

  public async onAttachmentDownload(
    order: IOrder,
    attachment: IOrderAttachment
  ): Promise<void> {
    const res = await this._orderAttachments
      .getOne(order.number, attachment.id)
      .toPromise();
    const fileName = attachment.originalName;
    const extension = extensionUtil(fileName);
    this._browserUtils.download(fileName, extension, res.data);
  }

  public async onOrderDelete(order: IOrder): Promise<void> {
    this._alert.open({
      caption: this._translate.instant('orders.view.delete.alert.caption'),
      message: this._translate.instant('orders.view.delete.alert.message'),
      buttons: [
        {
          type: 'gray-outlined',
          text: this._translate.instant('orders.view.delete.alert.btn-no'),
          clicked: () => this._alert.close(),
        },
        {
          type: 'warn',
          text: this._translate.instant('orders.view.delete.alert.btn-yes'),
          clicked: async () => {
            await this._orderSvc.delete(order.number).toPromise();
            this._slider.close();
            this._alert.close();
            this._events.publish('orders');
            this._toast.info(
              this._translate.instant('common.success'),
              this._translate.instant('orders.view.delete.success', {
                profession: order.profession,
              })
            );
          },
        },
      ],
      closed: () => {},
    });
  }
}
