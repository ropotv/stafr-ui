import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HeaderModule, SidebarModule } from '@stafr/layout';
import { AlertModule, OverlayModule } from '@stafr/modules';

import { MainRoutes } from './main.routes';
import { MainComponent } from './main.component';

@NgModule({
  imports: [
    RouterModule.forChild(MainRoutes),
    HeaderModule,
    OverlayModule,
    SidebarModule,
    AlertModule,
  ],
  declarations: [MainComponent],
})
export class MainModule {}
