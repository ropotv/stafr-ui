import {
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';

import { TableSource } from '@stafr/modules';
import { IBusiness } from '@stafr/core';
import { BusinessService } from '@stafr/api';

@Component({
  templateUrl: './businesses.component.html',
  styleUrls: ['./businesses.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BusinessesComponent {
  public source$: TableSource<IBusiness>;

  constructor(private _service: BusinessService) {
    this.source$ = (q) => this._service.getMany(q);
  }
}
