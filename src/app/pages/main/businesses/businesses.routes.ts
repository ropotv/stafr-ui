import { Routes } from '@angular/router';

import { RbacGuard } from '@stafr/modules';
import { UserRole } from '@stafr/core';

import { BusinessesComponent } from './businesses.component';

export const BusinessesRoutes: Routes = [
  {
    path: '',
    component: BusinessesComponent,
    canActivate: [RbacGuard],
    data: {
      userRoles: [UserRole.Admin],
      accountRoles: [],
    },
  },
];
