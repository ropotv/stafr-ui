import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { CardModule, DateUtilsModule, TableModule } from '@stafr/modules';

import { BusinessesRoutes } from './businesses.routes';
import { BusinessesComponent } from './businesses.component';

@NgModule({
  imports: [
    RouterModule.forChild(BusinessesRoutes),
    CommonModule,
    TableModule,
    CardModule,
    DateUtilsModule,
    TranslateModule,
  ],
  declarations: [BusinessesComponent],
})
export class BusinessesModule {}
