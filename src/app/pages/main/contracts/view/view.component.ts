import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

import { ContractStatus, IContract } from '@stafr/core';
import { ContractsService } from '@stafr/api';
import { AppEvents } from '@stafr/app.events';
import { AlertService, SliderComponent, ToastService } from '@stafr/modules';

@Component({
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ViewContractComponent {
  public ContractStatus = ContractStatus;
  public contract$: Observable<IContract>;

  @ViewChild(SliderComponent)
  private _slider: SliderComponent;

  constructor(
    private _contractSvc: ContractsService,
    private _toast: ToastService,
    private _translate: TranslateService,
    private _alert: AlertService,
    private _cdr: ChangeDetectorRef,
    private _events: AppEvents,
    route: ActivatedRoute
  ) {
    this.contract$ = route.paramMap.pipe(
      map((p) => Number(p.get('number'))),
      switchMap((number) => this._contractSvc.getOne(number))
    );
  }

  public async onContractClose(contract: IContract): Promise<void> {
    this._alert.open({
      caption: this._translate.instant('contracts.view.close.alert.caption'),
      message: this._translate.instant('contracts.view.close.alert.message'),
      buttons: [
        {
          type: 'gray-outlined',
          text: this._translate.instant('contracts.view.close.alert.btn-no'),
          clicked: () => this._alert.close(),
        },
        {
          type: 'warn',
          text: this._translate.instant('contracts.view.close.alert.btn-yes'),
          clicked: async () => {
            await this._contractSvc.close(contract.number).toPromise();
            this._alert.close();
            this._events.publish('contract', 'contracts');
            this._toast.info(
              this._translate.instant('common.success'),
              this._translate.instant('contracts.view.close.success')
            );
          },
        },
      ],
      closed: () => {},
    });
  }

  public async onContractDelete(contract: IContract): Promise<void> {
    this._alert.open({
      caption: this._translate.instant('contracts.view.delete.alert.caption'),
      message: this._translate.instant('contracts.view.delete.alert.message'),
      buttons: [
        {
          type: 'gray-outlined',
          text: this._translate.instant('contracts.view.delete.alert.btn-no'),
          clicked: () => this._alert.close(),
        },
        {
          type: 'warn',
          text: this._translate.instant('contracts.view.delete.alert.btn-yes'),
          clicked: async () => {
            await this._contractSvc.delete(contract.number).toPromise();
            this._slider.close();
            this._alert.close();
            this._events.publish('contracts');
            this._toast.info(
              this._translate.instant('common.success'),
              this._translate.instant('contracts.view.delete.success')
            );
          },
        },
      ],
      closed: () => {},
    });
  }
}
