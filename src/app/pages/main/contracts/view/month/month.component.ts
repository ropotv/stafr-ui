import {
  ChangeDetectionStrategy,
  Component,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest, Observable } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ContractsService } from '@stafr/api';
import { AppEvents } from '@stafr/app.events';
import { IDialog, ToastService } from '@stafr/modules';
import { IContractMonth } from '@stafr/core';
import { AppStore } from '@stafr/app.store';
import { IContractMonthForm } from '@stafr/sections';

@Component({
  templateUrl: './month.component.html',
  styleUrls: ['./month.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContractMonthComponent {
  public form$: Observable<FormGroup>;

  constructor(
    private _service: ContractsService,
    private _toast: ToastService,
    private _translate: TranslateService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _events: AppEvents,
    route: ActivatedRoute,
    fb: FormBuilder,
    store: AppStore
  ) {
    this.form$ = combineLatest([route.parent.paramMap, route.paramMap]).pipe(
      filter(([parent, current]) => Boolean(parent && current)),
      switchMap(([parent, current]) =>
        this._service.getMonth(
          Number(parent.get('number')),
          Number(current.get('monthNo'))
        )
      ),
      map((month: IContractMonth) => {
        return fb.group({
          contractNo: month.contract.number,
          monthNo: month.number,
          orderRate: {
            amount: Number(month.orderRate),
            currency: month.orderCurrency || store.currency,
          },
          candidateRate: {
            amount: Number(month.candidateRate),
            currency: month.candidateCurrency || store.currency,
          },
          expenses: {
            amount: Number(month.expensesAmount),
            currency: month.expensesCurrency || store.currency,
          },
          taxes: month.taxes,
          workingHours: month.workingHours,
        });
      })
    );
  }

  @ViewChild('dialog')
  private _dialog: IDialog;

  public async onSubmit(data: IContractMonthForm): Promise<void> {
    await this._service
      .updateMonth(data.contractNo, data.monthNo, {
        orderRate: data.orderRate.amount?.toString(),
        orderCurrency: data.orderRate.currency,
        candidateRate: data.candidateRate.amount?.toString(),
        candidateCurrency: data.candidateRate.currency,
        expensesAmount: data.expenses.amount?.toString(),
        expensesCurrency: data.expenses.currency,
        taxes: data.taxes,
        workingHours: data.workingHours,
      })
      .toPromise();

    this._events.publish('contracts', 'contract');
    this._toast.success(
      this._translate.instant('common.success'),
      this._translate.instant('contracts.month.edit.success')
    );
    this._dialog.close();
    this._router.navigate(['../../'], { relativeTo: this._route });
  }
}
