import { Routes } from '@angular/router';
import { RbacGuard } from '@stafr/modules';
import { AccountRole, UserRole } from '@stafr/core';

import { ContractsComponent } from './contracts.component';
import { CreateContractComponent } from './create';
import { ContractMonthComponent, ViewContractComponent } from './view';

export const ContractsRoutes: Routes = [
  {
    path: '',
    component: ContractsComponent,
    canActivate: [RbacGuard],
    data: {
      userRoles: [UserRole.Account],
      accountRoles: [AccountRole.Admin, AccountRole.Manager],
    },
    children: [
      {
        path: 'create',
        component: CreateContractComponent,
      },
      {
        path: ':number',
        component: ViewContractComponent,
        children: [
          {
            path: 'month/:monthNo',
            component: ContractMonthComponent,
          },
        ],
      },
    ],
  },
];
