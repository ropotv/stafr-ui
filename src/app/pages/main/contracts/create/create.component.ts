import {
  ChangeDetectionStrategy,
  Component,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { IContractForm } from '@stafr/sections';
import { ContractsService } from '@stafr/api';
import { AppEvents } from '@stafr/app.events';
import { FormSection, IDialog, ToastService } from '@stafr/modules';
import { AppStore } from '@stafr/app.store';

@Component({
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateContractComponent {
  public form: FormGroup;
  public pageForm: FormGroup;

  constructor(
    private _service: ContractsService,
    private _toast: ToastService,
    private _translate: TranslateService,
    private _events: AppEvents,
    store: AppStore,
    fb: FormBuilder
  ) {
    this.pageForm = fb.group({ createAnother: false });
    this.form = fb.group({
      order: null,
      candidate: null,
      orderRate: { amount: null, currency: store.currency },
      candidateRate: { amount: null, currency: store.currency },
      expenses: { amount: null, currency: store.currency },
      taxes: null,
      profession: '',
      workingHours: 160,
    });
  }

  @ViewChild('dialog')
  private _dialog: IDialog;

  @ViewChild('formComponent')
  private _contractForm: FormSection<IContractForm>;

  public async onSubmit(data: IContractForm): Promise<void> {
    const { createAnother } = this.pageForm.getRawValue();

    await this._service
      .create({
        candidateNo: data.candidate.number,
        candidateRate: data.candidateRate.amount?.toString(),
        candidateCurrency: data.candidateRate.currency,
        orderNo: data.order.number,
        orderRate: data.orderRate.amount?.toString(),
        orderCurrency: data.orderRate.currency,
        expensesAmount: data.expenses.amount?.toString(),
        expensesCurrency: data.expenses.currency,
        taxes: data.taxes,
        workingHours: data.workingHours,
        profession: data.profession,
      })
      .toPromise();

    this._events.publish('contracts', 'contract');
    this._toast.success(
      this._translate.instant('common.success'),
      this._translate.instant('contracts.create.success')
    );

    if (createAnother) {
      const prev = this._contractForm.formValue;
      this._contractForm.unLock();
      this._contractForm.reset();
      this._contractForm.markAsSubmitted(false);
      this._contractForm.form.patchValue({
        taxes: prev.taxes,
        workingHours: prev.workingHours,
      });
    } else {
      this._dialog.close();
    }
  }
}
