import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import {
  AccountBoxModule,
  ButtonModule,
  CardModule,
  ContractUtilsModule,
  CurrencyUtilsModule,
  DateUtilsModule,
  DialogModule,
  SliderModule,
  StafrFormsModule,
  StatusBoxModule,
  TableModule,
} from '@stafr/modules';
import { ContractFormModule, ContractMonthFormModule } from '@stafr/sections';

import { ContractsComponent } from './contracts.component';
import { ContractsRoutes } from './contracts.routes';
import { CreateContractComponent } from './create';
import { ContractMonthComponent, ViewContractComponent } from './view';

@NgModule({
  imports: [
    RouterModule.forChild(ContractsRoutes),
    CommonModule,
    CardModule,
    TableModule,
    ButtonModule,
    StatusBoxModule,
    AccountBoxModule,
    DialogModule,
    ContractFormModule,
    StafrFormsModule,
    SliderModule,
    DateUtilsModule,
    CurrencyUtilsModule,
    ContractUtilsModule,
    ContractMonthFormModule,
    TranslateModule,
  ],
  declarations: [
    ContractsComponent,
    CreateContractComponent,
    ViewContractComponent,
    ContractMonthComponent,
  ],
})
export class ContractsModule {}
