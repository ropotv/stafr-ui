import {
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { KeyValue } from '@angular/common';
import { map } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

import {
  ITableRowClick,
  TableSource,
  TranslateUtilsService,
} from '@stafr/modules';
import { ContractStatus, IContract } from '@stafr/core';
import { ContractsService } from '@stafr/api';

@Component({
  templateUrl: './contracts.component.html',
  styleUrls: ['./contracts.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContractsComponent {
  public statuses$: Observable<KeyValue<ContractStatus, string>[]> =
    this._translateUtils.change$.pipe(
      map(() => [
        {
          value: ContractStatus.Active,
          key: this._translateSvc.instant(`enum.${ContractStatus.Active}`),
        },
        {
          value: ContractStatus.Closed,
          key: this._translateSvc.instant(`enum.${ContractStatus.Closed}`),
        },
      ])
    );
  public source$: TableSource<IContract>;

  constructor(
    private _service: ContractsService,
    private _router: Router,
    private _translateUtils: TranslateUtilsService,
    private _translateSvc: TranslateService
  ) {
    this.source$ = (p) => this._service.getMany(p);
  }

  public onRowClick(event: ITableRowClick<IContract>): void {
    this._router.navigate(['contracts', event.data.number]);
  }
}
