import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewEncapsulation,
} from '@angular/core';
import { tap } from 'rxjs/operators';
import { BehaviorSubject, Subscription } from 'rxjs';

import { IActivity } from '@stafr/core';
import { ActivitiesProvider } from '@stafr/api';

@Component({
  templateUrl: './activities.component.html',
  styleUrls: ['./activities.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActivitiesPage implements OnInit, OnDestroy {
  private _subscription = new Subscription();
  public pageChange$ = new BehaviorSubject<number>(0);
  public activities: IActivity[] = [];
  public shouldLoadMore = true;

  constructor(
    private _provider: ActivitiesProvider,
    private _cdr: ChangeDetectorRef
  ) {}

  public ngOnInit(): void {
    this._subscription.add(
      this.pageChange$
        .pipe(
          tap(async (page) => {
            const fetcher$ = this._provider.getMany({ page, size: 20 });
            const [activities, total] = await fetcher$.toPromise();
            this.activities = [...this.activities, ...activities];
            this.shouldLoadMore = this.activities.length < total;
            this._cdr.detectChanges();
          })
        )
        .subscribe()
    );
  }

  public ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }
}
