import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import {
  ButtonModule,
  CardModule,
  ChartModule,
  ContractUtilsModule,
  CurrencyUtilsModule,
  DateUtilsModule,
  IconModule,
  RbacModule,
  SliderModule,
} from '@stafr/modules';
import { ActivitiesViewModule } from '@stafr/sections';

import {
  ActivitiesComponent,
  BusinessContractsComponent,
  DashboardBusinessChartComponent,
  DashboardBusinessOverviewComponent,
  DashboardSystemBusinessesComponent,
  DashboardSystemChartComponent,
  DashboardSystemOverviewComponent,
  HrChartComponent,
  HrOrdersComponent,
} from './@sections';
import { DashboardRoutes } from './dashboard.routes';
import { DashboardComponent } from './dashboard.component';
import { ActivitiesPage } from './activities';

@NgModule({
  imports: [
    RouterModule.forChild(DashboardRoutes),
    CommonModule,
    CardModule,
    IconModule,
    CurrencyUtilsModule,
    ChartModule,
    ButtonModule,
    ActivitiesViewModule,
    DateUtilsModule,
    ContractUtilsModule,
    RbacModule,
    SliderModule,
    TranslateModule,
  ],
  declarations: [
    DashboardComponent,
    DashboardBusinessOverviewComponent,
    DashboardBusinessChartComponent,
    ActivitiesComponent,
    BusinessContractsComponent,
    HrOrdersComponent,
    HrChartComponent,
    DashboardSystemBusinessesComponent,
    DashboardSystemOverviewComponent,
    DashboardSystemChartComponent,
    ActivitiesPage,
  ],
})
export class DashboardModule {}
