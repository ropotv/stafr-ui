import {
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';
import { Observable } from 'rxjs';

import { AccountRole, ContractStatus, IContract, UserRole } from '@stafr/core';
import {
  DashboardService,
  IDashboardBusiness,
  IDashboardHR,
  IDashboardSystem,
} from '@stafr/api';

@Component({
  selector: 'stafr-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardComponent {
  public UserRole = UserRole;
  public AccountRole = AccountRole;
  public business$: Observable<IDashboardBusiness>;
  public hr$: Observable<IDashboardHR>;
  public system$: Observable<IDashboardSystem>;

  public activeFilter = (c: IContract) => c.status === ContractStatus.Active;

  constructor(private _service: DashboardService) {
    this.business$ = this._service.business();
    this.hr$ = this._service.hr();
    this.system$ = this._service.system();
  }
}
