import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewEncapsulation,
} from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { DateService, IChartBar, TranslateUtilsService } from '@stafr/modules';
import { ICandidate } from '@stafr/core';

@Component({
  selector: 'dashboard-hr-chart',
  templateUrl: './hr-chart.component.html',
  styleUrls: ['./hr-chart.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HrChartComponent {
  public bars$: Observable<IChartBar[]>;

  @Input()
  public candidatesAmount: number;

  @Input()
  public set candidates(candidates: ICandidate[]) {
    const currentYear = new Date().getFullYear();
    const currentMonth = new Date().getMonth();
    const months = Array.from(Array(12).keys(), (idx) => {
      const month = currentMonth - idx;
      return new Date(currentYear, month, 1);
    }).reverse();

    this.bars$ = this._translateUtilsSvc.change$.pipe(
      map(() =>
        months.map((month) => {
          const firstDay = new Date(
            month.getFullYear(),
            month.getMonth(),
            1
          ).getTime();
          const lastDay = new Date(
            month.getFullYear(),
            month.getMonth() + 1,
            0
          ).getTime();

          const created = candidates.filter((c) => {
            const createdAt = new Date(c.createdAt).getTime();
            return createdAt > firstDay && createdAt < lastDay;
          });

          return {
            name: this._dateSvc.transform(month, 'month'),
            values: [
              {
                color: '#055ffc',
                value: created.length,
                valueString: String(created.length),
              },
            ],
          };
        })
      )
    );
  }

  constructor(
    private _dateSvc: DateService,
    private _translateUtilsSvc: TranslateUtilsService
  ) {}
}
