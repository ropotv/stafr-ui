import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewEncapsulation,
} from '@angular/core';

import { IContract } from '@stafr/core';
import { ContractUtilsService } from '@stafr/modules';

@Component({
  selector: 'dashboard-business-overview',
  templateUrl: './business-overview.component.html',
  styleUrls: ['./business-overview.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardBusinessOverviewComponent {
  public income: number;

  @Input()
  public candidatesAmount: number;

  @Input()
  public ordersAmount: number;

  @Input()
  public contractsAmount: number;

  @Input()
  public set contracts(contracts: IContract[]) {
    this.income = contracts
      .map((c) => this._contractUtilsSvc.getContractInfo(c)?.hourlyIncome)
      .reduce((a, b) => a + b, 0);
  }

  constructor(private _contractUtilsSvc: ContractUtilsService) {}
}
