import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewEncapsulation,
} from '@angular/core';

import { IOrder } from '@stafr/core';

@Component({
  selector: 'dashboard-hr-orders',
  templateUrl: './hr-orders.component.html',
  styleUrls: ['./hr-orders.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HrOrdersComponent {
  @Input()
  public orders: IOrder[];
}
