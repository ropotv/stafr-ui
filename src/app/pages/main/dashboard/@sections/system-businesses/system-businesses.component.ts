import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewEncapsulation,
} from '@angular/core';

import { IBusiness } from '@stafr/core';

@Component({
  selector: 'dashboard-system-businesses',
  templateUrl: './system-businesses.component.html',
  styleUrls: ['./system-businesses.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardSystemBusinessesComponent {
  @Input()
  public businesses: IBusiness[];
}
