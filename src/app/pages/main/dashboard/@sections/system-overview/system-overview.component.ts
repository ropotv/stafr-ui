import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewEncapsulation,
} from '@angular/core';

@Component({
  selector: 'dashboard-system-overview',
  templateUrl: './system-overview.component.html',
  styleUrls: ['./system-overview.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardSystemOverviewComponent {
  @Input()
  public businessAmount: number;

  @Input()
  public usersAmount: number;

  @Input()
  public accountsAmount: number;

  @Input()
  public candidatesAmount: number;
}
