import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewEncapsulation,
} from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

import {
  ContractUtilsService,
  CurrencyUtilsService,
  DateService,
  IChartBar,
  TranslateUtilsService,
} from '@stafr/modules';
import { IContract, monthDiff } from '@stafr/core';

@Component({
  selector: 'dashboard-business-chart',
  templateUrl: './business-chart.component.html',
  styleUrls: ['./business-chart.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardBusinessChartComponent {
  public bars$: Observable<IChartBar[]>;
  public income: number;

  @Input()
  public set contracts(contracts: IContract[]) {
    const months = Array.from(
      Array(12).keys(),
      (idx) => new Date(new Date().getFullYear(), new Date().getMonth() - idx)
    ).reverse();

    this.income = contracts
      .map((c) => this._contractUtilsSvc.getContractInfo(c)?.monthIncome)
      .reduce((a, b) => a + b, 0);

    this.bars$ = this._translateUtilsSvc.change$.pipe(
      map(() =>
        months.map((date) => {
          const expenses = contracts
            .map((c) => c.months)
            .reduce((a, b) => [...a, ...b], [])
            .filter((m) => monthDiff(new Date(m.date), date) === 0)
            .map((m) => this._contractUtilsSvc.getMonthInfo(m)?.monthExpenses)
            .reduce((a, b) => a + b, 0);

          const employee = contracts
            .map((c) => c.months)
            .reduce((a, b) => [...a, ...b], [])
            .filter((m) => monthDiff(new Date(m.date), date) === 0)
            .map((m) => this._contractUtilsSvc.getMonthInfo(m)?.monthEmployee)
            .reduce((a, b) => a + b, 0);

          const profit = contracts
            .map((c) => c.months)
            .reduce((a, b) => [...a, ...b], [])
            .filter((m) => monthDiff(new Date(m.date), date) === 0)
            .map((m) => this._contractUtilsSvc.getMonthInfo(m)?.monthProfit)
            .reduce((a, b) => a + b, 0);

          const taxes = contracts
            .map((c) => c.months)
            .reduce((a, b) => [...a, ...b], [])
            .filter((m) => monthDiff(new Date(m.date), date) === 0)
            .map((m) => this._contractUtilsSvc.getMonthInfo(m)?.monthTaxes)
            .reduce((a, b) => a + b, 0);

          return {
            name: this._dateSvc.transform(date, 'month'),
            values: [
              {
                color: '#ffe76b',
                value: taxes,
                valueString: `${this._translateSvc.instant(
                  'dashboard.business-chart.taxes'
                )} ${this._currencySvc.transform(taxes)}`,
              },
              {
                color: '#FB4D3D',
                value: expenses,
                valueString: `${this._translateSvc.instant(
                  'dashboard.business-chart.expenses'
                )} ${this._currencySvc.transform(expenses)}`,
              },
              {
                color: '#21D19F',
                value: employee,
                valueString: `${this._translateSvc.instant(
                  'dashboard.business-chart.employee'
                )} ${this._currencySvc.transform(employee)}`,
              },
              {
                color: '#055ffc',
                value: profit,
                valueString: `${this._translateSvc.instant(
                  'dashboard.business-chart.profit'
                )} ${this._currencySvc.transform(profit)}`,
              },
            ],
          };
        })
      )
    );
  }

  constructor(
    private _currencySvc: CurrencyUtilsService,
    private _dateSvc: DateService,
    private _translateUtilsSvc: TranslateUtilsService,
    private _contractUtilsSvc: ContractUtilsService,
    private _translateSvc: TranslateService
  ) {}
}
