import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewEncapsulation,
} from '@angular/core';

import { IContract } from '@stafr/core';

@Component({
  selector: 'dashboard-business-contracts',
  templateUrl: './business-contracts.component.html',
  styleUrls: ['./business-contracts.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BusinessContractsComponent {
  @Input()
  public contracts: IContract[];
}
