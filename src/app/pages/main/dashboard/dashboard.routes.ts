import { Routes } from '@angular/router';

import { DashboardComponent } from './dashboard.component';
import { ActivitiesPage } from './activities';

export const DashboardRoutes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: 'activities',
        component: ActivitiesPage,
      },
    ],
  },
];
