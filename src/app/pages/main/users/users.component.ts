import {
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';
import { Router } from '@angular/router';

import { ITableRowClick, TableSource } from '@stafr/modules';
import { IUser } from '@stafr/core';
import { UsersService } from '@stafr/api';

@Component({
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UsersComponent {
  public source$: TableSource<IUser>;

  constructor(private _service: UsersService, private _router: Router) {
    this.source$ = (q) => this._service.getMany(q);
  }

  public onRowClick(event: ITableRowClick<IUser>): void {
    this._router.navigate(['users', event.data.id]);
  }
}
