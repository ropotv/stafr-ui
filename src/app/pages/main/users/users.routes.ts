import { Routes } from '@angular/router';

import { RbacGuard } from '@stafr/modules';
import { UserRole } from '@stafr/core';

import { UsersComponent } from './users.component';
import { ViewUserComponent } from './view';

export const UsersRoutes: Routes = [
  {
    path: '',
    component: UsersComponent,
    canActivate: [RbacGuard],
    data: {
      userRoles: [UserRole.Admin],
      accountRoles: [],
    },
    children: [
      {
        path: ':id',
        component: ViewUserComponent,
      },
    ],
  },
];
