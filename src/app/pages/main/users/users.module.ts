import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import {
  ButtonModule,
  CardModule,
  DateUtilsModule,
  SliderModule,
  StatusBoxModule,
  TableModule,
} from '@stafr/modules';

import { UsersRoutes } from './users.routes';
import { UsersComponent } from './users.component';
import { ViewUserComponent } from './view';

@NgModule({
  imports: [
    RouterModule.forChild(UsersRoutes),
    CommonModule,
    TableModule,
    CardModule,
    DateUtilsModule,
    TranslateModule,
    StatusBoxModule,
    SliderModule,
    ButtonModule,
  ],
  declarations: [UsersComponent, ViewUserComponent],
})
export class UsersModule {}
