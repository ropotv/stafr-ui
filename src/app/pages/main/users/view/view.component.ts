import {
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

import { IUser } from '@stafr/core';
import { AlertService, StatusBoxColors, ToastService } from '@stafr/modules';
import { UsersService } from '@stafr/api';
import { AppEvents } from '@stafr/app.events';

@Component({
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ViewUserComponent {
  public colors = StatusBoxColors;
  public user$: Observable<IUser>;

  constructor(
    private _userSvc: UsersService,
    private _toast: ToastService,
    private _translate: TranslateService,
    private _alert: AlertService,
    private _events: AppEvents,
    route: ActivatedRoute
  ) {
    this.user$ = route.paramMap.pipe(
      map((p) => Number(p.get('id'))),
      switchMap((id) => this._userSvc.getOne(id))
    );
  }

  public async onBlock(user: IUser): Promise<void> {
    this._alert.open({
      caption: this._translate.instant('user.view.block.alert.caption'),
      message: this._translate.instant('user.view.block.alert.message'),
      buttons: [
        {
          type: 'gray-outlined',
          text: this._translate.instant('user.view.block.alert.btn-no'),
          clicked: () => this._alert.close(),
        },
        {
          type: 'warn',
          text: this._translate.instant('user.view.block.alert.btn-yes'),
          clicked: async () => {
            await this._userSvc.block(user.id).toPromise();
            this._alert.close();
            this._events.publish('users', 'user');
            this._toast.info(
              this._translate.instant('common.success'),
              this._translate.instant('user.view.block.success', {
                email: user.email,
              })
            );
          },
        },
      ],
      closed: () => {},
    });
  }

  public async onUnBlock(user: IUser): Promise<void> {
    this._alert.open({
      caption: this._translate.instant('user.view.unblock.alert.caption'),
      message: this._translate.instant('user.view.unblock.alert.message'),
      buttons: [
        {
          type: 'gray-outlined',
          text: this._translate.instant('user.view.unblock.alert.btn-no'),
          clicked: () => this._alert.close(),
        },
        {
          type: 'warn',
          text: this._translate.instant('user.view.unblock.alert.btn-yes'),
          clicked: async () => {
            await this._userSvc.unBlock(user.id).toPromise();
            this._alert.close();
            this._events.publish('users', 'user');
            this._toast.info(
              this._translate.instant('common.success'),
              this._translate.instant('user.view.unblock.success', {
                email: user.email,
              })
            );
          },
        },
      ],
      closed: () => {},
    });
  }
}
