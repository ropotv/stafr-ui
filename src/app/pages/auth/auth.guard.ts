import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { AppStore } from '@stafr/app.store';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(private _router: Router, private _store: AppStore) {}

  public canActivate(): Observable<boolean> {
    return this._store.state$.pipe(
      map((state) => {
        const { authToken } = state;
        if (authToken) {
          this._router.navigate(['']);
          return false;
        }

        return true;
      })
    );
  }
}
