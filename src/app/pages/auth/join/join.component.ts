import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewEncapsulation,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { filter, map, switchMap, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { FormBuilder, FormGroup } from '@angular/forms';

import {
  AuthService,
  IInvitationResponse,
  InvitationProvider,
} from '@stafr/api';

import { JoinValidators } from './join.validators';
import { IJoinForm } from './join.interface';

@Component({
  templateUrl: './join.component.html',
  styleUrls: ['./join.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class JoinComponent {
  public form: FormGroup;
  public invitation$: Observable<IInvitationResponse>;
  public validators = JoinValidators;

  constructor(
    private _service: AuthService,
    private _route: ActivatedRoute,
    private _provider: InvitationProvider,
    cdr: ChangeDetectorRef,
    fb: FormBuilder
  ) {
    this.invitation$ = this._route.queryParamMap.pipe(
      map((param) => param.get('invitation')),
      filter(Boolean),
      switchMap((uuid: string) => this._provider.getOne(uuid)),
      tap((invitation) => {
        if (invitation.inSystem) {
          this.form = fb.group({ fullName: '' });
        } else {
          this.form = fb.group({
            password: '',
            agreement: false,
            fullName: '',
          });
        }

        cdr.detectChanges();
      })
    );
  }

  public onSubmit(data: IJoinForm, invitation: IInvitationResponse): void {
    this._service
      .join({
        invitationUUID: invitation.uuid,
        agreement: data.agreement,
        fullName: data.fullName,
        password: data.password,
      })
      .toPromise();
  }
}
