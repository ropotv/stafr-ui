import { IFormValidator, StafrValidators } from '@stafr/modules';
import { IJoinForm } from './join.interface';

export const JoinValidators: Record<keyof IJoinForm, IFormValidator[]> = {
  fullName: [
    {
      validator: StafrValidators.required(),
      message: 'auth.join.fullName.required',
    },
  ],
  password: [
    {
      validator: StafrValidators.required(),
      message: 'auth.join.password.required',
    },
  ],
  agreement: [
    {
      validator: StafrValidators.isTrue(),
      message: 'auth.join.agreement.required',
    },
  ],
};
