export interface IJoinForm {
  fullName: string;
  password: string;
  agreement: boolean;
}
