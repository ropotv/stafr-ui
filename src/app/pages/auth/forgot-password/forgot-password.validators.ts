import { IFormValidator, StafrValidators } from '@stafr/modules';
import { IForgotPassword } from '@stafr/api';

export const ForgotPasswordValidators: Record<
  keyof IForgotPassword,
  IFormValidator[]
> = {
  email: [
    {
      validator: StafrValidators.required(),
      message: 'auth.forgot-password.form.email.required',
    },
    {
      validator: StafrValidators.email(),
      message: 'auth.forgot-password.form.email.isEmail',
    },
  ],
};
