import {
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { FormBuilder, FormGroup } from '@angular/forms';

import { AuthService, IForgotPassword } from '@stafr/api';

import { ForgotPasswordValidators } from './forgot-password.validators';

@Component({
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ForgotPasswordComponent {
  public form: FormGroup;
  public template$ = new BehaviorSubject('form');
  public validators = ForgotPasswordValidators;

  constructor(private _service: AuthService, fb: FormBuilder) {
    this.form = fb.group({ email: '' });
  }

  public async onSubmit(model: IForgotPassword): Promise<void> {
    await this._service.forgotPassword(model).toPromise();
    this.template$.next('success');
  }
}
