import { IFormValidator, StafrValidators } from '@stafr/modules';
import { ILogin } from '@stafr/api';

export const LoginValidators: Record<keyof ILogin, IFormValidator[]> = {
  email: [
    {
      validator: StafrValidators.required(),
      message: 'auth.login.email.required',
    },
  ],
  password: [
    {
      validator: StafrValidators.required(),
      message: 'auth.login.password.required',
    },
  ],
  remember: [],
};
