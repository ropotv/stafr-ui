import {
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { AuthService, ILogin } from '@stafr/api';

import { LoginValidators } from './login.validators';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginComponent {
  public form: FormGroup;
  public validators = LoginValidators;

  constructor(private _service: AuthService, fb: FormBuilder) {
    this.form = fb.group({
      email: '',
      password: '',
      remember: false,
    });
  }

  public onSubmit(model: ILogin): void {
    this._service.login(model).toPromise();
  }
}
