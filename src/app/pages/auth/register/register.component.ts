import {
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { AuthService, IRegister } from '@stafr/api';

import { RegisterValidators } from './register.validators';

@Component({
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RegisterComponent {
  public form: FormGroup;
  public validators = RegisterValidators;

  constructor(private _service: AuthService, fb: FormBuilder) {
    this.form = fb.group({
      businessName: '',
      fullName: '',
      email: '',
      password: '',
      agreement: false,
    });
  }

  public onSubmit(model: IRegister): void {
    this._service.register(model).toPromise();
  }
}
