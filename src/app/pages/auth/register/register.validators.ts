import { IFormValidator, StafrValidators } from '@stafr/modules';
import { IRegister } from '@stafr/api';

export const RegisterValidators: Record<keyof IRegister, IFormValidator[]> = {
  businessName: [
    {
      validator: StafrValidators.required(),
      message: 'auth.register.businessName.required',
    },
  ],
  fullName: [
    {
      validator: StafrValidators.required(),
      message: 'auth.register.fullName.required',
    },
  ],
  email: [
    {
      validator: StafrValidators.required(),
      message: 'auth.register.email.required',
    },
    {
      validator: StafrValidators.email(),
      message: 'auth.register.email.isEmail',
    },
  ],
  password: [
    {
      validator: StafrValidators.required(),
      message: 'auth.register.password.required',
    },
  ],
  agreement: [
    {
      validator: StafrValidators.isTrue(),
      message: 'auth.register.agreement.required',
    },
  ],
};
