import {
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

import { AuthService, IResetPassword } from '@stafr/api';

import { ResetPasswordValidators } from './reset-password.validators';

@Component({
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResetPasswordComponent {
  public form$: Observable<FormGroup>;
  public validators = ResetPasswordValidators;

  constructor(
    private _service: AuthService,
    route: ActivatedRoute,
    fb: FormBuilder
  ) {
    this.form$ = route.queryParamMap.pipe(
      map((p) =>
        fb.group({
          email: p.get('email'),
          uuid: p.get('key'),
          password: '',
        })
      )
    );
  }

  public onSubmit(model: IResetPassword): void {
    this._service.resetPassword(model).toPromise();
  }
}
