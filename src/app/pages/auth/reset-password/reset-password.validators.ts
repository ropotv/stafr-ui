import { IFormValidator, StafrValidators } from '@stafr/modules';
import { IResetPassword } from '@stafr/api';

export const ResetPasswordValidators: Record<
  keyof IResetPassword,
  IFormValidator[]
> = {
  email: [],
  uuid: [],
  password: [
    {
      validator: StafrValidators.required(),
      message: 'auth.reset-password.password.required',
    },
  ],
};
