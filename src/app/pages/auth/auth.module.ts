import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { ButtonModule, StafrFormsModule } from '@stafr/modules';

import { AuthRoutes } from './auth.routes';
import { AuthComponent } from './auth.component';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { JoinComponent } from './join';
import { ForgotPasswordComponent } from './forgot-password';
import { ResetPasswordComponent } from './reset-password';

@NgModule({
  imports: [
    RouterModule.forChild(AuthRoutes),
    CommonModule,
    StafrFormsModule,
    ButtonModule,
    TranslateModule,
  ],
  declarations: [
    AuthComponent,
    LoginComponent,
    RegisterComponent,
    JoinComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
  ],
})
export class AuthModule {}
