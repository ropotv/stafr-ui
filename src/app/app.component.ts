import { Component } from '@angular/core';

@Component({
  selector: 'stafr-root',
  templateUrl: './app.component.html',
})
export class AppComponent {}
