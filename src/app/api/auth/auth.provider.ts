import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import {
  IAuthResponse,
  IForgotPassword,
  IJoin,
  ILogin,
  IRegister,
  IResetPassword,
} from './auth.interface';

@Injectable({
  providedIn: 'root',
})
export class AuthProvider {
  private _endpoint = '/auth';

  constructor(private _http: HttpClient) {}

  public login(data: ILogin): Observable<IAuthResponse> {
    const url = `${this._endpoint}/login`;
    return this._http.post<IAuthResponse>(url, data);
  }

  public register(data: IRegister): Observable<IAuthResponse> {
    const url = `${this._endpoint}/register`;
    return this._http.post<IAuthResponse>(url, data);
  }

  public join(data: IJoin): Observable<IAuthResponse> {
    const url = `${this._endpoint}/join`;
    return this._http.post<IAuthResponse>(url, data);
  }

  public resetPassword(data: IResetPassword): Observable<IAuthResponse> {
    const url = `${this._endpoint}/reset-password`;
    return this._http.post<IAuthResponse>(url, data);
  }

  public forgotPassword(data: IForgotPassword): Observable<void> {
    const url = `${this._endpoint}/forgot-password`;
    return this._http.post<void>(url, data);
  }

  public refreshToken(): Observable<IAuthResponse> {
    const url = `${this._endpoint}/token`;
    return this._http.get<IAuthResponse>(url);
  }
}
