import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

import { StorageKeys } from '@stafr/core';
import { AppStore } from '@stafr/app.store';
import { AppEvents } from '@stafr/app.events';

import { AuthProvider } from './auth.provider';
import {
  IAuthResponse,
  IForgotPassword,
  IJoin,
  ILogin,
  IRegister,
  IResetPassword,
} from './auth.interface';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(
    private _provider: AuthProvider,
    private _store: AppStore,
    private _router: Router,
    private _events: AppEvents
  ) {}

  public login(data: ILogin): Observable<IAuthResponse> {
    return this._provider.login(data).pipe(
      tap((response) => {
        const storage = data.remember ? localStorage : sessionStorage;
        storage.setItem(StorageKeys.AuthToken, response.token);
        this._store.patchState({
          authToken: response.token,
          principal: response.principal,
          settings: response.settings,
          accounts: response.accounts,
          serverTimezoneOffset: response.timezoneOffset,
        });
        this._events.publish('reload-storage');
      }),
      tap(() => this._router.navigate(['/']))
    );
  }

  public register(data: IRegister): Observable<IAuthResponse> {
    return this._provider.register(data).pipe(
      tap((response) => this._setToken(response)),
      tap(() => this._router.navigate(['/']))
    );
  }

  public join(data: IJoin): Observable<IAuthResponse> {
    return this._provider.join(data).pipe(
      tap((response) => this._setToken(response)),
      tap(() => this._router.navigate(['/']))
    );
  }

  public resetPassword(data: IResetPassword): Observable<IAuthResponse> {
    return this._provider.resetPassword(data).pipe(
      tap((response) => this._setToken(response)),
      tap(() => this._router.navigate(['/']))
    );
  }

  public forgotPassword(data: IForgotPassword): Observable<void> {
    return this._provider.forgotPassword(data);
  }

  public logout(): void {
    localStorage.removeItem(StorageKeys.AuthToken);
    sessionStorage.removeItem(StorageKeys.AuthToken);
    this._store.resetState();
    this._router.navigate(['auth', 'login']);
  }

  public refresh(): Observable<IAuthResponse> {
    return this._provider
      .refreshToken()
      .pipe(tap((response) => this._setToken(response)));
  }

  private _setToken(response: IAuthResponse): void {
    this._store.patchState({
      authToken: response.token,
      principal: response.principal,
      settings: response.settings,
      accounts: response.accounts,
      serverTimezoneOffset: response.timezoneOffset,
    });
    localStorage.setItem(StorageKeys.AuthToken, response.token);
    this._events.publish('reload-storage');
  }
}
