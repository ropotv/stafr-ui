import { IAccountSettings, IJwtAccount, IJwtPrincipal } from '@stafr/core';

export interface IJoin {
  invitationUUID: string;
  fullName: string;
  password: string;
  agreement: boolean;
}

export interface ILogin {
  email: string;
  password: string;
  remember?: boolean;
}

export interface IForgotPassword {
  email: string;
}

export interface IResetPassword {
  email: string;
  uuid: string;
  password: string;
}

export interface IRegister {
  businessName: string;
  fullName: string;
  email: string;
  password: string;
  agreement: boolean;
}

export interface IAuthResponse {
  token: string;
  principal: IJwtPrincipal;
  settings: IAccountSettings;
  accounts: IJwtAccount[];
  timezoneOffset: number;
}
