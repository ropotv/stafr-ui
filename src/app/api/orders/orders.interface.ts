import { CurrencyType } from '@stafr/core';

export interface ICreateOrder {
  rateAmount: string;
  rateCurrency: CurrencyType;
  profession: string;
  experience: number;
  skills: string[];
  clientNo: number;
}

export interface IUpdateOrder {
  rateAmount: string;
  rateCurrency: CurrencyType;
  profession: string;
  experience: number;
  skills: string[];
  clientNo: number;
}
