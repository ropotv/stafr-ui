import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { BuildQuery, IOrder, IQuery, Paginated } from '@stafr/core';

import { ICreateOrder, IUpdateOrder } from './orders.interface';

@Injectable({ providedIn: 'root' })
export class OrdersProvider {
  private _endpoint = '/order';

  constructor(private _http: HttpClient) {}

  public getMany(params?: IQuery): Observable<Paginated<IOrder>> {
    const query = BuildQuery(params);
    const url = `${this._endpoint}${query}`;
    return this._http.get<Paginated<IOrder>>(url);
  }

  public getOne(orderNo: number): Observable<IOrder> {
    const url = `${this._endpoint}/${orderNo}`;
    return this._http.get<IOrder>(url);
  }

  public create(body: ICreateOrder): Observable<IOrder> {
    const url = `${this._endpoint}`;
    return this._http.post<IOrder>(url, body);
  }

  public update(orderNo: number, body: IUpdateOrder): Observable<void> {
    const url = `${this._endpoint}/${orderNo}`;
    return this._http.put<void>(url, body);
  }

  public delete(orderNo: number): Observable<void> {
    const url = `${this._endpoint}/${orderNo}`;
    return this._http.delete<void>(url);
  }
}
