import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { startWith, switchMap } from 'rxjs/operators';

import { IOrder, IQuery, Paginated } from '@stafr/core';
import { AppEvents } from '@stafr/app.events';

import { OrdersProvider } from './orders.provider';
import { ICreateOrder, IUpdateOrder } from './orders.interface';

@Injectable({ providedIn: 'root' })
export class OrdersService {
  constructor(private _provider: OrdersProvider, private _event: AppEvents) {}

  public getMany(params?: IQuery): Observable<Paginated<IOrder>> {
    return this._event.onChanges('orders', 'reload-storage', 'contract').pipe(
      startWith(''),
      switchMap(() => this._provider.getMany(params))
    );
  }

  public getOne(orderNo: number): Observable<IOrder> {
    return this._event.onChanges('order').pipe(
      startWith(''),
      switchMap(() => this._provider.getOne(orderNo))
    );
  }

  public create(body: ICreateOrder): Observable<IOrder> {
    return this._provider.create(body);
  }

  public update(orderNo: number, body: IUpdateOrder): Observable<void> {
    return this._provider.update(orderNo, body);
  }

  public delete(number: number): Observable<void> {
    return this._provider.delete(number);
  }
}
