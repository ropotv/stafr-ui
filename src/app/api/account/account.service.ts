import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AppStore } from '@stafr/app.store';

import { AccountProvider } from './account.provider';
import {
  ICreateAccount,
  IUpdateAccount,
  IUpdateSettings,
} from './account.interface';

@Injectable({ providedIn: 'root' })
export class AccountService {
  constructor(private _provider: AccountProvider, private _store: AppStore) {}

  public create(data: ICreateAccount): Observable<void> {
    return this._provider.create(data);
  }

  public update(data: IUpdateAccount): Observable<void> {
    return this._provider.update(data);
  }

  public updateSettings(data: Partial<IUpdateSettings>): Observable<void> {
    return this._provider.updateSettings(data).pipe(
      tap(() => {
        const { settings } = this._store.get;
        this._store.patchState({
          settings: {
            ...settings,
            preferredCountries: [
              data.preferredCountry,
              ...(settings.preferredCountries || []),
            ].slice(0, 100),
          },
        });
      })
    );
  }

  public switch(accountId: number): Observable<void> {
    return this._provider.switch(accountId);
  }

  public delete(): Observable<void> {
    return this._provider.delete();
  }
}
