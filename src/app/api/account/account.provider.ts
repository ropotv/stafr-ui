import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import {
  ICreateAccount,
  IUpdateAccount,
  IUpdateSettings,
} from './account.interface';

@Injectable({ providedIn: 'root' })
export class AccountProvider {
  private _endpoint = '/account';

  constructor(private _http: HttpClient) {}

  public create(data: ICreateAccount): Observable<void> {
    const url = this._endpoint;
    return this._http.post<void>(url, data);
  }

  public update(data: IUpdateAccount): Observable<void> {
    const url = this._endpoint;
    return this._http.put<void>(url, data);
  }

  public updateSettings(data: Partial<IUpdateSettings>): Observable<void> {
    const url = `${this._endpoint}/settings`;
    return this._http.patch<void>(url, data);
  }

  public switch(accountId: number): Observable<void> {
    const url = `${this._endpoint}/switch`;
    return this._http.post<void>(url, { accountId });
  }

  public delete(): Observable<void> {
    const url = this._endpoint;
    return this._http.delete<void>(url);
  }
}
