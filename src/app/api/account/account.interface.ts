import { CountryType, CurrencyType, LanguageType } from '@stafr/core';

export interface ICreateAccount {
  businessName: string;
  fullName: string;
}

export interface IUpdateAccount {
  fullName: string;
}

export interface IUpdateSettings {
  currency: CurrencyType;
  language: LanguageType;
  preferredCountry: CountryType;
}
