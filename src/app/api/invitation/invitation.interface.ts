import { AccountRole } from '@stafr/core';

export interface IInvitationCreate {
  email: string;
  role: AccountRole;
}

export interface IInvitationResponse {
  uuid: string;
  business: string;
  inSystem: boolean;
}
