import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { BuildQuery, IInvitation, IQuery, Paginated } from '@stafr/core';

import { IInvitationCreate, IInvitationResponse } from './invitation.interface';

@Injectable({ providedIn: 'root' })
export class InvitationProvider {
  private _endpoint = '/invitation';

  constructor(private _http: HttpClient) {}

  public getMany(params?: IQuery): Observable<Paginated<IInvitation>> {
    const query = BuildQuery(params);
    const url = `${this._endpoint}${query}`;
    return this._http.get<Paginated<IInvitation>>(url);
  }

  public getOne(uuid: string): Observable<IInvitationResponse> {
    const url = `${this._endpoint}/${uuid}`;
    return this._http.get<IInvitationResponse>(url);
  }

  public create(body: IInvitationCreate): Observable<void> {
    const url = `${this._endpoint}`;
    return this._http.post<void>(url, body);
  }

  public resend(id: number): Observable<void> {
    const url = `${this._endpoint}/${id}/resend`;
    return this._http.patch<void>(url, {});
  }

  public delete(id: number): Observable<void> {
    const url = `${this._endpoint}/${id}`;
    return this._http.delete<void>(url);
  }
}
