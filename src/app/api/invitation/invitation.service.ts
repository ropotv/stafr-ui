import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { startWith, switchMap } from 'rxjs/operators';

import { IInvitation, IQuery, Paginated } from '@stafr/core';
import { AppEvents } from '@stafr/app.events';

import { InvitationProvider } from './invitation.provider';
import { IInvitationCreate } from './invitation.interface';

@Injectable({ providedIn: 'root' })
export class InvitationService {
  constructor(
    private _provider: InvitationProvider,
    private _event: AppEvents
  ) {}

  public getMany(params?: IQuery): Observable<Paginated<IInvitation>> {
    return this._event.onChanges('invitations', 'reload-storage').pipe(
      startWith(''),
      switchMap(() => this._provider.getMany(params))
    );
  }

  public create(body: IInvitationCreate): Observable<void> {
    return this._provider.create(body);
  }

  public resend(id: number): Observable<void> {
    return this._provider.resend(id);
  }

  public delete(id: number): Observable<void> {
    return this._provider.delete(id);
  }
}
