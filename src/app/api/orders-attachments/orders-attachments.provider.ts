import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ICloudFile, IOrderAttachment } from '@stafr/core';

@Injectable({ providedIn: 'root' })
export class OrdersAttachmentsProvider {
  private _endpoint = '/order-attachment';

  constructor(private _http: HttpClient) {}

  public create(
    orderNo: number,
    files: File[]
  ): Observable<IOrderAttachment[]> {
    const data = new FormData();
    for (const file of files) {
      data.append('attachments', file);
    }
    const url = `${this._endpoint}/${orderNo}`;
    return this._http.post<IOrderAttachment[]>(url, data);
  }

  public getOne(orderNo: number, attachmentId: number): Observable<ICloudFile> {
    const url = `${this._endpoint}/${orderNo}/${attachmentId}`;
    return this._http.get<ICloudFile>(url);
  }

  public delete(orderNo: number, attachmentId: number): Observable<void> {
    const url = `${this._endpoint}/${orderNo}/${attachmentId}`;
    return this._http.delete<void>(url);
  }
}
