import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ICandidateComment } from '@stafr/core';
import { ICreateCandidateComment } from './candidates-comments.interface';

@Injectable({ providedIn: 'root' })
export class CandidatesCommentsProvider {
  private _endpoint = '/candidate-comment';

  constructor(private _http: HttpClient) {}

  public create(
    candidateNo: number,
    dto: ICreateCandidateComment
  ): Observable<ICandidateComment> {
    const url = `${this._endpoint}/${candidateNo}`;
    return this._http.post<ICandidateComment>(url, dto);
  }

  public delete(candidateNo: number, commentId: number): Observable<void> {
    const url = `${this._endpoint}/${candidateNo}/${commentId}`;
    return this._http.delete<void>(url);
  }
}
