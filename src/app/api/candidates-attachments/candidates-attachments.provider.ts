import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ICandidateAttachment, ICloudFile } from '@stafr/core';

@Injectable({ providedIn: 'root' })
export class CandidatesAttachmentsProvider {
  private _endpoint = '/candidate-attachment';

  constructor(private _http: HttpClient) {}

  public create(
    candidateNo: number,
    files: File[]
  ): Observable<ICandidateAttachment[]> {
    const data = new FormData();
    for (const file of files) {
      data.append('attachments', file);
    }
    const url = `${this._endpoint}/${candidateNo}`;
    return this._http.post<ICandidateAttachment[]>(url, data);
  }

  public getOne(
    candidateNo: number,
    attachmentId: number
  ): Observable<ICloudFile> {
    const url = `${this._endpoint}/${candidateNo}/${attachmentId}`;
    return this._http.get<ICloudFile>(url);
  }

  public delete(candidateNo: number, attachmentId: number): Observable<void> {
    const url = `${this._endpoint}/${candidateNo}/${attachmentId}`;
    return this._http.delete<void>(url);
  }
}
