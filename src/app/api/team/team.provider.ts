import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { BuildQuery, IAccount, IQuery, Paginated } from '@stafr/core';
import { IUpdateTeamMember } from './team.interface';

@Injectable({ providedIn: 'root' })
export class TeamProvider {
  private _endpoint = '/team';

  constructor(private _http: HttpClient) {}

  public getMany(params?: IQuery): Observable<Paginated<IAccount>> {
    const query = BuildQuery(params);
    const url = `${this._endpoint}${query}`;
    return this._http.get<Paginated<IAccount>>(url);
  }

  public getOne(id: number): Observable<IAccount> {
    const url = `${this._endpoint}/${id}`;
    return this._http.get<IAccount>(url);
  }

  public update(body: IUpdateTeamMember, id: number): Observable<void> {
    const url = `${this._endpoint}/${id}`;
    return this._http.put<void>(url, body);
  }

  public delete(id: number): Observable<void> {
    const url = `${this._endpoint}/${id}`;
    return this._http.delete<void>(url);
  }

  public block(id: number): Observable<void> {
    const url = `${this._endpoint}/${id}/block`;
    return this._http.patch<void>(url, {});
  }

  public unBlock(id: number): Observable<void> {
    const url = `${this._endpoint}/${id}/unblock`;
    return this._http.patch<void>(url, {});
  }
}
