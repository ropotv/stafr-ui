import { AccountRole } from '@stafr/core';

export interface IUpdateTeamMember {
  fullName: string;
  role: AccountRole;
}
