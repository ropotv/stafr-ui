import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { startWith, switchMap } from 'rxjs/operators';

import { IAccount, IQuery, Paginated } from '@stafr/core';
import { AppEvents } from '@stafr/app.events';

import { TeamProvider } from './team.provider';
import { IUpdateTeamMember } from './team.interface';

@Injectable({ providedIn: 'root' })
export class TeamService {
  constructor(private _provider: TeamProvider, private _event: AppEvents) {}

  public getMany(params?: IQuery): Observable<Paginated<IAccount>> {
    return this._event.onChanges('team', 'reload-storage').pipe(
      startWith(''),
      switchMap(() => this._provider.getMany(params))
    );
  }

  public getOne(id: number): Observable<IAccount> {
    return this._event.onChanges('team-member').pipe(
      startWith(''),
      switchMap(() => this._provider.getOne(id))
    );
  }

  public update(body: IUpdateTeamMember, id: number): Observable<void> {
    return this._provider.update(body, id);
  }

  public delete(id: number): Observable<void> {
    return this._provider.delete(id);
  }

  public block(id: number): Observable<void> {
    return this._provider.block(id);
  }

  public unBlock(id: number): Observable<void> {
    return this._provider.unBlock(id);
  }
}
