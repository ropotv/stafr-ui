import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ISearchItem } from './search.interface';

@Injectable({ providedIn: 'root' })
export class SearchProvider {
  private _endpoint = '/search';

  constructor(private _http: HttpClient) {}

  public search(term: string): Observable<ISearchItem[]> {
    const url = `${this._endpoint}/${term}`;
    return this._http.get<ISearchItem[]>(url);
  }
}
