export enum SearchType {
  Candidate = 'search.candidate',
  Client = 'search.client',
  Order = 'search.order',
}

export interface ISearchItem {
  id: number;
  type: SearchType;
  name: string;
}
