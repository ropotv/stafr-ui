import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IAccountAvatar } from '@stafr/core';

@Injectable({ providedIn: 'root' })
export class AccountAvatarProvider {
  private _endpoint = '/account-avatar';

  constructor(private _http: HttpClient) {}

  public create(file: File): Observable<IAccountAvatar> {
    const data = new FormData();
    data.append('avatar', file);
    const url = `${this._endpoint}`;
    return this._http.post<IAccountAvatar>(url, data);
  }

  public delete(): Observable<void> {
    const url = `${this._endpoint}`;
    return this._http.delete<void>(url);
  }
}
