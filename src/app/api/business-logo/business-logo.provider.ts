import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IBusinessLogo } from '@stafr/core';

@Injectable({ providedIn: 'root' })
export class BusinessLogoProvider {
  private _endpoint = '/business-logo';

  constructor(private _http: HttpClient) {}

  public create(file: File): Observable<IBusinessLogo> {
    const data = new FormData();
    data.append('logo', file);
    const url = `${this._endpoint}`;
    return this._http.post<IBusinessLogo>(url, data);
  }

  public delete(): Observable<void> {
    const url = `${this._endpoint}`;
    return this._http.delete<void>(url);
  }
}
