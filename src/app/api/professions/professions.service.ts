import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { ProfessionsList } from './professions.list';

@Injectable({ providedIn: 'root' })
export class ProfessionsService {
  public getMany(): Observable<string[]> {
    return of(ProfessionsList);
  }
}
