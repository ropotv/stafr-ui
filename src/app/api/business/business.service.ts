import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { startWith, switchMap } from 'rxjs/operators';

import { IBusiness, IQuery, Paginated } from '@stafr/core';
import { AppEvents } from '@stafr/app.events';

import { IUpdateBusiness } from './business.interface';
import { BusinessProvider } from './business.provider';

@Injectable({ providedIn: 'root' })
export class BusinessService {
  constructor(private _provider: BusinessProvider, private _event: AppEvents) {}

  public getMany(params?: IQuery): Observable<Paginated<IBusiness>> {
    return this._event.onChanges('businesses').pipe(
      startWith(''),
      switchMap(() => this._provider.getMany(params))
    );
  }

  public update(data: IUpdateBusiness): Observable<void> {
    return this._provider.update(data);
  }

  public delete(): Observable<void> {
    return this._provider.delete();
  }
}
