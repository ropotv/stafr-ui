import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { BuildQuery, IBusiness, IQuery, Paginated } from '@stafr/core';

import { IUpdateBusiness } from './business.interface';

@Injectable({ providedIn: 'root' })
export class BusinessProvider {
  private _endpoint = '/business';

  constructor(private _http: HttpClient) {}

  public getMany(params?: IQuery): Observable<Paginated<IBusiness>> {
    const query = BuildQuery(params);
    const url = `${this._endpoint}${query}`;
    return this._http.get<Paginated<IBusiness>>(url);
  }

  public update(data: IUpdateBusiness): Observable<void> {
    const url = this._endpoint;
    return this._http.put<void>(url, data);
  }

  public delete(): Observable<void> {
    const url = this._endpoint;
    return this._http.delete<void>(url);
  }
}
