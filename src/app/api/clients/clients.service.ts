import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { startWith, switchMap } from 'rxjs/operators';

import { IClient, IQuery, Paginated } from '@stafr/core';
import { AppEvents } from '@stafr/app.events';

import { ClientsProvider } from './clients.provider';
import { ICreateClient, IUpdateClient } from './clients.interface';

@Injectable({ providedIn: 'root' })
export class ClientsService {
  constructor(private _provider: ClientsProvider, private _event: AppEvents) {}

  public getMany(params?: IQuery): Observable<Paginated<IClient>> {
    return this._event.onChanges('clients', 'reload-storage').pipe(
      startWith(''),
      switchMap(() => this._provider.getMany(params))
    );
  }

  public getOne(number: number): Observable<IClient> {
    return this._event.onChanges('client').pipe(
      startWith(''),
      switchMap(() => this._provider.getOne(number))
    );
  }

  public create(body: ICreateClient): Observable<void> {
    return this._provider.create(body);
  }

  public update(number: number, body: IUpdateClient): Observable<void> {
    return this._provider.update(number, body);
  }

  public delete(number: number): Observable<void> {
    return this._provider.delete(number);
  }
}
