export interface ICreateClient {
  name: string;
  company: string;
  contact: string;
}

export interface IUpdateClient {
  name: string;
  company: string;
  contact: string;
}
