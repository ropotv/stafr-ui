import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { BuildQuery, IClient, IQuery, Paginated } from '@stafr/core';

import { ICreateClient, IUpdateClient } from './clients.interface';

@Injectable({ providedIn: 'root' })
export class ClientsProvider {
  private _endpoint = '/client';

  constructor(private _http: HttpClient) {}

  public getMany(params?: IQuery): Observable<Paginated<IClient>> {
    const query = BuildQuery(params);
    const url = `${this._endpoint}${query}`;
    return this._http.get<Paginated<IClient>>(url);
  }

  public getOne(number: number): Observable<IClient> {
    const url = `${this._endpoint}/${number}`;
    return this._http.get<IClient>(url);
  }

  public create(body: ICreateClient): Observable<void> {
    const url = `${this._endpoint}`;
    return this._http.post<void>(url, body);
  }

  public update(number: number, body: IUpdateClient): Observable<void> {
    const url = `${this._endpoint}/${number}`;
    return this._http.put<void>(url, body);
  }

  public delete(number: number): Observable<void> {
    const url = `${this._endpoint}/${number}`;
    return this._http.delete<void>(url);
  }
}
