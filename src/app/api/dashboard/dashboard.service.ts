import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { startWith, switchMap } from 'rxjs/operators';

import { AppEvents } from '@stafr/app.events';

import { DashboardProvider } from './dashboard.provider';
import {
  IDashboardBusiness,
  IDashboardHR,
  IDashboardSystem,
} from './dashboard.interface';

@Injectable({ providedIn: 'root' })
export class DashboardService {
  constructor(
    private _provider: DashboardProvider,
    private _event: AppEvents
  ) {}

  public system(): Observable<IDashboardSystem> {
    return this._event.onChanges('dashboard', 'reload-storage').pipe(
      startWith(''),
      switchMap(() => this._provider.system())
    );
  }

  public business(): Observable<IDashboardBusiness> {
    return this._event.onChanges('dashboard', 'reload-storage').pipe(
      startWith(''),
      switchMap(() => this._provider.business())
    );
  }

  public hr(): Observable<IDashboardHR> {
    return this._event.onChanges('dashboard', 'reload-storage').pipe(
      startWith(''),
      switchMap(() => this._provider.hr())
    );
  }
}
