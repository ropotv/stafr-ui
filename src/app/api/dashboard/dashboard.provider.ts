import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import {
  IDashboardBusiness,
  IDashboardHR,
  IDashboardSystem,
} from './dashboard.interface';

@Injectable({ providedIn: 'root' })
export class DashboardProvider {
  private _endpoint = '/dashboard';

  constructor(private _http: HttpClient) {}

  public system(): Observable<IDashboardSystem> {
    const url = `${this._endpoint}/system`;
    return this._http.get<IDashboardSystem>(url);
  }

  public business(): Observable<IDashboardBusiness> {
    const url = `${this._endpoint}/business`;
    return this._http.get<IDashboardBusiness>(url);
  }

  public hr(): Observable<IDashboardHR> {
    const url = `${this._endpoint}/hr`;
    return this._http.get<IDashboardHR>(url);
  }
}
