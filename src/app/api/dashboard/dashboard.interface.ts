import {
  IActivity,
  IBusiness,
  ICandidate,
  IContract,
  IOrder,
  IUser,
} from '@stafr/core';

export interface IDashboardSystem {
  businessesAmount: number;
  usersAmount: number;
  accountsAmount: number;
  candidatesAmount: number;
  activities: IActivity[];
  users: IUser[];
  businesses: IBusiness[];
}

export interface IDashboardBusiness {
  candidatesAmount: number;
  ordersAmount: number;
  contractsAmount: number;
  activities: IActivity[];
  contracts: IContract[];
}

export interface IDashboardHR {
  candidatesAmount: number;
  candidates: ICandidate[];
  activities: IActivity[];
  orders: IOrder[];
}
