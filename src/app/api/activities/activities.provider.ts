import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { BuildQuery, IActivity, IQuery, Paginated } from '@stafr/core';

@Injectable({ providedIn: 'root' })
export class ActivitiesProvider {
  private _endpoint = '/activity';

  constructor(private _http: HttpClient) {}

  public getMany(params?: IQuery): Observable<Paginated<IActivity>> {
    const query = BuildQuery(params);
    const url = `${this._endpoint}${query}`;
    return this._http.get<Paginated<IActivity>>(url);
  }
}
