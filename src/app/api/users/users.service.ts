import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { startWith, switchMap } from 'rxjs/operators';

import { IQuery, IUser, Paginated } from '@stafr/core';
import { AppEvents } from '@stafr/app.events';

import { UsersProvider } from './users.provider';

@Injectable({ providedIn: 'root' })
export class UsersService {
  constructor(private _provider: UsersProvider, private _event: AppEvents) {}

  public getMany(params?: IQuery): Observable<Paginated<IUser>> {
    return this._event.onChanges('users').pipe(
      startWith(''),
      switchMap(() => this._provider.getMany(params))
    );
  }

  public getOne(id: number): Observable<IUser> {
    return this._event.onChanges('user').pipe(
      startWith(''),
      switchMap(() => this._provider.getOne(id))
    );
  }

  public block(id: number): Observable<void> {
    return this._provider.block(id);
  }

  public unBlock(id: number): Observable<void> {
    return this._provider.unBlock(id);
  }
}
