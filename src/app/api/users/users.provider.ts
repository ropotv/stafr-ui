import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { BuildQuery, IQuery, IUser, Paginated } from '@stafr/core';

@Injectable({ providedIn: 'root' })
export class UsersProvider {
  private _endpoint = '/user';

  constructor(private _http: HttpClient) {}

  public getMany(params?: IQuery): Observable<Paginated<IUser>> {
    const query = BuildQuery(params);
    const url = `${this._endpoint}${query}`;
    return this._http.get<Paginated<IUser>>(url);
  }

  public getOne(id: number): Observable<IUser> {
    const url = `${this._endpoint}/${id}`;
    return this._http.get<IUser>(url);
  }

  public block(id: number): Observable<void> {
    const url = `${this._endpoint}/${id}/block`;
    return this._http.patch<void>(url, {});
  }

  public unBlock(id: number): Observable<void> {
    const url = `${this._endpoint}/${id}/unblock`;
    return this._http.patch<void>(url, {});
  }
}
