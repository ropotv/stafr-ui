import {
  CandidateStatus,
  CountryType,
  CurrencyType,
  TimeInDays,
} from '@stafr/core';

export interface ICreateCandidate {
  name: string;
  contact: string;
  profession: string;
  experience: TimeInDays;
  rateAmount: string;
  rateCurrency: CurrencyType;
  skills: string[];
  assigneeId: number;
  country: CountryType;
  status: CandidateStatus;
}

export interface IUpdateCandidate {
  name: string;
  contact: string;
  profession: string;
  experience: TimeInDays;
  rateAmount: string;
  rateCurrency: CurrencyType;
  skills: string[];
  assigneeId: number;
  country: CountryType;
  status: CandidateStatus;
}
