import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { startWith, switchMap } from 'rxjs/operators';

import { ICandidate, IQuery, Paginated } from '@stafr/core';
import { AppEvents } from '@stafr/app.events';
import { ICreateCandidate, IUpdateCandidate } from '@stafr/api';

import { CandidatesProvider } from './candidates.provider';

@Injectable({ providedIn: 'root' })
export class CandidatesService {
  constructor(
    private _provider: CandidatesProvider,
    private _event: AppEvents
  ) {}

  public getMany(params?: IQuery): Observable<Paginated<ICandidate>> {
    return this._event
      .onChanges('candidates', 'reload-storage', 'contract')
      .pipe(
        startWith(''),
        switchMap(() => this._provider.getMany(params))
      );
  }

  public count(params?: IQuery): Observable<number> {
    return this._provider.count(params);
  }

  public getOne(candidateNo: number): Observable<ICandidate> {
    return this._event.onChanges('candidate').pipe(
      startWith(''),
      switchMap(() => this._provider.getOne(candidateNo))
    );
  }

  public create(body: ICreateCandidate): Observable<ICandidate> {
    return this._provider.create(body);
  }

  public update(candidateNo: number, body: IUpdateCandidate): Observable<void> {
    return this._provider.update(candidateNo, body);
  }

  public delete(candidateNo: number): Observable<void> {
    return this._provider.delete(candidateNo);
  }
}
