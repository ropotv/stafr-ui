import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { BuildQuery, ICandidate, IQuery, Paginated } from '@stafr/core';
import { ICreateCandidate, IUpdateCandidate } from '@stafr/api';

@Injectable({ providedIn: 'root' })
export class CandidatesProvider {
  private _endpoint = '/candidate';

  constructor(private _http: HttpClient) {}

  public getMany(params?: IQuery): Observable<Paginated<ICandidate>> {
    const query = BuildQuery(params);
    const url = `${this._endpoint}${query}`;
    return this._http.get<Paginated<ICandidate>>(url);
  }

  public count(params?: IQuery): Observable<number> {
    const query = BuildQuery(params);
    const url = `${this._endpoint}/count${query}`;
    return this._http.get<number>(url);
  }

  public getOne(candidateNo: number): Observable<ICandidate> {
    const url = `${this._endpoint}/${candidateNo}`;
    return this._http.get<ICandidate>(url);
  }

  public create(body: ICreateCandidate): Observable<ICandidate> {
    const url = `${this._endpoint}`;
    return this._http.post<ICandidate>(url, body);
  }

  public update(candidateNo: number, body: IUpdateCandidate): Observable<void> {
    const url = `${this._endpoint}/${candidateNo}`;
    return this._http.put<void>(url, body);
  }

  public delete(candidateNo: number): Observable<void> {
    const url = `${this._endpoint}/${candidateNo}`;
    return this._http.delete<void>(url);
  }
}
