import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { SkillsList } from './skills.list';

@Injectable({ providedIn: 'root' })
export class SkillsService {
  public getMany(): Observable<string[]> {
    return of(SkillsList);
  }
}
