import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import {
  BuildQuery,
  IContract,
  IContractMonth,
  IQuery,
  Paginated,
} from '@stafr/core';
import { IContractMonthUpdate } from '@stafr/api';
import { ICreateContract } from './contracts.interface';

@Injectable({ providedIn: 'root' })
export class ContractsProvider {
  private _endpoint = '/contract';

  constructor(private _http: HttpClient) {}

  public getMany(params?: IQuery): Observable<Paginated<IContract>> {
    const query = BuildQuery(params);
    const url = `${this._endpoint}${query}`;
    return this._http.get<Paginated<IContract>>(url);
  }

  public getOne(number: number): Observable<IContract> {
    const url = `${this._endpoint}/${number}`;
    return this._http.get<IContract>(url);
  }

  public create(body: ICreateContract): Observable<void> {
    const url = `${this._endpoint}`;
    return this._http.post<void>(url, body);
  }

  public close(number: number): Observable<void> {
    const url = `${this._endpoint}/${number}/close`;
    return this._http.post<void>(url, {});
  }

  public delete(number: number): Observable<void> {
    const url = `${this._endpoint}/${number}`;
    return this._http.delete<void>(url);
  }

  public getMonth(number: number, monthNo: number): Observable<IContractMonth> {
    const url = `${this._endpoint}/${number}/month/${monthNo}`;
    return this._http.get<IContractMonth>(url);
  }

  public updateMonth(
    number: number,
    monthNo: number,
    dto: IContractMonthUpdate
  ): Observable<IContractMonth> {
    const url = `${this._endpoint}/${number}/month/${monthNo}`;
    return this._http.put<IContractMonth>(url, dto);
  }
}
