import { CurrencyType } from '@stafr/core';

export interface ICreateContract {
  candidateNo: number;
  candidateRate: string;
  candidateCurrency: CurrencyType;
  orderNo: number;
  orderRate: string;
  orderCurrency: CurrencyType;
  expensesAmount: string;
  expensesCurrency: CurrencyType;
  taxes: number;
  workingHours: number;
  profession: string;
}

export interface IContractMonthUpdate {
  candidateRate: string;
  candidateCurrency: CurrencyType;
  orderRate: string;
  orderCurrency: CurrencyType;
  expensesAmount: string;
  expensesCurrency: CurrencyType;
  taxes: number;
  workingHours: number;
}
