import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { startWith, switchMap } from 'rxjs/operators';

import { IContract, IContractMonth, IQuery, Paginated } from '@stafr/core';
import { IContractMonthUpdate } from '@stafr/api';
import { AppEvents } from '@stafr/app.events';

import { ContractsProvider } from './contracts.provider';
import { ICreateContract } from './contracts.interface';

@Injectable({ providedIn: 'root' })
export class ContractsService {
  constructor(
    private _provider: ContractsProvider,
    private _event: AppEvents
  ) {}

  public getMany(params?: IQuery): Observable<Paginated<IContract>> {
    return this._event.onChanges('contracts', 'reload-storage').pipe(
      startWith(''),
      switchMap(() => this._provider.getMany(params))
    );
  }

  public getOne(number: number): Observable<IContract> {
    return this._event.onChanges('contract').pipe(
      startWith(''),
      switchMap(() => this._provider.getOne(number))
    );
  }

  public create(body: ICreateContract): Observable<void> {
    return this._provider.create(body);
  }

  public close(number: number): Observable<void> {
    return this._provider.close(number);
  }

  public delete(number: number): Observable<void> {
    return this._provider.delete(number);
  }

  public getMonth(number: number, monthNo: number): Observable<IContractMonth> {
    return this._provider.getMonth(number, monthNo);
  }

  public updateMonth(
    number: number,
    monthNo: number,
    dto: IContractMonthUpdate
  ): Observable<IContractMonth> {
    return this._provider.updateMonth(number, monthNo, dto);
  }
}
