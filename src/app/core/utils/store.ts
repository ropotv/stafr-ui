import { Directive } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Directive()
export abstract class ComponentStore<T> {
  private _state$: BehaviorSubject<T>;

  protected constructor(private _defaultState: T) {
    this._state$ = new BehaviorSubject<T>(this._defaultState);
  }

  public patchState(partialState: Partial<T>): void {
    this._state$.next({ ...this._state$.value, ...partialState });
  }

  public resetState(): void {
    this._state$.next({ ...this._defaultState });
  }

  public get get(): T {
    return this._state$.value;
  }

  public get state$(): Observable<T> {
    return this._state$.asObservable();
  }

  public select(...keys: (keyof T)[]): Observable<Partial<T>> {
    return this.state$.pipe(
      map(
        (s) =>
          keys
            .map((key) => ({ [key]: s[key] }))
            .reduce((a, b) => ({ ...a, ...b }), {}) as Partial<T>
      )
    );
  }
}
