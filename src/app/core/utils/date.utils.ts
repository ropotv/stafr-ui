export function monthDiff(from: Date, to: Date): number {
  return (
    to.getMonth() -
    from.getMonth() +
    12 * (to.getFullYear() - from.getFullYear())
  );
}
