import { Injectable } from '@angular/core';

const ExtensionMap = {
  pdf: 'application/pdf',
  doc: 'application/msword',
  docx: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  ppt: 'application/vnd.ms-powerpoint',
  pptx: 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
};

@Injectable({ providedIn: 'root' })
export class BrowserUtils {
  public browseFile(format: string = '/', multiple = false): Promise<FileList> {
    return new Promise<FileList>((resolve) => {
      const input = document.createElement('input');
      input.type = 'file';
      input.accept = format;
      input.multiple = multiple;
      input.click();
      input.onchange = (event: any) => resolve(event.target.files);
      input.remove();
    });
  }

  public copyToClipboard(value: string): void {
    const input = document.createElement('input');
    input.style.position = 'fixed';
    input.style.opacity = '0';
    input.value = value;
    document.body.appendChild(input);
    input.focus();
    input.select();
    document.execCommand('copy');
    document.body.removeChild(input);
  }

  public openInNewTab(value: string): void {
    window.open(value, '_blank');
  }

  public openEmail(value: string): void {
    window.open(`mailto:${value}`, '_blank');
  }

  public download(
    fileName: string,
    extension: string,
    buffer: Iterable<number>
  ): void {
    const type = ExtensionMap[extension];
    if (!type) {
      throw new Error('Extension is not found in the type mapping');
    }

    const blob = new Blob([new Uint8Array(buffer)], { type });

    const link = document.createElement('a');
    link.href = window.URL.createObjectURL(blob);
    link.download = fileName;
    link.click();
  }
}
