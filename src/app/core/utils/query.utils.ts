export interface IQuery {
  size?: number;
  page?: number;

  filter?: string;
  order?: string;

  [k: string]: any;
}

export const BuildQuery = (queries: IQuery): string => {
  let query = '';
  for (const key in queries) {
    if (queries.hasOwnProperty(key)) {
      const proto = queries[key];
      query = `${query}&${key}=${proto}`;
    }
  }
  return `?${query.substring(1)}`;
};
