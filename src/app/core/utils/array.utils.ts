export const removeArrayItem = (value: any[], index: number) => [
  ...value.slice(0, index),
  ...value.slice(index + 1),
];

export function findFrequent(values: string[]): string[] {
  const map = new Map();
  if (!values) return [];

  for (let i = 0; i < values.length; i++) {
    if (map.has(values[i])) {
      map.set(values[i], map.get(values[i]) + 1);
    } else {
      map.set(values[i], 1);
    }
  }

  const list = [...map];
  list.sort((a, b) => (a[1] < b[1] ? 1 : -1));

  return list.map((l) => l[0]);
}
