import { ErrorHandler, Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { ToastService } from '@stafr/modules/toast';
import { AppEvents } from '@stafr/app.events';

import { StorageKeys } from '../models';

export enum HttpCodeType {
  BadRequest = 400,
  Unauthorized = 401,
  Forbidden = 403,
  Error = 500,
}

@Injectable()
export class ErrorInterceptor implements ErrorHandler {
  constructor(
    private _toast: ToastService,
    private _events: AppEvents,
    private _translate: TranslateService
  ) {}

  protected GetOrigin(error: any): any {
    const err = error.rejection || error;
    return err.originalError || err.stack || err;
  }

  public handleError(error: any): void {
    const err = this.GetOrigin(error);

    switch (err.status) {
      case HttpCodeType.Unauthorized:
        localStorage.removeItem(StorageKeys.AuthToken);
        sessionStorage.removeItem(StorageKeys.AuthToken);
        window.location.href = '/auth/login';
        break;
      case HttpCodeType.BadRequest:
      case HttpCodeType.Forbidden:
        this._events.publish('http-error');
        this._toast.error(
          this._translate.instant('common.error'),
          this._translate.instant(err.error?.message || err.message)
        );
        break;

      case HttpCodeType.Error:
        this._events.publish('http-error');
        this._toast.error(
          this._translate.instant('common.error'),
          this._translate.instant('common.api-error')
        );
        break;
    }

    console.error(err);
  }
}
