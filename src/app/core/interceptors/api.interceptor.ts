import { Injectable } from '@angular/core';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '@environment';
import { AppStore } from '@stafr/app.store';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
  constructor(private readonly _store: AppStore) {}

  private _prepareRequest(request: HttpRequest<any>): HttpRequest<any> {
    let { headers } = request;
    const { authToken } = this._store.get;

    if (authToken) {
      headers = headers.set('Authorization', `Bearer ${authToken}`);
    }

    return request.clone({
      headers,
      url: environment.api.concat(request.url),
    });
  }

  public intercept(
    request: HttpRequest<any>,
    handler: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (request.url.includes('/assets/')) {
      return handler.handle(request);
    }

    return handler
      .handle(this._prepareRequest(request))
      .pipe(catchError((err: HttpErrorResponse) => throwError(err)));
  }
}
