import { Directive, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

@Directive()
export abstract class Subscribable implements OnDestroy {
  protected readonly subscription = new Subscription();

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
