import { IBusiness } from '../business';
import { AccountRole } from '../account';

export interface IInvitation {
  id: number;
  uuid: string;
  email: string;
  role: AccountRole;

  business: IBusiness;
}
