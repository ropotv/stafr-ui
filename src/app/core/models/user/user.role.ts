export enum UserRole {
  Admin = 'system-admin',
  Account = 'system-account',
}
