import { IAccount } from '../account';

import { UserRole } from './user.role';

export interface IUser {
  id: number;
  createdAt: string;
  email: string;
  password: string;
  accountId: number;
  role: UserRole;
  isBlocked: boolean;

  accounts: IAccount[];
}
