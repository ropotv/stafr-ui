import { ICandidate } from '../candidate';
import { IAccount } from '../account';
import { IActivity } from '../activity';
import { IClient } from '../client';
import { IContract } from '../contract';
import { IOrder } from '../order';
import { IInvitation } from '../invitation';
import { IBusinessLogo } from '../business-logo';

export interface IBusiness {
  id: number;
  createdAt: string;
  name: string;

  logo: IBusinessLogo;
  accounts: IAccount[];
  candidates: ICandidate[];
  clients: IClient[];
  activities: IActivity[];
  orders: IOrder[];
  contracts: IContract[];
  invitations: IInvitation[];
}
