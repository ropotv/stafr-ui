import { IBusiness } from '../business';

export interface IBusinessLogo {
  id: number;
  createdAt: string;
  fileName: string;
  size: number;
  mimeType: string;

  business: IBusiness;
}
