import { ICandidate } from '../candidate';
import { IAccount } from '../account';

export interface ICandidateComment {
  id: number;
  createdAt: string;
  updatedAt: string;
  message: string;

  author: IAccount;
  candidate: ICandidate;
}
