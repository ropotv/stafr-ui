import { IAccount } from '../account';
import { IBusiness } from '../business';

import { ActivityData } from './activity.data';
import { ActivityAction } from './activity.action';
import { ActivityVisibility } from './activity.visibility';

export interface IActivity {
  id: number;
  createdAt: string;
  action: ActivityAction;
  data: ActivityData;
  visibility: ActivityVisibility;

  business: IBusiness;
  author: IAccount;
}
