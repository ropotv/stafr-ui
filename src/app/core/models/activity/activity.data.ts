export interface ActivityData {
  id: number;
  name: string;
}
