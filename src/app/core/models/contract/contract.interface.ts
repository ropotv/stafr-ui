import { IBusiness } from '../business';
import { ICandidate } from '../candidate';
import { CurrencyType } from '../currency';
import { IOrder } from '../order';
import { IClient } from '../client';

import { ContractStatus } from './contract.status';

export interface IContract {
  number: number;
  updatedAt: string;
  comment: string;
  status: ContractStatus;
  profession: string;

  months: IContractMonth[];
  client: IClient;
  business: IBusiness;
  order: IOrder;
  candidate: ICandidate;
}

export interface IContractMonth {
  number: number;
  date: string;
  candidateRate: string;
  candidateCurrency: CurrencyType;
  orderRate: string;
  orderCurrency: CurrencyType;
  expensesAmount: string;
  expensesCurrency: CurrencyType;
  taxes: number;
  workingHours: number;
  contract: IContract;
}
