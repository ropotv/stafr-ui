export enum ContractStatus {
  Active = 'active',
  Closed = 'closed',
}
