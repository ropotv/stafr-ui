import { CurrencyType } from '../currency';
import { IBusiness } from '../business';
import { IAccount } from '../account';
import { IContract } from '../contract';
import { ICandidateComment } from '../candidate-comment';
import { CountryType } from '../country';
import { TimeInDays } from '../date';
import { ICandidateAttachment } from '../candidate-attachment';

import { CandidateStatus } from './candidate.status';

export interface ICandidate {
  number: number;
  createdAt: string;
  updatedAt: string;
  name: string;
  contact: string;
  country: CountryType;
  status: CandidateStatus;
  rateAmount: string;
  rateCurrency: CurrencyType;
  profession: string;
  experience: TimeInDays;
  skills: string[];

  attachments: ICandidateAttachment[];
  comments: ICandidateComment[];
  business: IBusiness;
  author: IAccount;
  assignee: IAccount;
  contracts: IContract[];
}
