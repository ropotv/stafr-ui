export enum CandidateStatus {
  Unavailable = 'unavailable',
  Unknown = 'unknown',
  Interested = 'interested',
  InReview = 'in-review',
  Employee = 'employee',
}
