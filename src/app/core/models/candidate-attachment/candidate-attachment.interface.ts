import { ICandidate } from '../candidate';

export interface ICandidateAttachment {
  id: number;
  createdAt: string;
  fileName: string;
  originalName: string;
  size: number;
  mimeType: string;
  downloads: number;
  candidate: ICandidate;
}
