export enum LanguageType {
  English = 'en',
  Romanian = 'ro',
  Russian = 'ru',
}
