export interface ICloudFile {
  type: 'Buffer';
  data: Iterable<number>;
}
