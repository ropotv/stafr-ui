export enum AccountRole {
  Admin = 'business-administrator',
  Manager = 'business-manager',
  HR = 'business-hr',
}
