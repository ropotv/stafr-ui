import { IUser } from '../user';
import { IBusiness } from '../business';
import { ICandidate } from '../candidate';
import { IActivity } from '../activity';
import { ICandidateComment } from '../candidate-comment';
import { IAccountSettings } from '../account-settings';
import { IAccountAvatar } from '../account-avatar';

import { AccountRole } from './account.role';

export interface IAccount {
  id: number;
  createdAt: string;
  fullName: string;
  role: AccountRole;
  isBlocked: boolean;

  avatar: IAccountAvatar;
  settings: IAccountSettings;
  user: IUser;
  business: IBusiness;
  candidatesAssigned: ICandidate[];
  candidatesCreated: ICandidate[];
  candidateComments: ICandidateComment[];
  activities: IActivity[];
}
