import { IBusiness } from '../business';
import { IOrder } from '../order';
import { IContract } from '../contract';

export interface IClient {
  number: number;
  createdAt: string;
  updatedAt: string;
  name: string;
  contact: string;
  company: string;

  business: IBusiness;
  orders: IOrder[];
  contracts: IContract[];
}
