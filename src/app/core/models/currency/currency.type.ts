export enum CurrencyType {
  EUR = 'EUR',
  USD = 'USD',
  MDL = 'MDL',
  RON = 'RON',
  RUB = 'RUB',
}

export const AllCurrencies = [
  CurrencyType.EUR,
  CurrencyType.USD,
  CurrencyType.MDL,
  CurrencyType.RON,
  CurrencyType.RUB,
];
