export type Paginated<T> = [T[], number];
