import { AccountRole, CurrencyType, UserRole } from '../index';

export interface IJwtPrincipal {
  userId: number;
  userRole: UserRole;
  userBlocked: boolean;
  userEmail: string;
  accountId: number;
  accountName: string;
  accountAvatar: string;
  accountRole: AccountRole;
  accountBlocked: boolean;
  businessId: number;
  businessLogo: string;
  businessName: string;
  currency: CurrencyType;
}

export interface IJwtAccount {
  accountId: number;
  businessName: string;
  businessLogo: string;
}
