export enum OrderStatus {
  Active = 'active',
  Done = 'done',
}
