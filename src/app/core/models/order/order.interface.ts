import { IBusiness } from '../business';
import { IClient } from '../client';
import { CurrencyType } from '../currency';
import { IContract } from '../contract';
import { IOrderAttachment } from '../order-attachment';

import { OrderStatus } from './order.status';

export interface IOrder {
  number: number;
  createdAt: string;
  updatedAt: string;
  status: OrderStatus;
  rateAmount: string;
  rateCurrency: CurrencyType;
  profession: string;
  experience: number;
  skills: string[];

  attachments: IOrderAttachment[];
  business: IBusiness;
  client: IClient;
  contracts: IContract[];
}
