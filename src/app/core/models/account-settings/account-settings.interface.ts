import { CurrencyType } from '../currency';
import { LanguageType } from '../language';
import { IAccount } from '../account';
import { CountryType } from '../country';

export interface IAccountSettings {
  id: number;
  currency: CurrencyType;
  language: LanguageType;
  preferredCountries: CountryType[];

  account: IAccount;
}
