import { IOrder } from '../order';

export interface IOrderAttachment {
  id: number;
  createdAt: string;
  fileName: string;
  originalName: string;
  size: number;
  mimeType: string;
  downloads: number;
  order: IOrder;
}
