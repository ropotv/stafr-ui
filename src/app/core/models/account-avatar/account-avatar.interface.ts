import { IAccount } from '../account';

export interface IAccountAvatar {
  id: number;
  createdAt: string;
  fileName: string;
  size: number;
  mimeType: string;

  account: IAccount;
}
