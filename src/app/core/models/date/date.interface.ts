export const MaxDate = new Date(92715840000000);

export type TimeInDays = number;

export type DateFormat = 'month' | 'full-date' | 'time-ago' | 'month-year';
