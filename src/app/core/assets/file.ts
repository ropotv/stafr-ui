export const FileFormats = {
  document: ['pdf', 'doc', 'docx', 'ppt', 'pptx'],
  image: ['jpg', 'jpeg', 'png'],
};
