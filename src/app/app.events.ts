import { Injectable } from '@angular/core';
import { merge, Observable, Subject } from 'rxjs';

export type ChangeType =
  | 'refresh-session'
  | 'reload-storage'
  | 'logout-session'
  | 'http-error'
  | 'candidates'
  | 'candidate'
  | 'orders'
  | 'order'
  | 'clients'
  | 'client'
  | 'team'
  | 'team-member'
  | 'invitations'
  | 'contracts'
  | 'contract'
  | 'dashboard'
  | 'businesses'
  | 'users'
  | 'user';

@Injectable({
  providedIn: 'root',
})
export class AppEvents {
  private _events = new Map<ChangeType, Subject<any>>();

  private getOrCreateEvent(type: ChangeType): Subject<any> {
    return (
      this._events.get(type) || this._events.set(type, new Subject()).get(type)
    );
  }

  public publish<T = number>(...types: ChangeType[]): void {
    types.forEach((type) => this.getOrCreateEvent(type).next());
  }

  public onChange<T = number>(type: ChangeType): Observable<T> {
    return this.getOrCreateEvent(type).asObservable();
  }

  public onChanges<T = number>(...types: ChangeType[]): Observable<T> {
    return merge(...types.map((t) => this.onChange<T>(t)));
  }
}
