import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CoreModule } from '@stafr/core';
import { ToastModule } from '@stafr/modules';

import { AppInitializer } from './app.initializer';
import { AppRoutes } from './app.routes';
import { AppComponent } from './app.component';

@NgModule({
  imports: [
    RouterModule.forRoot(AppRoutes),
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    ToastModule,
  ],
  declarations: [AppComponent],
  bootstrap: [AppComponent],
  providers: [
    {
      provide: APP_INITIALIZER,
      multi: true,
      useFactory: (initializer: AppInitializer) => () => initializer.init(),
      deps: [AppInitializer],
    },
  ],
})
export class AppModule {}
