import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewEncapsulation,
} from '@angular/core';

import { IJwtAccount } from '@stafr/core';

@Component({
  selector: 'stafr-switch',
  templateUrl: './switch.component.html',
  styleUrls: ['./switch.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SwitchComponent {
  @Input()
  public accounts: IJwtAccount[];

  @Output()
  public onBusinessSelect = new EventEmitter<number>();

  @Output()
  public onBusinessCreate = new EventEmitter<void>();
}
