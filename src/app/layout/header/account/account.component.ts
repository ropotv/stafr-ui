import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewEncapsulation,
} from '@angular/core';

@Component({
  selector: 'stafr-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AccountComponent {
  @Input()
  public avatar: string;

  @Input()
  public fullName: string;

  @Input()
  public role: string;

  @Output()
  public onShowMenu = new EventEmitter<MouseEvent>();
}
