import {
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';
import { Observable } from 'rxjs';
import { distinctUntilKeyChanged, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import {
  IAccountCreate,
  IAccountSettings,
  IBusinessSettings,
} from '@stafr/sections';
import { AppStore } from '@stafr/app.store';
import { IJwtAccount, IJwtPrincipal, UserRole } from '@stafr/core';
import { FormSection, IDialog, IForm, ToastService } from '@stafr/modules';
import { AppEvents } from '@stafr/app.events';
import {
  AccountAvatarProvider,
  AccountService,
  BusinessLogoProvider,
  BusinessService,
} from '@stafr/api';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'stafr-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent {
  public UserRole = UserRole;
  public accountCreateForm: FormGroup;

  public principal$: Observable<IJwtPrincipal>;
  public accounts$: Observable<IJwtAccount[]>;
  public accountSettingsForm$: Observable<FormGroup>;
  public businessSettingsForm$: Observable<FormGroup>;

  constructor(
    private _store: AppStore,
    private _accountAvatar: AccountAvatarProvider,
    private _account: AccountService,
    private _business: BusinessService,
    private _businessLogo: BusinessLogoProvider,
    private _toast: ToastService,
    private _router: Router,
    private _translate: TranslateService,
    private _events: AppEvents,
    fb: FormBuilder
  ) {
    this.accountCreateForm = fb.group({
      businessName: '',
      fullName: '',
    });

    this.principal$ = this._store.state$.pipe(map((s) => s.principal));
    this.accounts$ = this._store.state$.pipe(map((s) => s.accounts));

    this.accountSettingsForm$ = this.principal$.pipe(
      distinctUntilKeyChanged('accountAvatar'),
      map((principal) =>
        fb.group({
          fullName: principal?.accountName,
          avatar: { image: principal?.accountAvatar, file: null },
          currency: principal.currency,
        })
      )
    );

    this.businessSettingsForm$ = this.principal$.pipe(
      distinctUntilKeyChanged('businessLogo'),
      map((principal) =>
        fb.group({
          name: principal?.businessName,
          logo: { image: principal?.businessLogo, file: null },
        })
      )
    );
  }

  public onLogout(): void {
    this._events.publish('logout-session');
  }

  public async onBusinessSwitch(
    accountId: number,
    dialog: IDialog
  ): Promise<void> {
    if (accountId !== this._store.get.principal.accountId) {
      await this._switchBusiness(accountId);
    }
    dialog.close();
  }

  public async onAccountCreate(
    data: IAccountCreate,
    dialog: IDialog,
    form: FormSection<any>
  ): Promise<void> {
    await this._account.create(data).toPromise();
    this._events.publish('refresh-session');
    dialog.close();
    form.unLock();
    form.reset();
  }

  public async onAccountSettings(
    data: IAccountSettings,
    dialog: IDialog,
    form: IForm
  ): Promise<void> {
    /** Update Account FullName **/ {
      await this._account.update({ fullName: data.fullName }).toPromise();
    }
    /** Update Account Currency **/ {
      await this._account
        .updateSettings({ currency: data.currency })
        .toPromise();
    }
    /** Delete Account Avatar **/ {
      if (this._store.get.principal.accountAvatar && !data.avatar.image) {
        await this._accountAvatar.delete().toPromise();
      }
    }
    /** Upload Account Avatar **/ {
      if (data.avatar.file) {
        if (this._store.get.principal.accountAvatar) {
          await this._accountAvatar.delete().toPromise();
        }
        await this._accountAvatar.create(data.avatar.file).toPromise();
      }
    }

    this._events.publish('refresh-session');
    this._toast.success(
      this._translate.instant('common.success'),
      this._translate.instant('header.settings-account.success')
    );
    form.unLock();
    dialog.close();
  }

  public async onBusinessSettings(
    data: IBusinessSettings,
    dialog: IDialog,
    form: IForm
  ): Promise<void> {
    /** Update Business name **/ {
      await this._business.update({ name: data.name }).toPromise();
    }

    /** Delete Business Logo **/ {
      if (this._store.get.principal.businessLogo && !data.logo.image) {
        await this._businessLogo.delete().toPromise();
      }
    }

    /** Upload Business Logo **/ {
      if (data.logo.file) {
        if (this._store.get.principal.businessLogo) {
          await this._businessLogo.delete().toPromise();
        }
        await this._businessLogo.create(data.logo.file).toPromise();
      }
    }

    this._events.publish('refresh-session');
    this._toast.success(
      this._translate.instant('common.success'),
      this._translate.instant('header.settings-business.success')
    );
    form.unLock();
    dialog.close();
  }

  public async onAccountDelete(dialog: IDialog): Promise<void> {
    await this._account.delete().toPromise();

    this._events.publish('refresh-session');
    this._toast.info(
      this._translate.instant('common.success'),
      this._translate.instant('header.delete-account.success')
    );
    this._router.navigate(['/']);
    dialog.close();
  }

  public async onBusinessDelete(dialog: IDialog): Promise<void> {
    await this._business.delete().toPromise();

    this._events.publish('refresh-session');
    this._toast.info(
      this._translate.instant('common.success'),
      this._translate.instant('header.delete-business.success')
    );
    this._router.navigate(['/']);
    dialog.close();
  }

  private async _switchBusiness(accountId: number): Promise<void> {
    await this._account.switch(accountId).toPromise();

    this._events.publish('refresh-session');
    this._router.navigate(['/']);
  }
}
