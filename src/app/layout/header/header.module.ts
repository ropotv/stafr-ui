import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import {
  AvatarModule,
  ButtonModule,
  DialogModule,
  IconModule,
  OverlayModule,
  RbacModule,
} from '@stafr/modules';
import {
  AccountCreateModule,
  AccountSettingsModule,
  BusinessSettingsModule,
} from '@stafr/sections';

import { HeaderComponent } from './header.component';
import { AccountComponent } from './account';
import { AccountMenuComponent } from './account-menu';
import { SearchComponent } from './search';
import { SwitchComponent } from './switch';

@NgModule({
  imports: [
    CommonModule,
    IconModule,
    AvatarModule,
    ButtonModule,
    OverlayModule,
    DialogModule,
    AccountCreateModule,
    AccountSettingsModule,
    BusinessSettingsModule,
    RbacModule,
    TranslateModule,
    FormsModule,
    RouterModule,
  ],
  declarations: [
    HeaderComponent,
    AccountComponent,
    AccountMenuComponent,
    SearchComponent,
    SwitchComponent,
  ],
  exports: [HeaderComponent],
})
export class HeaderModule {}
