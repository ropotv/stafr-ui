import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Output,
  ViewEncapsulation,
} from '@angular/core';

import { AccountRole, UserRole } from '@stafr/core';

@Component({
  selector: 'stafr-account-menu',
  templateUrl: './account-menu.component.html',
  styleUrls: ['./account-menu.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AccountMenuComponent {
  public UserRole = UserRole;
  public AccountRole = AccountRole;

  @Output()
  public onSwitchBusiness = new EventEmitter();

  @Output()
  public onLogout = new EventEmitter();

  @Output()
  public onAccountSettings = new EventEmitter();

  @Output()
  public onBusinessSettings = new EventEmitter();
}
