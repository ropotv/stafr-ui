import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  ViewEncapsulation,
} from '@angular/core';
import {
  BehaviorSubject,
  fromEvent,
  merge,
  Observable,
  of,
  Subject,
  Subscription,
} from 'rxjs';
import { debounceTime, filter, switchMap, tap } from 'rxjs/operators';

import { ISearchItem, SearchProvider, SearchType } from '@stafr/api';
import { KeyCodeType } from '@stafr/modules';

@Component({
  selector: 'stafr-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchComponent implements OnDestroy {
  private _subscription = new Subscription();

  public SearchType = SearchType;
  public term$ = new Subject<string>();
  public items$: Observable<ISearchItem[]>;
  public opened$ = new BehaviorSubject(false);

  constructor(private _search: SearchProvider) {
    this._subscription.add(
      merge(
        fromEvent(document.body, 'click'),
        fromEvent(document.body, 'keydown').pipe(
          filter((event: KeyboardEvent) => event.keyCode === KeyCodeType.Escape)
        )
      )
        .pipe(filter(() => this.opened$.value))
        .subscribe(() => this.close())
    );

    this.items$ = this.term$.pipe(
      debounceTime(500),
      switchMap((term) =>
        term?.trim()?.length > 1 ? this._search.search(term) : of([])
      ),
      tap(() => this.open())
    );
  }

  public open(): void {
    this.opened$.next(true);
  }

  public close(): void {
    this.opened$.next(false);
  }

  public ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }
}
