import { IconType } from '@stafr/modules';
import { AccountRole, UserRole } from '@stafr/core';

export interface ISidebarMenuItem {
  icon: IconType;
  label: string;
  route: string;
  userRoles: UserRole[];
  accountRoles: AccountRole[];
}

export const SidebarMenu: ISidebarMenuItem[] = [
  {
    icon: 'dashboard',
    label: 'sidebar.dashboard',
    route: '/',
    userRoles: [UserRole.Admin, UserRole.Account],
    accountRoles: [AccountRole.Admin, AccountRole.Manager, AccountRole.HR],
  },
  {
    icon: 'contacts',
    label: 'sidebar.candidates',
    route: '/candidates',
    userRoles: [UserRole.Account],
    accountRoles: [AccountRole.Admin, AccountRole.Manager, AccountRole.HR],
  },
  {
    icon: 'orders',
    label: 'sidebar.orders',
    route: '/orders',
    userRoles: [UserRole.Account],
    accountRoles: [AccountRole.Admin, AccountRole.Manager, AccountRole.HR],
  },
  {
    icon: 'people',
    label: 'sidebar.clients',
    route: '/clients',
    userRoles: [UserRole.Account],
    accountRoles: [AccountRole.Admin, AccountRole.Manager],
  },
  {
    icon: 'document',
    label: 'sidebar.contracts',
    route: '/contracts',
    userRoles: [UserRole.Account],
    accountRoles: [AccountRole.Admin, AccountRole.Manager],
  },
  {
    icon: 'team',
    label: 'sidebar.team',
    route: '/team',
    userRoles: [UserRole.Account],
    accountRoles: [AccountRole.Admin],
  },
  {
    icon: 'business-settings',
    label: 'sidebar.business',
    route: '/businesses',
    userRoles: [UserRole.Admin],
    accountRoles: [],
  },
  {
    icon: 'people',
    label: 'sidebar.users',
    route: '/users',
    userRoles: [UserRole.Admin],
    accountRoles: [],
  },
];
