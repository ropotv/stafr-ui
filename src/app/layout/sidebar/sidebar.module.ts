import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';

import { IconModule, RbacModule } from '@stafr/modules';

import { SidebarComponent } from './sidebar.component';

@NgModule({
  imports: [
    CommonModule,
    IconModule,
    RouterModule,
    RbacModule,
    TranslateModule,
  ],
  declarations: [SidebarComponent],
  exports: [SidebarComponent],
})
export class SidebarModule {}
