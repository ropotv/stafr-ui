import { Injectable } from '@angular/core';
import { combineLatest, merge, Observable, of } from 'rxjs';
import { filter, map, switchMap, tap } from 'rxjs/operators';

import { StorageKeys } from '@stafr/core';
import { ExchangeProvider } from '@stafr/modules';
import { AccountService, AuthService } from '@stafr/api';

import { AppStore } from './app.store';
import { AppEvents } from './app.events';

@Injectable({
  providedIn: 'root',
})
export class AppInitializer {
  constructor(
    private _store: AppStore,
    private _event: AppEvents,
    private _auth: AuthService,
    private _account: AccountService,
    private _exchange: ExchangeProvider
  ) {}

  public init(): Promise<any> {
    return this._initStorageData()
      .pipe(
        map(() => this._store.get),
        tap(() => this._watchChanges()),
        filter((state) => state.authToken?.length > 0),
        switchMap(() => this._auth.refresh())
      )
      .toPromise();
  }

  private _reloadStorage(): Observable<void> {
    return combineLatest([this._exchange.getRates()]).pipe(
      map(([exchangeRate]) => this._store.patchState({ exchangeRate }))
    );
  }

  private _initStorageData(): Observable<void> {
    const authToken =
      localStorage.getItem(StorageKeys.AuthToken) ||
      sessionStorage.getItem(StorageKeys.AuthToken);

    return of({
      authToken,
    }).pipe(map((data) => this._store.patchState(data)));
  }

  private _watchChanges(): void {
    merge(
      this._event
        .onChange('refresh-session')
        .pipe(switchMap(() => this._auth.refresh())),
      this._event
        .onChange('logout-session')
        .pipe(tap(() => this._auth.logout())),
      this._event
        .onChange('reload-storage')
        .pipe(switchMap(() => this._reloadStorage()))
    ).subscribe();
  }
}
