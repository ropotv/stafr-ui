import { Routes } from '@angular/router';
import { MainGuard } from '@stafr/pages/main/main.guard';

export const AppRoutes: Routes = [
  {
    path: '',
    canActivate: [MainGuard],
    loadChildren: () =>
      import('./pages/main/main.module').then((m) => m.MainModule),
  },
  {
    path: 'auth',
    loadChildren: () =>
      import('./pages/auth/auth.module').then((m) => m.AuthModule),
  },
];
