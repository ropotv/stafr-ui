import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import {
  AvatarModule,
  DateUtilsModule,
  SafeHtmlUtilsModule,
} from '@stafr/modules';

import { ActivitiesViewComponent } from './activities-view.component';

@NgModule({
  imports: [
    CommonModule,
    AvatarModule,
    DateUtilsModule,
    TranslateModule,
    SafeHtmlUtilsModule,
  ],
  declarations: [ActivitiesViewComponent],
  exports: [ActivitiesViewComponent],
})
export class ActivitiesViewModule {}
