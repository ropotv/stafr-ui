import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewEncapsulation,
} from '@angular/core';
import { Router } from '@angular/router';

import { IActivity } from '@stafr/core';

@Component({
  selector: 'section-activities-view',
  templateUrl: './activities-view.component.html',
  styleUrls: ['./activities-view.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActivitiesViewComponent {
  @Input()
  public activities: IActivity[];

  constructor(private _router: Router) {}
  public onLabelClick(e: MouseEvent): void {
    const html = e.target as HTMLElement;
    if (html.nodeName === 'B') {
      const { link } = html.dataset;
      if (link) {
        this._router.navigate([link]);
      }
    }
  }
}
