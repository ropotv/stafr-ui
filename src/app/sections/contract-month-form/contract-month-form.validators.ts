import { IFormValidator, StafrValidators } from '@stafr/modules';

import { IContractMonthForm } from './contract-month-form.interface';

export const ContractMonthFormValidators: Record<
  keyof IContractMonthForm,
  IFormValidator[]
> = {
  contractNo: [],
  monthNo: [],
  orderRate: [
    {
      validator: StafrValidators.rateRequired(),
      message: 'forms.contract-month.orderRate.required',
    },
  ],
  candidateRate: [
    {
      validator: StafrValidators.rateRequired(),
      message: 'forms.contract-month.candidateRate.required',
    },
  ],
  expenses: [
    {
      validator: StafrValidators.rateRequired(),
      message: 'forms.contract-month.expenses.required',
    },
  ],
  taxes: [
    {
      validator: StafrValidators.required(),
      message: 'forms.contract-month.taxes.required',
    },
  ],
  workingHours: [
    {
      validator: StafrValidators.required(),
      message: 'forms.contract-month.workingHours.required',
    },
  ],
};
