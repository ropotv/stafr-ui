import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import {
  AvatarModule,
  ButtonModule,
  ExcludeUtilsModule,
  StafrFormsModule,
} from '@stafr/modules';

import { ContractMonthFormComponent } from './contract-month-form.component';

@NgModule({
  imports: [
    CommonModule,
    StafrFormsModule,
    AvatarModule,
    ButtonModule,
    ExcludeUtilsModule,
    TranslateModule,
  ],
  declarations: [ContractMonthFormComponent],
  exports: [ContractMonthFormComponent],
})
export class ContractMonthFormModule {}
