import {
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';

import { FormSection } from '@stafr/modules';

import { ContractMonthFormValidators } from './contract-month-form.validators';
import { IContractMonthForm } from './contract-month-form.interface';

@Component({
  selector: 'section-contract-month-form',
  templateUrl: './contract-month-form.component.html',
  styleUrls: ['./contract-month-form.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContractMonthFormComponent extends FormSection<IContractMonthForm> {
  public validators = ContractMonthFormValidators;
}
