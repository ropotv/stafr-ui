import { IRate } from '@stafr/modules';

export interface IContractMonthForm {
  contractNo: number;
  monthNo: number;
  orderRate: IRate;
  candidateRate: IRate;
  expenses: IRate;
  taxes: number;
  workingHours: number;
}
