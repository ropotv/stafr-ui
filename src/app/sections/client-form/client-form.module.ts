import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { StafrFormsModule } from '@stafr/modules';

import { ClientFormComponent } from './client-form.component';

@NgModule({
  imports: [CommonModule, StafrFormsModule, TranslateModule],
  declarations: [ClientFormComponent],
  exports: [ClientFormComponent],
})
export class ClientFormModule {}
