import {
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';

import { FormSection } from '@stafr/modules';

import { ClientFormValidators } from './client-form.validators';
import { IClientForm } from './client-form.interface';

@Component({
  selector: 'section-client-form',
  templateUrl: './client-form.component.html',
  styleUrls: ['./client-form.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClientFormComponent extends FormSection<IClientForm> {
  public validators = ClientFormValidators;
}
