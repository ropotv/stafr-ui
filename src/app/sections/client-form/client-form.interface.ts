export interface IClientForm {
  number: number;
  name: string;
  company: string;
  contact: string;
}
