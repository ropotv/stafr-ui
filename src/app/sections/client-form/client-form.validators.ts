import { IFormValidator, StafrValidators } from '@stafr/modules';

import { IClientForm } from './client-form.interface';

export const ClientFormValidators: Record<keyof IClientForm, IFormValidator[]> =
  {
    number: [],
    name: [
      {
        validator: StafrValidators.required(),
        message: 'forms.client.name.required',
      },
    ],
    company: [
      {
        validator: StafrValidators.required(),
        message: 'forms.client.company.required',
      },
    ],
    contact: [
      {
        validator: StafrValidators.required(),
        message: 'forms.client.contact.required',
      },
    ],
  };
