import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { StafrFormsModule } from '@stafr/modules';

import { AccountSettingsComponent } from './account-settings.component';

@NgModule({
  imports: [CommonModule, StafrFormsModule, TranslateModule],
  declarations: [AccountSettingsComponent],
  exports: [AccountSettingsComponent],
})
export class AccountSettingsModule {}
