import { IFormValidator, StafrValidators } from '@stafr/modules';
import { FileFormats } from '@stafr/core';

import { IAccountSettings } from './account-settings.interface';

export const AccountSettingsValidators: Record<
  keyof IAccountSettings,
  IFormValidator[]
> = {
  avatar: [
    {
      validator: StafrValidators.fileFormat(FileFormats.image),
      message: 'forms.account-settings.avatar.fileFormat',
    },
    {
      validator: StafrValidators.fileSize(10),
      message: 'forms.account-settings.avatar.fileSize',
    },
  ],
  fullName: [
    {
      validator: StafrValidators.required(),
      message: 'forms.account-settings.fullName.required',
    },
  ],
  currency: [
    {
      validator: StafrValidators.required(),
      message: 'forms.account-settings.currency.required',
    },
  ],
};
