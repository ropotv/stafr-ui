import { CurrencyType } from '@stafr/core';
import { IImageBoxValue } from '@stafr/modules';

export interface IAccountSettings {
  avatar: IImageBoxValue;
  fullName: string;
  currency: CurrencyType;
}
