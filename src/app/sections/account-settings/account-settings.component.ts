import {
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';

import { FormSection } from '@stafr/modules';
import { AllCurrencies, CurrencyType } from '@stafr/core';

import { AccountSettingsValidators } from './account-settings.validators';
import { IAccountSettings } from './account-settings.interface';

@Component({
  selector: 'section-account-settings',
  templateUrl: './account-settings.component.html',
  styleUrls: ['./account-settings.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AccountSettingsComponent extends FormSection<IAccountSettings> {
  public currencies: CurrencyType[] = AllCurrencies;
  public validators = AccountSettingsValidators;
}
