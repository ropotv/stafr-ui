import { IFormValidator, StafrValidators } from '@stafr/modules';

import { IOrderForm } from './order-form.interface';

export const OrderFormValidators: Record<keyof IOrderForm, IFormValidator[]> = {
  number: [],
  profession: [
    {
      validator: StafrValidators.required(),
      message: 'forms.order.profession.required',
    },
  ],
  experience: [
    {
      validator: StafrValidators.required(),
      message: 'forms.order.experience.required',
    },
  ],
  clientNo: [
    {
      validator: StafrValidators.required(),
      message: 'forms.order.client.required',
    },
  ],
  rate: [
    {
      validator: StafrValidators.rateRequired(),
      message: 'forms.order.rate.required',
    },
  ],
  skill: [],
  skills: [],
  attachments: [],
};
