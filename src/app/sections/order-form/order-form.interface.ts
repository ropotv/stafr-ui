import { IFileUploadBoxValue, IRate } from '@stafr/modules';
import { IOrderAttachment } from '@stafr/core';

export interface IOrderForm {
  number: number;
  clientNo: number;
  rate: IRate;
  profession: string;
  experience: number;
  skills: string[];
  attachments: IFileUploadBoxValue<IOrderAttachment>;

  skill: string;
}
