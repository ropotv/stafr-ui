import {
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';
import { map } from 'rxjs/operators';
import { FormArray, FormBuilder } from '@angular/forms';

import { FormSection, TranslateEnums } from '@stafr/modules';
import { FileFormats } from '@stafr/core';
import { ClientsProvider, ProfessionsService, SkillsService } from '@stafr/api';

import { OrderFormValidators } from './order-form.validators';
import { IOrderForm } from './order-form.interface';

@Component({
  selector: 'section-order-form',
  templateUrl: './order-form.component.html',
  styleUrls: ['./order-form.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrderFormComponent extends FormSection<IOrderForm> {
  public validators = OrderFormValidators;
  public format = [...FileFormats.document, ...FileFormats.image];

  public clients$ = this._clients.getMany().pipe(map(([res]) => res));
  public professions$ = this._professionSvc.getMany();
  public skills$ = this._skillSvc.getMany();

  constructor(
    private _translateEnums: TranslateEnums,
    private _clients: ClientsProvider,
    private _professionSvc: ProfessionsService,
    private _skillSvc: SkillsService,
    private _fb: FormBuilder
  ) {
    super();
  }

  public onSkillAdd(): void {
    const { skill } = this.formValue;
    if (!skill) {
      return;
    }
    this._skills.push(this._fb.control(skill));
    this.form.patchValue({ skill: '' });
  }

  public onSkillRemove(idx: number): void {
    this._skills.removeAt(idx);
  }

  private get _skills(): FormArray {
    return this.form.get('skills') as any;
  }
}
