import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import {
  AvatarModule,
  ButtonModule,
  ExcludeUtilsModule,
  StafrFormsModule,
  TimeUtilsModule,
} from '@stafr/modules';

import { OrderFormComponent } from './order-form.component';

@NgModule({
  imports: [
    CommonModule,
    StafrFormsModule,
    AvatarModule,
    ButtonModule,
    ExcludeUtilsModule,
    TimeUtilsModule,
    TranslateModule,
  ],
  declarations: [OrderFormComponent],
  exports: [OrderFormComponent],
})
export class OrderFormModule {}
