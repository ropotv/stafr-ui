import { IFormValidator, StafrValidators } from '@stafr/modules';
import { FileFormats } from '@stafr/core';

import { IBusinessSettings } from './business-settings.interface';

export const BusinessSettingsValidators: Record<
  keyof IBusinessSettings,
  IFormValidator[]
> = {
  logo: [
    {
      validator: StafrValidators.fileFormat(FileFormats.image),
      message: 'forms.business-settings.logo.fileFormat',
    },
    {
      validator: StafrValidators.fileSize(10),
      message: 'forms.business-settings.logo.fileSize',
    },
  ],
  name: [
    {
      validator: StafrValidators.required(),
      message: 'forms.business-settings.name.required',
    },
  ],
};
