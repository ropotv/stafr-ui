import {
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';

import { FormSection } from '@stafr/modules';

import { IBusinessSettings } from './business-settings.interface';
import { BusinessSettingsValidators } from './business-settings.validators';

@Component({
  selector: 'section-business-settings',
  templateUrl: './business-settings.component.html',
  styleUrls: ['./business-settings.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BusinessSettingsComponent extends FormSection<IBusinessSettings> {
  public validators = BusinessSettingsValidators;
}
