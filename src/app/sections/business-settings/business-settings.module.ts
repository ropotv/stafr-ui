import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { StafrFormsModule } from '@stafr/modules';

import { BusinessSettingsComponent } from './business-settings.component';

@NgModule({
  imports: [CommonModule, StafrFormsModule, TranslateModule],
  declarations: [BusinessSettingsComponent],
  exports: [BusinessSettingsComponent],
})
export class BusinessSettingsModule {}
