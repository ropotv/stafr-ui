import { IImageBoxValue } from '@stafr/modules';

export interface IBusinessSettings {
  logo: IImageBoxValue;
  name: string;
}
