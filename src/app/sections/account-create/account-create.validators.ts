import { IFormValidator, StafrValidators } from '@stafr/modules';

import { IAccountCreate } from './account-create.interface';

export const AccountCreateValidators: Record<
  keyof IAccountCreate,
  IFormValidator[]
> = {
  businessName: [
    {
      validator: StafrValidators.required(),
      message: 'forms.account-create.businessName.required',
    },
  ],
  fullName: [
    {
      validator: StafrValidators.required(),
      message: 'forms.account-create.fullName.required',
    },
  ],
};
