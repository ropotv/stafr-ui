import {
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';

import { FormSection } from '@stafr/modules';
import { IAccountCreate } from '@stafr/sections';

import { AccountCreateValidators } from './account-create.validators';

@Component({
  selector: 'section-account-create',
  templateUrl: './account-create.component.html',
  styleUrls: ['./account-create.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AccountCreateComponent extends FormSection<IAccountCreate> {
  public validators = AccountCreateValidators;
}
