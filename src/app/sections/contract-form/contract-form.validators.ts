import { IFormValidator, StafrValidators } from '@stafr/modules';

import { IContractForm } from './contract-form.interface';

export const ContractFormValidators: Record<
  keyof IContractForm,
  IFormValidator[]
> = {
  order: [
    {
      validator: StafrValidators.required(),
      message: 'forms.contract.order.required',
    },
  ],
  candidate: [
    {
      validator: StafrValidators.required(),
      message: 'forms.contract.candidate.required',
    },
  ],
  orderRate: [
    {
      validator: StafrValidators.rateRequired(),
      message: 'forms.contract.orderRate.required',
    },
  ],
  candidateRate: [
    {
      validator: StafrValidators.rateRequired(),
      message: 'forms.contract.candidateRate.required',
    },
  ],
  expenses: [
    {
      validator: StafrValidators.rateRequired(),
      message: 'forms.contract.expenses.required',
    },
  ],
  taxes: [
    {
      validator: StafrValidators.required(),
      message: 'forms.contract.taxes.required',
    },
  ],
  profession: [
    {
      validator: StafrValidators.required(),
      message: 'forms.contract.profession.required',
    },
  ],
  workingHours: [
    {
      validator: StafrValidators.required(),
      message: 'forms.contract.workingHours.required',
    },
  ],
};
