import {
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';
import { map } from 'rxjs/operators';

import { FormSection, IRate } from '@stafr/modules';
import {
  CandidatesService,
  OrdersService,
  ProfessionsService,
} from '@stafr/api';
import { CandidateStatus, ICandidate, IOrder, OrderStatus } from '@stafr/core';

import { ContractFormValidators } from './contract-form.validators';
import { IContractForm } from './contract-form.interface';

@Component({
  selector: 'section-contract-form',
  templateUrl: './contract-form.component.html',
  styleUrls: ['./contract-form.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContractFormComponent extends FormSection<IContractForm> {
  public validators = ContractFormValidators;

  public orders$ = this._orders
    .getMany({ filter: `order.status~eq~${OrderStatus.Active}~AND` })
    .pipe(map(([res]) => res));
  public candidates$ = this._candidates
    .getMany({
      filter: `candidate.status~neq~${CandidateStatus.Employee}~AND`,
    })
    .pipe(map(([res]) => res));
  public professions$ = this._professionSvc.getMany();

  constructor(
    private _orders: OrdersService,
    private _candidates: CandidatesService,
    private _professionSvc: ProfessionsService
  ) {
    super();
  }

  public onOrderChange(order: IOrder): void {
    if (!order) {
      return;
    }

    const rate: IRate = {
      currency: order.rateCurrency,
      amount: Number(order.rateAmount),
    };

    this.form.get('orderRate').setValue(rate);
    this.form.get('profession').setValue(order.profession);
  }

  public onCandidateChange(candidate: ICandidate): void {
    if (!candidate) {
      return;
    }

    const rate: IRate = {
      currency: candidate.rateCurrency,
      amount: Number(candidate.rateAmount),
    };
    this.form.get('candidateRate').setValue(rate);
  }
}
