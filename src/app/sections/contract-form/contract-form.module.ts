import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import {
  AvatarModule,
  ButtonModule,
  ExcludeUtilsModule,
  StafrFormsModule,
} from '@stafr/modules';

import { ContractFormComponent } from './contract-form.component';

@NgModule({
  imports: [
    CommonModule,
    StafrFormsModule,
    AvatarModule,
    ButtonModule,
    ExcludeUtilsModule,
    TranslateModule,
  ],
  declarations: [ContractFormComponent],
  exports: [ContractFormComponent],
})
export class ContractFormModule {}
