import { ICandidate, IOrder } from '@stafr/core';
import { IRate } from '@stafr/modules';

export interface IContractForm {
  order: IOrder;
  candidate: ICandidate;
  orderRate: IRate;
  candidateRate: IRate;
  expenses: IRate;
  taxes: number;
  profession: string;
  workingHours: number;
}
