import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { ButtonModule, StafrFormsModule } from '@stafr/modules';

import { InviteFormComponent } from './invite-form.component';

@NgModule({
  imports: [CommonModule, StafrFormsModule, ButtonModule, TranslateModule],
  declarations: [InviteFormComponent],
  exports: [InviteFormComponent],
})
export class InviteFormModule {}
