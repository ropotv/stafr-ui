import {
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';

import { FormSection, StatusBoxColors, TranslateEnums } from '@stafr/modules';

import { InviteFormValidators } from './invite-form.validators';
import { IInviteForm } from './invite-form.interface';

@Component({
  selector: 'section-invite-form',
  templateUrl: './invite-form.component.html',
  styleUrls: ['./invite-form.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InviteFormComponent extends FormSection<IInviteForm> {
  public colors = StatusBoxColors;
  public validators = InviteFormValidators;
  public roles$ = this._translateEnums.roles$;

  constructor(private _translateEnums: TranslateEnums) {
    super();
  }
}
