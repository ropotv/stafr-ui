import { AccountRole } from '@stafr/core';

export interface IInviteForm {
  email: string;
  role: AccountRole;
}
