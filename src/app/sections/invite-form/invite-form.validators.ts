import { IFormValidator, StafrValidators } from '@stafr/modules';
import { IInviteForm } from './invite-form.interface';

export const InviteFormValidators: Record<keyof IInviteForm, IFormValidator[]> =
  {
    email: [
      {
        validator: StafrValidators.required(),
        message: 'forms.invite.email.required',
      },
      {
        validator: StafrValidators.email(),
        message: 'forms.invite.email.isEmail',
      },
    ],
    role: [
      {
        validator: StafrValidators.required(),
        message: 'forms.invite.role.required',
      },
    ],
  };
