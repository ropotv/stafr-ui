import { AccountRole } from '@stafr/core';

export interface ITeamMemberForm {
  id: number;
  fullName: string;
  role: AccountRole;
}
