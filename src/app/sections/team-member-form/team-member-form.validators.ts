import { IFormValidator, StafrValidators } from '@stafr/modules';
import { ITeamMemberForm } from './team-member-form.interface';

export const TeamMemberFormValidators: Record<
  keyof ITeamMemberForm,
  IFormValidator[]
> = {
  id: [],
  fullName: [
    {
      validator: StafrValidators.required(),
      message: 'Please enter team-member full name',
    },
  ],
  role: [
    {
      validator: StafrValidators.required(),
      message: 'Please select team-member role',
    },
  ],
};
