import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { StafrFormsModule } from '@stafr/modules';

import { TeamMemberFormComponent } from './team-member-form.component';

@NgModule({
  imports: [CommonModule, StafrFormsModule, TranslateModule],
  declarations: [TeamMemberFormComponent],
  exports: [TeamMemberFormComponent],
})
export class TeamMemberFormModule {}
