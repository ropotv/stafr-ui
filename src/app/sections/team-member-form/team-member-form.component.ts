import {
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';

import { FormSection, StatusBoxColors, TranslateEnums } from '@stafr/modules';

import { TeamMemberFormValidators } from './team-member-form.validators';
import { ITeamMemberForm } from './team-member-form.interface';

@Component({
  selector: 'section-team-member-form',
  templateUrl: './team-member-form.component.html',
  styleUrls: ['./team-member-form.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TeamMemberFormComponent extends FormSection<ITeamMemberForm> {
  public colors = StatusBoxColors;
  public validators = TeamMemberFormValidators;
  public roles$ = this._translateEnums.roles$;

  constructor(private _translateEnums: TranslateEnums) {
    super();
  }
}
