import {
  CandidateStatus,
  CountryType,
  ICandidateAttachment,
  TimeInDays,
} from '@stafr/core';
import { IFileUploadBoxValue, IRate } from '@stafr/modules';

export interface ICandidateForm {
  number: number;
  name: string;
  assigneeId: number;
  contact: string;
  country: CountryType;
  status: CandidateStatus;
  profession: string;
  experience: TimeInDays;
  rate: IRate;
  skill: string;
  skills: string[];
  attachments: IFileUploadBoxValue<ICandidateAttachment>;
}
