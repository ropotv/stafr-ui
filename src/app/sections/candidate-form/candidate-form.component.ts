import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  ViewEncapsulation,
} from '@angular/core';
import { debounceTime, filter, map, switchMap } from 'rxjs/operators';
import { FormArray, FormBuilder } from '@angular/forms';
import { Subject, Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

import {
  FormGroupComponent,
  FormSection,
  StatusBoxColors,
  TranslateEnums,
  TranslateUtilsService,
} from '@stafr/modules';
import {
  CandidatesService,
  ProfessionsService,
  SkillsService,
  TeamProvider,
} from '@stafr/api';
import { FileFormats } from '@stafr/core';

import { CandidateFormValidators } from './candidate-form.validators';
import { ICandidateForm } from './candidate-form.interface';

interface ICheckExist {
  key: string;
  group: FormGroupComponent;
  message: string;
}

@Component({
  selector: 'section-candidate-form',
  templateUrl: './candidate-form.component.html',
  styleUrls: ['./candidate-form.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CandidateFormComponent
  extends FormSection<ICandidateForm>
  implements OnDestroy
{
  private _subscription: Subscription;
  public validators = CandidateFormValidators;
  public colors = StatusBoxColors;
  public format = [...FileFormats.document, ...FileFormats.image];

  public onCheckExist = new Subject<ICheckExist>();
  public statuses$ = this._translateEnums.candidates$;
  public countries$ = this._translateEnums.countries$;
  public assignees$ = this._team.getMany().pipe(map(([res]) => res));
  public professions$ = this._professionSvc.getMany();
  public skills$ = this._skillSvc.getMany();

  constructor(
    private _translateEnums: TranslateEnums,
    private _team: TeamProvider,
    private _professionSvc: ProfessionsService,
    private _skillSvc: SkillsService,
    private _fb: FormBuilder,
    private _candidateSvc: CandidatesService,
    private _translateSvc: TranslateService,
    private _translateUtils: TranslateUtilsService
  ) {
    super();
    this._subscription = this.onCheckExist
      .pipe(
        debounceTime(1000),
        filter((res) => res.group.component.value != null),
        switchMap((res) =>
          this._candidateSvc
            .count({
              filter: `candidate.${res.key}~ieq~${res.group.component.value}~AND `,
            })
            .pipe(
              map((amount) => ({
                group: res.group,
                message: res.message,
                amount,
              }))
            )
        ),
        switchMap((res) => this._translateUtils.change$.pipe(map(() => res)))
      )
      .subscribe((res) => {
        res.group.setState(
          res.amount > 0 ? 'warning' : null,
          res.amount > 0 ? this._translateSvc.instant(res.message) : null
        );
      });
  }

  public onSkillAdd(): void {
    const { skill } = this.formValue;
    if (!skill) {
      return;
    }
    this._skills.push(this._fb.control(skill));
    this.form.patchValue({ skill: '' });
  }

  public onSkillRemove(idx: number): void {
    this._skills.removeAt(idx);
  }

  public ngOnDestroy(): void {
    this._subscription?.unsubscribe();
  }

  private get _skills(): FormArray {
    return this.form.get('skills') as any;
  }
}
