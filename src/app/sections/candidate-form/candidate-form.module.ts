import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import {
  AvatarModule,
  ButtonModule,
  ExcludeUtilsModule,
  StafrFormsModule,
  TimeUtilsModule,
} from '@stafr/modules';

import { CandidateFormComponent } from './candidate-form.component';

@NgModule({
  imports: [
    CommonModule,
    StafrFormsModule,
    AvatarModule,
    ButtonModule,
    ExcludeUtilsModule,
    TimeUtilsModule,
    TranslateModule,
  ],
  declarations: [CandidateFormComponent],
  exports: [CandidateFormComponent],
})
export class CandidateFormModule {}
