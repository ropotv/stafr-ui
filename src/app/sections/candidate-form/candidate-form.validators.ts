import { IFormValidator, StafrValidators } from '@stafr/modules';

import { ICandidateForm } from './candidate-form.interface';

export const CandidateFormValidators: Record<
  keyof ICandidateForm,
  IFormValidator[]
> = {
  number: [],
  name: [
    {
      validator: StafrValidators.required(),
      message: 'forms.candidate.name.required',
    },
  ],
  assigneeId: [
    {
      validator: StafrValidators.required(),
      message: 'forms.candidate.assignee.required',
    },
  ],
  contact: [
    {
      validator: StafrValidators.required(),
      message: 'forms.candidate.contact.required',
    },
  ],
  country: [
    {
      validator: StafrValidators.required(),
      message: 'forms.candidate.country.required',
    },
  ],
  status: [
    {
      validator: StafrValidators.required(),
      message: 'forms.candidate.status.required',
    },
  ],
  profession: [
    {
      validator: StafrValidators.required(),
      message: 'forms.candidate.profession.required',
    },
  ],
  experience: [
    {
      validator: StafrValidators.required(),
      message: 'forms.candidate.experience.required',
    },
  ],
  rate: [
    {
      validator: StafrValidators.rateRequired(),
      message: 'forms.candidate.rate.required',
    },
  ],
  skill: [],
  skills: [],
  attachments: [],
};
