import {
  animate,
  animateChild,
  query,
  style,
  transition,
  trigger,
} from '@angular/animations';

export const SliderAnimations = [
  trigger('wrapper', [
    transition(':leave', [
      query('@slide', [animateChild()], { optional: true }),
    ]),
  ]),
  trigger('slide', [
    transition(':enter', [
      style({ right: '-80%' }),
      animate('0.3s ease-out', style({ right: 0 })),
    ]),
    transition(':leave', [
      style({ right: 0 }),
      animate('0.3s ease-out', style({ right: '-80%' })),
    ]),
  ]),
];
