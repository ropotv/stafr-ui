import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewEncapsulation,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { SliderAnimations } from './slider.animations';

@Component({
  selector: 'stafr-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: SliderAnimations,
})
export class SliderComponent {
  @Input()
  public opened: boolean;

  @Input()
  public caption: string;

  @Input()
  public sub: string;

  @Input()
  public width: string = '500px';

  @Input()
  public routeBack: string;

  @Output()
  public closed = new EventEmitter<void>();

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _cdr: ChangeDetectorRef
  ) {}

  public close(): void {
    this.opened = false;
    this._cdr.detectChanges();
  }

  public onClose(): void {
    this.closed.emit();
    if (this.routeBack) {
      this._router.navigate([this.routeBack], { relativeTo: this._route });
    }
  }
}
