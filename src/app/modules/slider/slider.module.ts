import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ButtonModule } from '@stafr/modules/button';

import { SliderComponent } from './slider.component';

@NgModule({
  imports: [CommonModule, ButtonModule],
  declarations: [SliderComponent],
  exports: [SliderComponent],
})
export class SliderModule {}
