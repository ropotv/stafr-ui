import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { distinctUntilKeyChanged, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { KeyValue } from '@angular/common';

import {
  AccountRole,
  CandidateStatus,
  CountryType,
  findFrequent,
} from '@stafr/core';
import { AppStore } from '@stafr/app.store';

import { MapyService } from '../mapy';
import { TranslateUtilsService } from './translate.service';

@Injectable({
  providedIn: 'root',
})
export class TranslateEnums {
  public candidates$: Observable<KeyValue<CandidateStatus, string>[]> =
    this._translateUtils.change$.pipe(
      map(() => [
        {
          value: CandidateStatus.Unavailable,
          key: this._translateSvc.instant(
            `enum.${CandidateStatus.Unavailable}`
          ),
        },
        {
          value: CandidateStatus.Unknown,
          key: this._translateSvc.instant(`enum.${CandidateStatus.Unknown}`),
        },
        {
          value: CandidateStatus.Interested,
          key: this._translateSvc.instant(`enum.${CandidateStatus.Interested}`),
        },
        {
          value: CandidateStatus.InReview,
          key: this._translateSvc.instant(`enum.${CandidateStatus.InReview}`),
        },
      ])
    );

  public roles$: Observable<KeyValue<AccountRole, string>[]> =
    this._translateUtils.change$.pipe(
      map(() => [
        {
          value: AccountRole.Admin,
          key: this._translateSvc.instant(`enum.${AccountRole.Admin}`),
        },
        {
          value: AccountRole.Manager,
          key: this._translateSvc.instant(`enum.${AccountRole.Manager}`),
        },
        {
          value: AccountRole.HR,
          key: this._translateSvc.instant(`enum.${AccountRole.HR}`),
        },
      ])
    );

  public countries$: Observable<KeyValue<string, CountryType>[]> =
    this._store.state$.pipe(
      distinctUntilKeyChanged('settings'),
      map((s) => {
        const frequent = findFrequent(s.settings.preferredCountries);
        const enums = Object.values(CountryType);
        const top = frequent.filter((c) => enums.some((e) => e === c));

        const countries = Object.values(CountryType).filter(
          (c) => !frequent.includes(c)
        );
        const res = [...top, ...countries];
        return res.map((v: CountryType) => ({
          key: this._mapy.transform(v, 'country'),
          value: v,
        }));
      })
    );

  constructor(
    private _store: AppStore,
    private _translateSvc: TranslateService,
    private _translateUtils: TranslateUtilsService,
    private _mapy: MapyService
  ) {}
}
