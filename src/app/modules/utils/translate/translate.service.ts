import { Injectable } from '@angular/core';
import { merge, Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { map, startWith } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class TranslateUtilsService {
  public change$: Observable<void> = merge(
    this._translate.onDefaultLangChange,
    this._translate.onLangChange,
    this._translate.onTranslationChange
  ).pipe(
    startWith(''),
    map(() => {})
  );

  constructor(private _translate: TranslateService) {}
}
