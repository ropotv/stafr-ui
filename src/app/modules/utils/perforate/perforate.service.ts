import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class PerforateUtilsService {
  public transform(value: any, key: string): any {
    if (!key) {
      return value;
    }
    return key.split('.').reduce((prev, curr) => {
      return prev ? prev[curr] : null;
    }, value);
  }
}
