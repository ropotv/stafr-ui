import { Pipe, PipeTransform } from '@angular/core';

import { PerforateUtilsService } from './perforate.service';

@Pipe({
  name: 'perforate',
})
export class PerforatePipe implements PipeTransform {
  constructor(private _service: PerforateUtilsService) {}

  public transform(value: any, key: string): any {
    return this._service.transform(value, key);
  }
}
