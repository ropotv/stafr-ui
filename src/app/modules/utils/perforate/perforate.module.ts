import { NgModule } from '@angular/core';

import { PerforatePipe } from './perforate.pipe';

@NgModule({
  declarations: [PerforatePipe],
  exports: [PerforatePipe],
})
export class PerforateUtilsModule {}
