export type DocumentExtension = 'pdf' | 'doc' | 'docx' | 'ppt' | 'pptx';
export type ImageExtension = 'jpg' | 'jpeg' | 'png';

export type ExtensionType = DocumentExtension | ImageExtension | string;
