import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExtensionPipe } from './extension.pipe';

@NgModule({
  imports: [CommonModule],
  declarations: [ExtensionPipe],
  exports: [ExtensionPipe],
})
export class ExtensionUtilsModule {}
