import { ExtensionType } from './extension.type';

export function extensionUtil(value: string): ExtensionType {
  if (!value) {
    return '';
  }
  return value.substring(value.lastIndexOf('.') + 1);
}
