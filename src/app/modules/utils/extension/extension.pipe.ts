import { Pipe, PipeTransform } from '@angular/core';

import { ExtensionType } from './extension.type';
import { extensionUtil } from './extension.util';

@Pipe({
  name: 'extension',
})
export class ExtensionPipe implements PipeTransform {
  public transform(value: string): ExtensionType {
    return extensionUtil(value);
  }
}
