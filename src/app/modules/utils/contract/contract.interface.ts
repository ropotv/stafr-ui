export interface IContractInfo {
  hourlyIncome: number;
  hourlyEmployee: number;
  hourlyTaxes: number;
  hourlyProfit: number;
  monthIncome: number;
  monthEmployee: number;
  monthTaxes: number;
  monthProfit: number;
  monthExpenses: number;
}
