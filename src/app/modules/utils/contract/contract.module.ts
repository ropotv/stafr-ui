import { NgModule } from '@angular/core';

import { ContractProfitPipe } from './contract-profit.pipe';
import { ContractInfoPipe } from './contract-info.pipe';
import { ContractMonthInfoPipe } from './month-info.pipe';

@NgModule({
  declarations: [ContractProfitPipe, ContractInfoPipe, ContractMonthInfoPipe],
  exports: [ContractProfitPipe, ContractInfoPipe, ContractMonthInfoPipe],
})
export class ContractUtilsModule {}
