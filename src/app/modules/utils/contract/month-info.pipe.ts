import { Pipe, PipeTransform } from '@angular/core';

import { IContractMonth } from '@stafr/core';

import { ContractUtilsService } from './contract.service';
import { IContractInfo } from './contract.interface';

@Pipe({
  name: 'contractMonthInfo',
})
export class ContractMonthInfoPipe implements PipeTransform {
  constructor(private _service: ContractUtilsService) {}

  public transform(month: IContractMonth): IContractInfo {
    return this._service.getMonthInfo(month);
  }
}
