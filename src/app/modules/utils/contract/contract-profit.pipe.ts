import { Pipe, PipeTransform } from '@angular/core';

import { IContract } from '@stafr/core';
import { AppStore } from '@stafr/app.store';

import { ContractUtilsService } from './contract.service';

@Pipe({
  name: 'contractProfit',
})
export class ContractProfitPipe implements PipeTransform {
  constructor(
    private _service: ContractUtilsService,
    private _store: AppStore
  ) {}

  public transform(contract: IContract): number {
    return this._service.getContractProfit(contract);
  }
}
