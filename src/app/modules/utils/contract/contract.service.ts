import { Injectable } from '@angular/core';

import { IContract, IContractMonth, monthDiff } from '@stafr/core';
import { ExchangeUtilsService } from '../exchange';
import { IContractInfo } from './contract.interface';

@Injectable({ providedIn: 'root' })
export class ContractUtilsService {
  constructor(private _exchange: ExchangeUtilsService) {}

  public getMonthInfo(month: IContractMonth): IContractInfo {
    const hourlyEmployee = this._exchange.exchange(
      month.candidateRate,
      month.candidateCurrency
    );
    const hourlyIncome = this._exchange.exchange(
      month.orderRate,
      month.orderCurrency
    );
    const monthExpenses = this._exchange.exchange(
      month.expensesAmount,
      month.expensesCurrency
    );

    const hourlyTaxes = hourlyIncome * (month.taxes / 100);
    const hourlyProfit = hourlyIncome - hourlyEmployee - hourlyTaxes;

    return {
      hourlyIncome,
      hourlyEmployee,
      hourlyProfit,
      hourlyTaxes,
      monthIncome: hourlyIncome * month.workingHours,
      monthEmployee: hourlyEmployee * month.workingHours,
      monthTaxes: hourlyTaxes * month.workingHours,
      monthProfit: hourlyProfit * month.workingHours - monthExpenses,
      monthExpenses,
    };
  }

  public getContractInfo(contract: IContract): IContractInfo {
    const months = contract.months
      .filter((m) => monthDiff(new Date(m.date), new Date()) === 0)
      .sort((a, b) => (new Date(a.date) > new Date(b.date) ? 1 : -1));

    return months.length
      ? this.getMonthInfo(months.first())
      : {
          hourlyIncome: 0,
          hourlyEmployee: 0,
          hourlyProfit: 0,
          hourlyTaxes: 0,
          monthIncome: 0,
          monthEmployee: 0,
          monthProfit: 0,
          monthTaxes: 0,
          monthExpenses: 0,
        };
  }

  public getContractProfit(contract: IContract): number {
    return contract.months
      .map((m) => this.getMonthInfo(m)?.monthProfit)
      .reduce((a, b) => a + b, 0);
  }
}
