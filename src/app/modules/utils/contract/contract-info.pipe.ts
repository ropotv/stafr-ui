import { Pipe, PipeTransform } from '@angular/core';

import { IContract } from '@stafr/core';

import { ContractUtilsService } from './contract.service';
import { IContractInfo } from './contract.interface';

@Pipe({
  name: 'contractInfo',
})
export class ContractInfoPipe implements PipeTransform {
  constructor(private _service: ContractUtilsService) {}

  public transform(contract: IContract): IContractInfo {
    return this._service.getContractInfo(contract);
  }
}
