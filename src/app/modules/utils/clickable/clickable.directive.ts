import {
  Directive,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  Renderer2,
} from '@angular/core';

export enum KeyCodeType {
  Backspace = 8,
  Tab = 9,
  Enter = 13,
  Shift = 16,
  Ctrl = 17,
  Alt = 18,
  CapsLock = 20,
  Escape = 27,
  Space = 32,
}

@Directive({ selector: '[clickable]' })
export class ClickableDirective implements OnInit, OnDestroy {
  private readonly _element: HTMLElement;

  @Input()
  public role = 'button';

  constructor(
    elRef: ElementRef<HTMLElement>,
    private readonly _renderer: Renderer2
  ) {
    this._element = elRef.nativeElement;
  }

  private listener = (event: KeyboardEvent) => {
    if ([KeyCodeType.Enter, KeyCodeType.Space].includes(event.keyCode)) {
      this._element.click();
    }
  };

  public ngOnInit(): void {
    this._renderer.setAttribute(this._element, 'tabindex', '0');
    this._renderer.setAttribute(this._element, 'role', this.role);
    this._element.addEventListener('keydown', this.listener);
  }

  public ngOnDestroy(): void {
    this._element.removeEventListener('keydown', this.listener);
  }
}
