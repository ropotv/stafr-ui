import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { TimeInDays } from '@stafr/core';

const TIME_MAP = {
  y: 360,
  m: 30,
  w: 7,
  d: 1,
};

@Injectable({ providedIn: 'root' })
export class TimeService {
  constructor(private _translate: TranslateService) {}

  public toHuman(value: TimeInDays): string {
    let res = '';

    for (const key in TIME_MAP) {
      if (TIME_MAP.hasOwnProperty(key)) {
        const proto = TIME_MAP[key];
        const integer = Math.floor(value / proto);
        value = value % proto;

        if (integer > 0) {
          res += `${integer}${key} `;
        }
      }
    }

    return res.trim();
  }

  public fromHuman(value: string): TimeInDays {
    if (!value) {
      return null;
    }
    let days = 0;

    value.split(' ').forEach((part) => {
      const amount = Number(part.substring(0, part.length - 1));
      const metric = part.charAt(part.length - 1);
      days += TIME_MAP[metric] * amount;
    });

    return Number.isNaN(days) ? null : days;
  }

  public timeSince(date: string | Date): string {
    date = new Date(date);
    const seconds = Math.floor((new Date().getTime() - date.getTime()) / 1000);

    let interval = seconds / 31536000;
    if (interval > 1) {
      const value = Math.floor(interval);
      if (value === 1) {
        return this._translate.instant('timeSince.year');
      }
      return this._translate.instant('timeSince.years', { value });
    }

    interval = seconds / 2592000;
    if (interval > 1) {
      const value = Math.floor(interval);
      if (value === 1) {
        return this._translate.instant('timeSince.month');
      }
      return this._translate.instant('timeSince.months', { value });
    }

    interval = seconds / 86400;
    if (interval > 1) {
      const value = Math.floor(interval);
      if (value === 1) {
        return this._translate.instant('timeSince.day');
      }
      return this._translate.instant('timeSince.days', { value });
    }

    interval = seconds / 3600;
    if (interval > 1) {
      const value = Math.floor(interval);
      if (value === 1) {
        return this._translate.instant('timeSince.hour');
      }
      return this._translate.instant('timeSince.hours', { value });
    }

    interval = seconds / 60;
    if (interval > 1) {
      const value = Math.floor(interval);
      if (value === 1) {
        return this._translate.instant('timeSince.minute');
      }
      return this._translate.instant('timeSince.minutes', { value });
    }

    {
      const value = Math.floor(seconds);
      if (value === 0) {
        return this._translate.instant('timeSince.now');
      }

      return this._translate.instant('timeSince.seconds', { value });
    }
  }
}
