import { Pipe, PipeTransform } from '@angular/core';

import { TimeInDays } from '@stafr/core';

import { TimeService } from './time.service';

@Pipe({
  name: 'stafrTime',
})
export class TimePipe implements PipeTransform {
  constructor(private _service: TimeService) {}

  public transform(value: TimeInDays): string {
    return this._service.toHuman(value);
  }
}
