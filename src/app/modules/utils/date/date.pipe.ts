import { Pipe, PipeTransform } from '@angular/core';

import { DateFormat } from '@stafr/core';
import { DateService } from './date.service';

@Pipe({
  name: 'stafrDate',
})
export class DatePipe implements PipeTransform {
  constructor(private _service: DateService) {}

  public transform(value: string | Date, format: DateFormat): string {
    return this._service.transform(value, format);
  }
}
