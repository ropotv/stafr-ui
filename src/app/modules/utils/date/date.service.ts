import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { DateFormat, MaxDate } from '@stafr/core';
import { AppStore } from '@stafr/app.store';
import { TimeService } from '@stafr/modules/utils/time';

@Injectable({ providedIn: 'root' })
export class DateService {
  constructor(
    private _translate: TranslateService,
    private _time: TimeService,
    private _store: AppStore
  ) {}

  public transform(value: string | Date, format: DateFormat): string {
    const serverOffset = this._store.get.serverTimezoneOffset || 0;
    const offset = serverOffset * 60 * 1000;
    value = new Date(new Date(value).getTime() - offset);

    switch (format) {
      case 'month': {
        return this._translate.instant(`date.month.${value.getMonth()}`);
      }
      case 'full-date': {
        if (value.getTime() >= MaxDate.getTime()) {
          return this._translate.instant('date.present');
        }
        const month = this._translate.instant(`date.month.${value.getMonth()}`);
        const day = String(value.getDate()).padStart(2, '0');
        const year = value.getFullYear();

        return `${day} ${month} ${year}`;
      }
      case 'month-year': {
        const month = this._translate.instant(`date.month.${value.getMonth()}`);
        const year = value.getFullYear();

        return `${month} ${year}`;
      }
      case 'time-ago': {
        return this._time.timeSince(value);
      }
      default: {
        return value.toDateString();
      }
    }
  }
}
