import { Pipe, PipeTransform } from '@angular/core';

import { SlugifyUtilsService } from './slugify.service';

@Pipe({
  name: 'slugify',
})
export class SlugifyPipe implements PipeTransform {
  constructor(private _service: SlugifyUtilsService) {}

  public transform(value: string): string {
    return this._service.transform(value);
  }
}
