import { NgModule } from '@angular/core';

import { MapyPipe } from './mapy.pipe';

@NgModule({
  declarations: [MapyPipe],
  exports: [MapyPipe],
})
export class MapyModule {}
