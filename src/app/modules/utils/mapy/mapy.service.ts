import { Injectable } from '@angular/core';
import * as mapy from 'assets/mapy/mapy.json';

import { MapySource } from './mapy.source';

@Injectable({
  providedIn: 'root',
})
export class MapyService {
  public transform(value: string, source: MapySource): string {
    const data = mapy[source];
    if (!data || !value) {
      return '';
    }
    const result = data[value];
    if (!result) {
      return value;
    }
    return result;
  }
}
