import { Pipe, PipeTransform } from '@angular/core';

import { MapyService } from './mapy.service';
import { MapySource } from './mapy.source';

@Pipe({
  name: 'mapy',
})
export class MapyPipe implements PipeTransform {
  constructor(private _service: MapyService) {}

  public transform(value: string, source: MapySource): string {
    return this._service.transform(value, source);
  }
}
