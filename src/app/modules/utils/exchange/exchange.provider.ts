import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ExchangeRate } from './exchange.type';

@Injectable({ providedIn: 'root' })
export class ExchangeProvider {
  private _endpoint = '/exchange';

  constructor(private _http: HttpClient) {}

  public getRates(): Observable<ExchangeRate> {
    const url = this._endpoint;
    return this._http.get<ExchangeRate>(url);
  }
}
