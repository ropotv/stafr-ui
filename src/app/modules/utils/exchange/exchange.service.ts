import { Injectable } from '@angular/core';

import { CurrencyType } from '@stafr/core/models/currency';
import { AppStore } from '@stafr/app.store';

@Injectable({ providedIn: 'root' })
export class ExchangeUtilsService {
  constructor(private _store: AppStore) {}

  public exchange(amount: string | number, from: CurrencyType): number {
    return Number(this._store.get.exchangeRate[from]) * Number(amount);
  }
}
