import { CurrencyType } from '@stafr/core';

export type ExchangeRate = {
  [k in CurrencyType]: number;
};
