export type PositionX = 'left' | 'right';
export type PositionY = 'top' | 'bottom';

export interface IPositionStrategy {
  originX: PositionX;
  originY: PositionY;
  overlayX: PositionX;
  overlayY: PositionY;
  offsetY?: number;
  offsetX?: number;
}

export interface IPositionFallback {
  top: string;
  left: string;
}

export interface IPositionBounds {
  top: number;
  left: number;
  right: number;
  bottom: number;
}
