import {
  AfterContentInit,
  Directive,
  ElementRef,
  Input,
  Renderer2,
} from '@angular/core';

import { IPositionFallback, IPositionStrategy } from './position.interface';
import { CalculatePosition } from './position.algorighm';

@Directive({
  selector: '[positionBy]',
})
export class PositionDirective implements AfterContentInit {
  @Input()
  public positionBy: HTMLElement;

  @Input()
  public strategy: IPositionStrategy;

  @Input()
  public fallback: IPositionFallback;

  constructor(
    private readonly _renderer: Renderer2,
    private readonly _el: ElementRef<HTMLDivElement>
  ) {}

  public ngAfterContentInit(): void {
    CalculatePosition(
      this.strategy,
      this._el.nativeElement,
      this.positionBy,
      {
        left: 300,
      },
      this.fallback
    );
  }
}
