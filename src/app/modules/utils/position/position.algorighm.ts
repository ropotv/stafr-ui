import {
  IPositionBounds,
  IPositionFallback,
  IPositionStrategy,
} from './position.interface';

export const CalculatePosition = (
  strategy: IPositionStrategy,
  element: HTMLElement,
  trigger: HTMLElement,
  bounds: Partial<IPositionBounds>,
  fallback?: IPositionFallback
) => {
  const docWidth = document.documentElement.clientWidth;
  const docHeight = document.documentElement.clientHeight;

  if (trigger && strategy) {
    const offsetX = strategy.offsetX || 0;
    const offsetY = strategy.offsetY || 0;
    const boundLeft = bounds.left || 0;
    const boundRight = bounds.right || 0;
    const boundTop = bounds.top || 0;
    const boundBottom = bounds.bottom || 0;
    const { overlayX, overlayY, originX, originY } = strategy;
    const { offsetWidth, offsetHeight } = trigger;
    let { top, left, bottom, right } = trigger.getBoundingClientRect();
    if (overlayX === 'left') {
      if (originX === 'right') {
        left += offsetWidth;
      }
      if (left + element.offsetWidth > docWidth - boundRight) {
        left = docWidth - element.offsetWidth - boundRight;
      }

      left += offsetX;
      element.style.left = `${left}px`;
      element.style.right = 'auto';
    }

    if (overlayX === 'right') {
      right = docWidth - right;
      if (originX === 'left') {
        right += offsetWidth;
      }
      if (right + element.offsetWidth > docWidth - boundLeft) {
        right = docWidth - element.offsetWidth - boundLeft;
      }

      right += offsetX;
      element.style.right = `${right}px`;
      element.style.left = 'auto';
    }

    if (overlayY === 'top') {
      if (originY === 'bottom') {
        top += offsetHeight;
      }
      if (top + element.offsetHeight > docHeight - boundBottom) {
        top = docHeight - element.offsetHeight - boundBottom;
      }

      top += offsetY;
      element.style.top = `${top}px`;
      element.style.bottom = 'auto';
    }

    if (overlayY === 'bottom') {
      bottom = docHeight - top;
      if (originY === 'bottom') {
        bottom -= offsetHeight;
      }
      if (bottom + element.offsetHeight > docHeight - boundTop) {
        bottom = docHeight - element.offsetHeight - boundTop;
      }

      bottom += offsetY;
      element.style.bottom = `${bottom}px`;
      element.style.top = 'auto';
    }
  } else {
    element.style.top = fallback?.top || '50%';
    element.style.left = fallback?.left || '50%';
    element.style.transform = 'translate(-50%, -50%)';
  }
};
