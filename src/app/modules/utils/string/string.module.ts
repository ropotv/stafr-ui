import { NgModule } from '@angular/core';

import { IsEmailPipe } from './is-email.pipe';
import { IsUrlPipe } from './is-url.pipe';

@NgModule({
  declarations: [IsEmailPipe, IsUrlPipe],
  exports: [IsEmailPipe, IsUrlPipe],
})
export class StringUtilsModule {}
