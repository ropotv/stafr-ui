import { Pipe, PipeTransform } from '@angular/core';

import { StringUtilsService } from './string.service';

@Pipe({
  name: 'isEmail',
})
export class IsEmailPipe implements PipeTransform {
  constructor(private _service: StringUtilsService) {}

  public transform(value: string): boolean {
    return this._service.isEmail(value);
  }
}
