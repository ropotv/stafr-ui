import { Pipe, PipeTransform } from '@angular/core';

import { StringUtilsService } from './string.service';

@Pipe({
  name: 'isUrl',
})
export class IsUrlPipe implements PipeTransform {
  constructor(private _service: StringUtilsService) {}

  public transform(value: string): boolean {
    return this._service.isUrl(value);
  }
}
