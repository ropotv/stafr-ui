import { Injectable } from '@angular/core';

const EMAIL_REGEX =
  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const URL_REGEX =
  /(?:^|[^@\.\w-])([a-z0-9]+:\/\/)?(\w(?!ailto:)\w+:\w+@)?([\w.-]+\.[a-z]{2,4})(:[0-9]+)?(\/.*)?(?=$|[^@\.\w-])/im;

@Injectable({
  providedIn: 'root',
})
export class StringUtilsService {
  public isEmail(value: string): boolean {
    return EMAIL_REGEX.test(value?.toLowerCase());
  }

  public isUrl(value: string): boolean {
    return URL_REGEX.test(value?.toLowerCase());
  }
}
