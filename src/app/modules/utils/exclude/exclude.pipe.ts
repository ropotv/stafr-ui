import { Pipe, PipeTransform } from '@angular/core';

import { ExcludeUtilsService } from './exclude.service';

@Pipe({
  name: 'exclude',
})
export class ExcludePipe implements PipeTransform {
  constructor(private _service: ExcludeUtilsService) {}

  public transform(
    value: any[],
    exclude: any[],
    valueKey: string,
    excludeKey: string = valueKey
  ): any[] {
    return this._service.transform(value, exclude, valueKey, excludeKey);
  }
}
