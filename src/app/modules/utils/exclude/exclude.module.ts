import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExcludePipe } from './exclude.pipe';

@NgModule({
  imports: [CommonModule],
  declarations: [ExcludePipe],
  exports: [ExcludePipe],
})
export class ExcludeUtilsModule {}
