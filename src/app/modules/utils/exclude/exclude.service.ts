import { Injectable } from '@angular/core';

import { PerforateUtilsService } from '@stafr/modules/utils/perforate';

@Injectable({
  providedIn: 'root',
})
export class ExcludeUtilsService {
  constructor(private _perforate: PerforateUtilsService) {}

  public transform(
    value: any[],
    exclude: any[],
    valueKey: string,
    excludeKey: string = valueKey
  ): any[] {
    if (!value || !exclude?.length) {
      return value;
    }

    return value.filter((el) => {
      const comparedBy = this._perforate.transform(el, valueKey);
      return !exclude.some(
        (v) => this._perforate.transform(v, excludeKey) === comparedBy
      );
    });
  }
}
