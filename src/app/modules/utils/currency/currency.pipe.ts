import { Pipe, PipeTransform } from '@angular/core';

import { CurrencyType } from '@stafr/core/models/currency';
import { AppStore } from '@stafr/app.store';

import { CurrencyUtilsService } from './currency.service';

@Pipe({
  name: 'stafrCurrency',
})
export class CurrencyPipe implements PipeTransform {
  constructor(
    private _service: CurrencyUtilsService,
    private _store: AppStore
  ) {}

  public transform(
    value: number | string,
    minimize = false,
    currency: CurrencyType = this._store.currency
  ): string {
    return this._service.transform(value, minimize, currency);
  }
}
