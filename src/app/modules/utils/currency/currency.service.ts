import { Injectable } from '@angular/core';
import { AppStore } from '@stafr/app.store';

import { CurrencySymbols } from './currency.symbols';

@Injectable({ providedIn: 'root' })
export class CurrencyUtilsService {
  private _metrics = {
    E: Math.pow(10, 18),
    P: Math.pow(10, 15),
    T: Math.pow(10, 12),
    B: Math.pow(10, 9),
    M: Math.pow(10, 6),
    k: Math.pow(10, 3),
  };

  constructor(private _store: AppStore) {}

  public transform(
    value: number | string,
    minimize = false,
    currency = this._store.currency
  ): string {
    const symbol = CurrencySymbols[currency];
    const val = minimize ? this._minimize(value) : this._format(value, 2);
    return symbol ? `${symbol}${val}` : `${val} ${currency}`;
  }

  private _minimize(value: number | string): string {
    for (const key in this._metrics) {
      if (value >= this._metrics[key]) {
        const response = Number(value) / this._metrics[key];
        return `${this._format(response)}${key}`;
      }
    }
    return this._format(value);
  }

  private _format(value: number | string, maximumFractionDigits = 1): string {
    return Number(value).toLocaleString('en', { maximumFractionDigits });
  }
}
