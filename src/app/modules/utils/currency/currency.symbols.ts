import { CurrencyType } from '@stafr/core/models/currency';

export const CurrencySymbols = {
  [CurrencyType.EUR]: '€',
  [CurrencyType.USD]: '$',
  [CurrencyType.RUB]: '₽',
};
