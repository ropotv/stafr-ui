import { Directive, Input, TemplateRef } from '@angular/core';

@Directive({ selector: '[dialogAction]' })
export class DialogAction {
  @Input()
  public dialogAction: 'between' | 'end';

  constructor(public template: TemplateRef<any>) {}
}
