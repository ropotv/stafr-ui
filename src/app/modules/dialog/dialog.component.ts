import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  EventEmitter,
  Input,
  Output,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { OverlayDirective } from '../overlay';
import { IDialog } from './dialog.interface';
import { DialogAction } from './dialog.action';

@Component({
  selector: 'stafr-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DialogComponent implements IDialog, AfterViewInit {
  @Input()
  public caption: string;

  @Input()
  public opened: boolean = false;

  @Input()
  public minWidth: string = '360px';

  @Input()
  public maxWidth: string = '920px';

  @Input()
  public routeBack: string;

  @Output()
  public closed = new EventEmitter();

  @ViewChild('overlay')
  private _overlay: OverlayDirective;

  @ContentChild(DialogAction)
  public action: DialogAction;

  constructor(private _router: Router, private _route: ActivatedRoute) {}

  public ngAfterViewInit(): void {
    if (this.opened) {
      this.open();
    }
  }

  public open(): void {
    this._overlay.open();
  }

  public close(): void {
    this._overlay.close();
    if (this.routeBack) {
      this._router.navigate([this.routeBack], { relativeTo: this._route });
    }
  }
}
