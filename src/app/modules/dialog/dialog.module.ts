import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OverlayModule } from '../overlay';
import { ButtonModule } from '../button';

import { DialogComponent } from './dialog.component';
import { DialogAction } from './dialog.action';

@NgModule({
  imports: [CommonModule, OverlayModule, ButtonModule],
  declarations: [DialogComponent, DialogAction],
  exports: [DialogComponent, DialogAction],
})
export class DialogModule {}
