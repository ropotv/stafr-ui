import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewEncapsulation,
} from '@angular/core';

@Component({
  selector: 'stafr-account-box',
  templateUrl: './account-box.component.html',
  styleUrls: ['./account-box.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AccountBoxComponent {
  @Input()
  public name: string;

  @Input()
  public image: string;

  @Input()
  public sub: string;

  @Input()
  public size: number = 40;

  @Input()
  public textWidth: string;
}
