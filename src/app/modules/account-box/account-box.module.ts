import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AvatarModule } from '../avatar';

import { AccountBoxComponent } from './account-box.component';

@NgModule({
  imports: [CommonModule, AvatarModule],
  declarations: [AccountBoxComponent],
  exports: [AccountBoxComponent],
})
export class AccountBoxModule {}
