import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import {
  ClickableUtilsModule,
  ExcludeUtilsModule,
  ExtensionUtilsModule,
  PerforateUtilsModule,
} from '@stafr/modules/utils';
import { IconModule } from '@stafr/modules/icon';
import { OverlayModule } from '@stafr/modules/overlay';
import { ButtonModule } from '@stafr/modules/button';

import { FormGroupComponent } from './form-group';
import {
  AutocompleteBoxComponent,
  AutocompleteBoxHeight,
  CheckBoxModule,
  FileDropDirective,
  FileUploadBoxComponent,
  ImageBoxComponent,
  MultiSelectBoxComponent,
  MultiSelectBoxHeight,
  RateBoxComponent,
  RateBoxHeight,
  SelectBoxComponent,
  SelectBoxHeight,
  SelectBoxTemplate,
  TextBoxComponent,
  TimeBoxComponent,
} from './form-controls';
import { FormComponent } from './form';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ClickableUtilsModule,
    IconModule,
    PerforateUtilsModule,
    OverlayModule,
    ButtonModule,
    FormsModule,
    ExcludeUtilsModule,
    ExtensionUtilsModule,
    TranslateModule,
    CheckBoxModule,
  ],
  declarations: [
    FormComponent,
    FormGroupComponent,
    TextBoxComponent,
    SelectBoxComponent,
    SelectBoxHeight,
    SelectBoxTemplate,
    MultiSelectBoxComponent,
    MultiSelectBoxHeight,
    RateBoxComponent,
    RateBoxHeight,
    FileUploadBoxComponent,
    FileDropDirective,
    ImageBoxComponent,
    TimeBoxComponent,
    AutocompleteBoxComponent,
    AutocompleteBoxHeight,
  ],
  exports: [
    ReactiveFormsModule,
    FormComponent,
    FormGroupComponent,
    CheckBoxModule,
    TextBoxComponent,
    SelectBoxComponent,
    SelectBoxTemplate,
    MultiSelectBoxComponent,
    RateBoxComponent,
    FileUploadBoxComponent,
    ImageBoxComponent,
    TimeBoxComponent,
    AutocompleteBoxComponent,
  ],
})
export class StafrFormsModule {}
