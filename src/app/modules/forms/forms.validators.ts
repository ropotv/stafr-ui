import { AbstractControl, ValidatorFn } from '@angular/forms';

import { extensionUtil } from '@stafr/modules/utils';

const EMAIL_REGEX = new RegExp('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$');
const ONE_MB_IN_BYTES = 1048576;

export class StafrValidators {
  public static required(): ValidatorFn {
    const func = (control: AbstractControl): any => {
      const value = control.value;
      let isValid;

      switch (typeof value) {
        case 'bigint':
        case 'number':
        case 'boolean':
          isValid = value !== null;
          break;
        case 'string':
          isValid = value.length > 0;
          break;
        case 'object':
          if (Array.isArray(value)) {
            isValid = value.length > 0;
          } else {
            isValid = !!value;
          }
          break;
        default:
          isValid = !!value;
          break;
      }

      if (!isValid) {
        return { required: true };
      }

      return null;
    };

    Object.defineProperty(func, 'name', { value: 'required' });
    return func;
  }

  public static isTrue(): ValidatorFn {
    const func = (control: AbstractControl): any => {
      if (!control.value) {
        return { isTrue: true };
      }

      return null;
    };

    Object.defineProperty(func, 'name', { value: 'isTrue' });
    return func;
  }

  public static email(): ValidatorFn {
    const func = (control: AbstractControl): any => {
      if (control.value && typeof control.value === 'string') {
        if (!EMAIL_REGEX.test(control.value)) {
          return { email: true };
        }
      }

      return null;
    };

    Object.defineProperty(func, 'name', { value: 'email' });
    return func;
  }

  public static rateRequired(): ValidatorFn {
    const func = (control: AbstractControl): any => {
      const amount = control.value?.amount;
      const currency = control.value?.currency;
      if (amount != null && currency != null) {
        return null;
      }

      return { rateRequired: true };
    };

    Object.defineProperty(func, 'name', { value: 'rateRequired' });
    return func;
  }

  public static fileFormat(format: string[]): ValidatorFn {
    const func = (control: AbstractControl): any => {
      const file = control.value?.file;
      if (!file?.name) {
        return null;
      }

      if (format.includes(extensionUtil(file.name))) {
        return null;
      }

      return { fileFormat: file };
    };

    Object.defineProperty(func, 'name', { value: 'fileFormat' });
    return func;
  }

  public static fileSize(maxSize: number): ValidatorFn {
    const func = (control: AbstractControl): any => {
      const file = control.value?.file;
      if (!file?.size) {
        return null;
      }

      if (file.size <= maxSize * ONE_MB_IN_BYTES) {
        return null;
      }

      return { fileSize: file };
    };

    Object.defineProperty(func, 'name', { value: 'fileSize' });
    return func;
  }
}
