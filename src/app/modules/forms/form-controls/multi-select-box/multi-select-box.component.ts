import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  OnInit,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

import { PerforateUtilsService } from '@stafr/modules/utils';
import { OverlayDirective } from '@stafr/modules/overlay';

import { CreateAccessorFor, FormControlBase } from '../form-control';

@Component({
  selector: 'multi-select-box',
  templateUrl: './multi-select-box.component.html',
  styleUrls: ['./multi-select-box.component.scss'],
  providers: CreateAccessorFor(MultiSelectBoxComponent),
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MultiSelectBoxComponent
  extends FormControlBase<any[]>
  implements OnInit, OnChanges
{
  public selected$: Observable<any[]>;
  public opened: boolean;

  @Input()
  public placeholder: string;

  @Input()
  public valueKey: string;

  @Input()
  public renderKey: string;

  @Input()
  public items: any[];

  @ViewChild('overlay')
  private _overlay: OverlayDirective;

  constructor(
    private _perforateUtils: PerforateUtilsService,
    cdr: ChangeDetectorRef
  ) {
    super(cdr);
  }

  public ngOnInit(): void {
    this._initControl();
  }

  public ngOnChanges(): void {
    super.ngOnChanges();
    this._initControl();
  }

  public onMenuToggle(target: HTMLElement): void {
    if (!this.disabled) {
      this._overlay.open(target);
      this.opened = !this.opened;
    }
  }

  public onItemRemove(item: any): void {
    const value = this._perforateUtils.transform(item, this.valueKey);
    this.value = this.value.filter((v) => v !== value);
  }

  public onItemAdd(item: any): void {
    const value = this._perforateUtils.transform(item, this.valueKey);
    this.value = this.value ? [...this.value, value] : [value];
    this._overlay.close();
  }

  public get formStyle(): Record<string, boolean> {
    return { ...super.formStyle, focused: this.opened || this.focused };
  }

  private _initControl(): void {
    this.selected$ = this.valueChanges.pipe(
      startWith([]),
      map((value) =>
        this.items.filter((item) =>
          value?.includes(this._perforateUtils.transform(item, this.valueKey))
        )
      )
    );
  }
}
