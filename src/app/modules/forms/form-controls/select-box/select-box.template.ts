import { Directive, TemplateRef } from '@angular/core';

@Directive({ selector: '[selectBoxTemplate]' })
export class SelectBoxTemplate {
  constructor(public template: TemplateRef<any>) {}
}
