import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChild,
  Input,
  OnChanges,
  OnInit,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';

import { PerforateUtilsService } from '@stafr/modules/utils';
import { OverlayDirective } from '@stafr/modules/overlay';

import { CreateAccessorFor, FormControlBase } from '../form-control';
import { SelectBoxTemplate } from './select-box.template';

@Component({
  selector: 'select-box',
  templateUrl: './select-box.component.html',
  styleUrls: ['./select-box.component.scss'],
  providers: CreateAccessorFor(SelectBoxComponent),
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectBoxComponent
  extends FormControlBase
  implements OnInit, OnChanges
{
  public opened: boolean;
  public selected$: Observable<any>;
  public items$ = new BehaviorSubject<any[]>([]);

  @Input()
  public placeholder: string;

  @Input()
  public valueKey: string;

  @Input()
  public renderKey: string;

  @Input()
  public set items(value: any[]) {
    if (value) {
      this.items$.next(value);
    }
  }

  @ViewChild('overlay')
  private _overlay: OverlayDirective;

  @ContentChild(SelectBoxTemplate)
  public template: SelectBoxTemplate;

  constructor(
    private _perforateUtils: PerforateUtilsService,
    cdr: ChangeDetectorRef
  ) {
    super(cdr);
  }

  public ngOnInit(): void {
    this._initControl();
  }

  public ngOnChanges(): void {
    super.ngOnChanges();
    this._initControl();
  }

  public onMenuToggle(target: HTMLElement): void {
    if (!this.disabled) {
      this._overlay.open(target);
      this.opened = !this.opened;
    }
  }

  public get formStyle(): Record<string, boolean> {
    return { ...super.formStyle, focused: this.opened || this.focused };
  }

  public onItemSelect(item: any): void {
    this.value = this._perforateUtils.transform(item, this.valueKey);
    this._overlay.close();
  }

  private _initControl(): void {
    this.selected$ = this.valueChanges.pipe(
      startWith(''),
      switchMap(() => this.items$),
      map((items) =>
        items?.find(
          (item) =>
            this._perforateUtils.transform(item, this.valueKey) === this.value
        )
      )
    );
  }
}
