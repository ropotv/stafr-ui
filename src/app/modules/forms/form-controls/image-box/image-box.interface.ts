export interface IImageBoxValue {
  file: File;
  image: string;
}
