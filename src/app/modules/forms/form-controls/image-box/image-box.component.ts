import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnChanges,
  OnDestroy,
  OnInit,
  ViewEncapsulation,
} from '@angular/core';
import { Subscription } from 'rxjs';

import { BrowserUtils, FileFormats } from '@stafr/core';

import { CreateAccessorFor, FormControlBase } from '../form-control';

import { IImageBoxValue } from './image-box.interface';

@Component({
  selector: 'image-box',
  templateUrl: './image-box.component.html',
  styleUrls: ['./image-box.component.scss'],
  providers: CreateAccessorFor(ImageBoxComponent),
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageBoxComponent
  extends FormControlBase<IImageBoxValue>
  implements OnInit, OnChanges, OnDestroy
{
  private _subscription: Subscription;

  constructor(private _browser: BrowserUtils, cdr: ChangeDetectorRef) {
    super(cdr);
  }

  public ngOnInit(): void {
    this._initControl();
  }

  public ngOnChanges(): void {
    super.ngOnChanges();
    this._initControl();
  }

  public ngOnDestroy(): void {
    this._subscription?.unsubscribe();
  }

  public browse(): void {
    this._browser
      .browseFile(FileFormats.image.map((el) => '.' + el).join(','), true)
      .then((res) => this.setFiles(res));
  }

  public async setFiles(files: FileList): Promise<void> {
    const file = files.item(0);
    const image: string = await new Promise((resolve) => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent<FileReader>) => {
        resolve(e.target.result as string);
      };
      reader.readAsDataURL(file);
    });

    this.value = { file, image };
    this.control.markAsDirty();
  }

  public clearValue(): void {
    this.value = { file: null, image: null };
  }

  private _initControl(): void {
    this._subscription?.unsubscribe();
    this._subscription = this.valueChanges.subscribe((val) => {
      this.detectChanges();
      if (!val) {
        this.value = { file: null, image: null };
      }
    });
  }
}
