import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'autocompleteBoxHeight',
})
export class AutocompleteBoxHeight implements PipeTransform {
  public transform(value: HTMLElement): number {
    if (!value) {
      return;
    }

    const { bottom } = value.getBoundingClientRect();
    return window.innerHeight - bottom - 30;
  }
}
