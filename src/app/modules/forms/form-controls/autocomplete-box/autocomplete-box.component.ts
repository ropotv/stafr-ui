import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';

import { OverlayDirective } from '@stafr/modules/overlay';

import { CreateAccessorFor, FormControlBase } from '../form-control';

@Component({
  selector: 'autocomplete-box',
  templateUrl: './autocomplete-box.component.html',
  styleUrls: ['./autocomplete-box.component.scss'],
  providers: CreateAccessorFor(AutocompleteBoxComponent),
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AutocompleteBoxComponent extends FormControlBase<string> {
  public onSearch$ = new Subject<string>();
  public searched$ = this.onSearch$.pipe(
    map((term) =>
      this.items?.filter((item) =>
        item?.toLowerCase()?.includes(term?.toLowerCase())
      )
    )
  );

  @Input()
  public placeholder: string;

  @Input()
  public items: string[];

  @ViewChild('overlay')
  private _overlay: OverlayDirective;

  public onItemSelect(item: string): void {
    this.value = item;
    this._overlay.close();
  }

  public openMenu(target: HTMLElement): void {
    if (!this.disabled) {
      this._overlay.open(target);
    }
  }
}
