import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewEncapsulation,
} from '@angular/core';

import { CreateAccessorFor, FormControlBase } from '../form-control';

@Component({
  selector: 'text-box',
  templateUrl: './text-box.component.html',
  styleUrls: ['./text-box.component.scss'],
  providers: CreateAccessorFor(TextBoxComponent),
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TextBoxComponent extends FormControlBase<string> {
  @Input()
  public type: 'text' | 'password' | 'email' | 'number' = 'text';

  @Input()
  public placeholder: string;
}
