import { CurrencyType } from '@stafr/core';

export interface IRate {
  amount: number;
  currency: CurrencyType;
}
