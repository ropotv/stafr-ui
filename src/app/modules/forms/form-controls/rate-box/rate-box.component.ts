import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnChanges,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { Subscription } from 'rxjs';

import { AllCurrencies, CurrencyType } from '@stafr/core';
import { OverlayDirective } from '@stafr/modules/overlay';
import { AppStore } from '@stafr/app.store';

import { CreateAccessorFor, FormControlBase } from '../form-control';
import { IRate } from './rate.interface';

@Component({
  selector: 'rate-box',
  templateUrl: './rate-box.component.html',
  styleUrls: ['./rate-box.component.scss'],
  providers: CreateAccessorFor(RateBoxComponent),
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RateBoxComponent
  extends FormControlBase<IRate>
  implements OnInit, OnDestroy, OnChanges
{
  private _subscription: Subscription;
  public currencies: CurrencyType[] = AllCurrencies;
  public opened: boolean;

  @ViewChild('overlay')
  private _overlay: OverlayDirective;

  @ViewChild('currencyBox')
  public currencyBox: ElementRef;

  constructor(cdr: ChangeDetectorRef, private _store: AppStore) {
    super(cdr);
  }

  public ngOnInit(): void {
    this._initControl();
  }

  public ngOnChanges(): void {
    super.ngOnChanges();
    this._initControl();
  }

  public ngOnDestroy(): void {
    this._subscription?.unsubscribe();
  }

  public onMenuToggle(target: HTMLElement): void {
    if (!this.disabled) {
      this._overlay.open(target);
      this.opened = !this.opened;
    }
  }

  public get formStyle(): Record<string, boolean> {
    return { ...super.formStyle, focused: this.opened || this.focused };
  }

  public onValueChange(amount: number): void {
    this.value = { ...this.value, amount };
  }

  public onCurrencySelect(currency: CurrencyType): void {
    this.value = { ...this.value, currency };
    this._overlay.close();
  }

  private _initControl(): void {
    this._subscription?.unsubscribe();
    this._subscription = this.valueChanges.subscribe(() => {
      if (!this.value) {
        this.value = { amount: null, currency: null };
      }
      this.detectChanges();
    });
  }
}
