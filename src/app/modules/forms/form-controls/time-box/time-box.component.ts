import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  ViewEncapsulation,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { startWith } from 'rxjs/operators';

import { TimeInDays } from '@stafr/core';
import { TimeService } from '@stafr/modules/utils';

import { CreateAccessorFor, FormControlBase } from '../form-control';

@Component({
  selector: 'time-box',
  templateUrl: './time-box.component.html',
  styleUrls: ['./time-box.component.scss'],
  providers: CreateAccessorFor(TimeBoxComponent),
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TimeBoxComponent
  extends FormControlBase<TimeInDays>
  implements OnInit, OnDestroy, OnChanges
{
  private _subscription: Subscription;
  public innerValue: string;

  @Input()
  public placeholder: string;

  constructor(private _time: TimeService, cdr: ChangeDetectorRef) {
    super(cdr);
  }

  public ngOnInit(): void {
    this._initControl();
  }

  public ngOnChanges(): void {
    super.ngOnChanges();
    this._initControl();
  }

  public ngOnDestroy(): void {
    this._subscription?.unsubscribe();
  }

  public onBlur(): void {
    const previous = this.value;
    this.value = this._time.fromHuman(this.innerValue);
    if (!this.value) {
      return this.control.setValue(previous);
    }

    this.control.setValue(this.value);
  }

  private _initControl(): void {
    this._subscription?.unsubscribe();
    this._subscription = this.valueChanges
      .pipe(startWith(this.value))
      .subscribe((value) => {
        this.innerValue = this._time.toHuman(value);
        this.detectChanges();
      });
  }
}
