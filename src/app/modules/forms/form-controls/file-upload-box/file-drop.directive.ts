import { Directive, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[fileDrop]',
})
export class FileDropDirective {
  @HostBinding('class.file-over')
  private fileOver: boolean;

  @HostListener('dragover', ['$event'])
  private OnDragOver(event: DragEvent): void {
    event.preventDefault();
    event.stopPropagation();
    this.fileOver = true;
  }

  @HostListener('dragleave', ['$event'])
  private OnDragLeave(event: DragEvent): void {
    event.preventDefault();
    event.stopPropagation();
    this.fileOver = false;
  }

  @HostListener('drop', ['$event'])
  private OnDrop(event: DragEvent): void {
    event.preventDefault();
    event.stopPropagation();
    this.fileOver = false;
  }
}
