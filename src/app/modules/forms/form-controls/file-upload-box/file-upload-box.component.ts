import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  ViewEncapsulation,
} from '@angular/core';

import { BrowserUtils, removeArrayItem } from '@stafr/core';
import { extensionUtil } from '@stafr/modules/utils';

import { CreateAccessorFor, FormControlBase } from '../form-control';

const ONE_MB_IN_BYTES = 1048576;

export interface IFileUploadBoxValue<T> {
  files: File[];
  attachments: T[];
  prevAttachments: T[];
}

@Component({
  selector: 'file-upload-box',
  templateUrl: './file-upload-box.component.html',
  styleUrls: ['./file-upload-box.component.scss'],
  providers: CreateAccessorFor(FileUploadBoxComponent),
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class FileUploadBoxComponent extends FormControlBase<
  IFileUploadBoxValue<any>
> {
  public error: string;

  @Input()
  public maxAmount: number = 10;

  @Input()
  public maxSize: number = 10;

  @Input()
  public format: string[];

  @Input()
  public attachmentRenderKey: string;

  constructor(private _browser: BrowserUtils, cdr: ChangeDetectorRef) {
    super(cdr);
  }

  public browse(): void {
    const format = this.format?.map((el) => '.' + el)?.join(',') || '/';
    this._browser.browseFile(format, true).then((res) => this.setFiles(res));
  }

  public async setFiles(files: FileList): Promise<void> {
    for (let i = 0; i < files.length; i++) {
      await this._validateFile(files.item(i))
        .then((result) => {
          this.error = null;
          this.value = { ...this.value, files: [...this.value.files, result] };
        })
        .catch((error) => (this.error = error));
    }
    this.detectChanges();
  }

  public onFileDelete(idx: number): void {
    this.value = {
      ...this.value,
      files: removeArrayItem(this.value.files, idx),
    };
    this.error = null;
  }

  public onAttachmentDelete(idx: number): void {
    this.value = {
      ...this.value,
      attachments: removeArrayItem(this.value.attachments, idx),
    };
    this.error = null;
  }

  public get formStyle(): Record<string, boolean> {
    return {
      ...super.formStyle,
      error: super.formStyle.error || !!this.error,
    };
  }

  private _validateFile(file: File): Promise<File> {
    return new Promise((resolve, reject) => {
      if (this.value.files.length + 1 > this.maxAmount) {
        reject('modules.file-upload-box.error.maxAmount');
      }

      if (
        this.format?.length &&
        !this.format.includes(extensionUtil(file.name))
      ) {
        reject('modules.file-upload-box.error.format');
      }

      if (file.size > this.maxSize * ONE_MB_IN_BYTES) {
        reject('modules.file-upload-box.error.size');
      }

      resolve(file);
    });
  }
}
