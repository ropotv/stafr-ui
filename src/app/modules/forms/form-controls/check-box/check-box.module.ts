import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { ClickableUtilsModule } from '@stafr/modules/utils';
import { CheckBoxComponent } from './check-box.component';

@NgModule({
  imports: [CommonModule, ClickableUtilsModule, ReactiveFormsModule],
  declarations: [CheckBoxComponent],
  exports: [CheckBoxComponent],
})
export class CheckBoxModule {}
