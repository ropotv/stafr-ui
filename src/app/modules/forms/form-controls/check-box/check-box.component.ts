import {
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';

import { CreateAccessorFor, FormControlBase } from '../form-control';

@Component({
  selector: 'check-box',
  templateUrl: './check-box.component.html',
  styleUrls: ['./check-box.component.scss'],
  providers: CreateAccessorFor(CheckBoxComponent),
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CheckBoxComponent extends FormControlBase<boolean> {
  public get formStyle(): Record<string, boolean> {
    return { ...super.formStyle, checked: this.value };
  }
}
