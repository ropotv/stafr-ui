import {
  ChangeDetectorRef,
  Directive,
  EventEmitter,
  forwardRef,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';
import { Observable, Subject, Subscription } from 'rxjs';
import { randomString } from '@stafr/core';

import { FormStateType } from '../form';

@Directive()
export class FormControlBase<T = any> implements OnChanges, OnInit, OnDestroy {
  private _subs: Subscription;
  private _detectChanges = new Subject<void>();
  private _onChanges = new Subject<void>();

  public focused: boolean;
  public state: FormStateType;
  public name = randomString(16);

  @Input()
  public disabled: boolean;

  @Input()
  public control: AbstractControl = new FormControl();

  @Input()
  public theme: 'outlined' | 'filled' = 'outlined';

  @Input()
  public set value(value: T) {
    if (this.control.value !== value) {
      this.control.setValue(value);
    }
  }

  public get value(): T {
    return this.control.value;
  }

  @Output()
  public valueChanged = new EventEmitter<T>();

  public get valueChanges(): Observable<T> {
    return this.control.valueChanges;
  }

  constructor(private _cdr: ChangeDetectorRef) {}

  public ngOnInit(): void {
    this._initSubs();
  }

  public ngOnChanges(): void {
    this._initSubs();
    this._onChanges.next();
  }

  public ngOnDestroy(): void {
    this._subs?.unsubscribe();
  }

  public detectChanges(): void {
    this._detectChanges.next();
    this._cdr.detectChanges();
  }

  public get onChanges$(): Observable<void> {
    return this._onChanges.asObservable();
  }

  public get onDetectChanges$(): Observable<void> {
    return this._detectChanges.asObservable();
  }

  public get formStyle(): Record<string, boolean> {
    return {
      [this.theme]: true,
      disabled: this.disabled,
      focused: this.focused,
      error: this.state === 'error',
      warning: this.state === 'warning',
      success: this.state === 'success',
    };
  }

  private _initSubs(): void {
    this._subs?.unsubscribe();
    this._subs = this.valueChanges.subscribe((val) =>
      this.valueChanged.emit(val)
    );
  }
}

export function CreateAccessorFor<T = FormControlBase>(control: T): any {
  return [
    {
      provide: FormControlBase,
      useExisting: forwardRef(() => control),
      multi: true,
    },
  ];
}
