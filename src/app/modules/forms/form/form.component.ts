import {
  Component,
  ContentChildren,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  QueryList,
  ViewChild,
} from '@angular/core';
import { FormGroup } from '@angular/forms';

import { Subscribable } from '@stafr/core';
import { AppEvents } from '@stafr/app.events';

import { FormGroupComponent } from '../form-group';
import { IForm } from './form.interface';

@Component({
  selector: 'stafr-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent extends Subscribable implements IForm {
  @Input()
  public form: FormGroup;

  @Output()
  public submitted = new EventEmitter<any>();

  @ViewChild('submitter')
  private _submitter: ElementRef<HTMLButtonElement>;

  @ContentChildren(FormGroupComponent, { descendants: true })
  private _groups: QueryList<FormGroupComponent>;

  constructor(event: AppEvents) {
    super();
    this.subscription.add(
      event.onChange('http-error').subscribe(() => this.unLock())
    );
  }

  public submit(): void {
    this._submitter.nativeElement.click();
  }

  public onSubmit(): void {
    this._groups.forEach((g) => {
      g.markAsSubmitted(true);
      g.component.control.markAsTouched();
      g.detectChanges();
    });

    if (this.form.valid) {
      this.lock();
      this.submitted.emit(this.form.getRawValue());
    }
  }

  public markAsSubmitted(value: boolean): void {
    this._groups.forEach((g) => {
      g.markAsSubmitted(value);
      g.detectChanges();
    });
  }

  public lock(): void {
    this._groups.forEach((g) => {
      g.component.disabled = true;
      g.detectChanges();
    });
  }

  public unLock(): void {
    this._groups.forEach((g) => {
      g.component.disabled = false;
      g.detectChanges();
    });
  }
}
