import { FormGroup, ValidatorFn } from '@angular/forms';

export interface IForm {
  form: FormGroup;

  submit(): void;

  lock(): void;

  unLock(): void;
}

export type FormStateType = 'warning' | 'error' | 'success';

export interface IFormValidator {
  validator: ValidatorFn;
  message: string;
}
