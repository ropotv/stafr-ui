import {
  Directive,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { FormGroup } from '@angular/forms';

import { FormComponent } from './form';

@Directive()
export class FormSection<T> {
  @ViewChild(FormComponent)
  private _form: FormComponent;

  @Input()
  public form: FormGroup;

  @Output()
  public submitted = new EventEmitter<T>();

  public submit(): void {
    this._form.submit();
  }

  public markAsSubmitted(value: boolean): void {
    this._form.markAsSubmitted(value);
  }

  public unLock(): void {
    this._form.unLock();
  }

  public lock(): void {
    this._form.lock();
  }

  public reset(): void {
    this.form?.reset();
  }

  public get formValue(): T {
    return this.form?.value;
  }
}
