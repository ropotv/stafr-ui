import {
  AfterContentInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChild,
  Input,
  OnChanges,
  OnDestroy,
  ViewEncapsulation,
} from '@angular/core';
import { BehaviorSubject, merge, ReplaySubject, Subscription } from 'rxjs';
import { FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

import { TranslateUtilsService } from '@stafr/modules/utils';
import { FormControlBase } from '../form-controls';
import { FormStateType, IFormValidator } from '../form';

@Component({
  selector: 'form-group',
  templateUrl: './form-group.component.html',
  styleUrls: ['./form-group.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormGroupComponent
  implements AfterContentInit, OnChanges, OnDestroy
{
  private _onChanges$ = new ReplaySubject(1);
  private _onSubmit$ = new BehaviorSubject(false);
  private _subscription = new Subscription();

  public message: string;

  @Input()
  public label: string;

  @Input()
  public validators: IFormValidator[];

  @ContentChild(FormControlBase)
  public component: FormControlBase;

  public ngAfterContentInit(): void {
    if (!this.component) {
      throw new Error('Please add a form control inside your form group');
    }

    this._subscription.add(
      this.component.onDetectChanges$.subscribe(() => {
        this._cdr.detectChanges();
      })
    );

    if (!this.validators) return;
    this._subscription.add(
      merge(
        this._onChanges$,
        this.component.onChanges$,
        this._translateUtils.change$
      ).subscribe(() => {
        this.control.setValidators(this.validators.map((v) => v.validator));
        this.control.updateValueAndValidity({
          emitEvent: false,
          onlySelf: true,
        });

        this._subscription.add(
          merge(this.control.valueChanges, this._onSubmit$).subscribe(() => {
            const error = this.control.errors;

            if (error) {
              const validator = this.validators.find(
                (el) => error[el.validator.name]
              );

              if (this._onSubmit$.value) {
                return this.setState(
                  'error',
                  this._translateSvc.instant(validator.message)
                );
              }
            }

            this.setState(null, null);
          })
        );
      })
    );
  }

  constructor(
    private _cdr: ChangeDetectorRef,
    private _translateUtils: TranslateUtilsService,
    private _translateSvc: TranslateService
  ) {}

  public setState(state: FormStateType, message: string): void {
    this.component.state = state;
    this.message = message;
    this.detectChanges();
  }

  public ngOnChanges(): void {
    this._onChanges$.next();
  }

  public detectChanges(): void {
    this.component.detectChanges();
  }

  public markAsSubmitted(value: boolean): void {
    this._onSubmit$.next(value);
  }

  public get control(): FormControl {
    return this.component.control as FormControl;
  }

  public ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }

  public get state(): FormStateType {
    return this.component.state;
  }
}
