import { NgModule } from '@angular/core';

import { RbacDirective } from './rbac.directive';

@NgModule({
  declarations: [RbacDirective],
  exports: [RbacDirective],
})
export class RbacModule {}
