import {
  ChangeDetectorRef,
  Directive,
  Input,
  OnInit,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import { combineLatest, ReplaySubject } from 'rxjs';
import { map } from 'rxjs/operators';

import { AccountRole, Subscribable, UserRole } from '@stafr/core';
import { AppStore } from '@stafr/app.store';

@Directive({
  selector: '[rbac]',
})
export class RbacDirective extends Subscribable implements OnInit {
  private _userRoles = new ReplaySubject<UserRole[]>();
  private _accountRoles = new ReplaySubject<AccountRole[]>();

  @Input()
  public set rbac(roles: UserRole[]) {
    this._userRoles.next(roles);
  }

  @Input()
  public set rbacAccounts(roles: AccountRole[]) {
    this._accountRoles.next(roles);
  }

  constructor(
    private _vcr: ViewContainerRef,
    private _ref: TemplateRef<any>,
    private _store: AppStore,
    private _cdr: ChangeDetectorRef
  ) {
    super();
  }

  public ngOnInit(): void {
    this.subscription.add(
      combineLatest([
        this._store.state$.pipe(map((s) => s.principal)),
        this._userRoles,
        this._accountRoles,
      ]).subscribe(([principal, userRoles, accountRoles]) => {
        this._vcr.clear();

        if (!principal) {
          return;
        }
        if (!principal.userRole && userRoles?.length) {
          return;
        }
        if (
          !principal.accountRole &&
          accountRoles?.length &&
          principal.userRole !== UserRole.Admin
        ) {
          return;
        }
        if (userRoles?.length && !userRoles.includes(principal.userRole)) {
          return;
        }
        if (
          accountRoles?.length &&
          !accountRoles.includes(principal.accountRole) &&
          principal.userRole !== UserRole.Admin
        ) {
          return;
        }

        this._vcr.createEmbeddedView(this._ref);
        this._cdr.detectChanges();
      })
    );
  }
}
