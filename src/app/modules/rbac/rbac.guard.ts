import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { AppStore } from '@stafr/app.store';
import { UserRole } from '@stafr/core';

@Injectable({
  providedIn: 'root',
})
export class RbacGuard implements CanActivate {
  constructor(private _store: AppStore) {}

  public canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    const { userRoles, accountRoles } = route.data;
    return this._store.state$.pipe(
      map((state) => {
        const { principal } = state;
        if (!principal) {
          return this._reject();
        }
        if (!principal.userRole && userRoles?.length) {
          return this._reject();
        }
        if (
          !principal.accountRole &&
          accountRoles?.length &&
          principal.userRole !== UserRole.Admin
        ) {
          return this._reject();
        }
        if (userRoles?.length && !userRoles.includes(principal.userRole)) {
          return this._reject();
        }
        if (
          accountRoles?.length &&
          !accountRoles.includes(principal.accountRole) &&
          principal.userRole !== UserRole.Admin
        ) {
          return this._reject();
        }

        return this._resolve();
      })
    );
  }

  private _reject(): boolean {
    return false;
  }

  private _resolve(): boolean {
    return true;
  }
}
