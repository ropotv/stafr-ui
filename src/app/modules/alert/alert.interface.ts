import { ButtonType } from '../button';
import { IconType } from '../icon';

export interface IAlertButton {
  type: ButtonType;
  text: string;
  icon?: IconType;
  clicked(): void;
}

export interface IAlert {
  caption: string;
  message: string;
  buttons: IAlertButton[];
  closed(): void;
}
