import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ButtonModule } from '../button';
import { OverlayModule } from '../overlay';
import { AlertComponent } from './alert.component';

@NgModule({
  imports: [CommonModule, OverlayModule, ButtonModule],
  declarations: [AlertComponent],
  exports: [AlertComponent],
})
export class AlertModule {}
