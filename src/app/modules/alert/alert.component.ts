import {
  ChangeDetectionStrategy,
  Component,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';

import { OverlayDirective } from '../overlay';
import { AlertService } from './alert.service';
import { IAlert } from './alert.interface';

@Component({
  selector: 'stafr-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AlertComponent {
  public alert: IAlert;

  @ViewChild('overlay')
  private _overlay: OverlayDirective;

  constructor(private _service: AlertService) {
    this._service.onAlert$.subscribe((config) => {
      this.alert = config;
      this.alert ? this._overlay.open() : this._overlay.close();
    });
  }
}
