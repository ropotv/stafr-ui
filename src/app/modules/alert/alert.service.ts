import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { IAlert } from './alert.interface';

@Injectable({ providedIn: 'root' })
export class AlertService {
  public onAlert$ = new Subject<IAlert>();

  public open(config: IAlert): void {
    this.onAlert$.next({ ...config });
  }

  public close(): void {
    this.onAlert$.next(null);
  }
}
