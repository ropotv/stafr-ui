import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewEncapsulation,
} from '@angular/core';

import { IconsMap, IconType } from './icons.map';

@Component({
  selector: 'stafr-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IconComponent {
  public map = IconsMap;

  @Input()
  public icon: IconType;

  @Input()
  public size = 16;
}
