import { NgModule } from '@angular/core';

import { SafeHtmlUtilsModule } from '../utils';
import { IconComponent } from './icon.component';

@NgModule({
  declarations: [IconComponent],
  exports: [IconComponent],
  imports: [SafeHtmlUtilsModule],
})
export class IconModule {}
