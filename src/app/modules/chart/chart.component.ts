import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';

import { IChartBar } from './chart.interface';

@Component({
  selector: 'stafr-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChartComponent implements AfterViewInit {
  public bars: IChartBar[];

  @Input()
  public chartHeight: number = 100;

  @Input()
  public barWidth: number = 30;

  @Input()
  public scrollToEnd: boolean;

  @Input('bars')
  public set _bars(value: IChartBar[]) {
    if (!value) {
      return;
    }
    const max = Math.max(
      ...value.map((b) =>
        b.values.map((v) => v.value).reduce((a1, a2) => a1 + a2, 0)
      )
    );

    this.bars = value.map((el) => {
      const bar = { ...el };
      bar.values
        .filter((v) => v.value)
        .map((bv) => {
          bv.value = bv.value ? `${(bv.value * 100) / max}%` : (null as any);
        });
      return bar;
    });
  }

  @ViewChild('chartBars')
  private _chartBars: ElementRef<HTMLElement>;

  public ngAfterViewInit(): void {
    if (this.scrollToEnd) {
      const chartBars = this._chartBars.nativeElement;
      chartBars.scrollLeft = chartBars.scrollWidth;
    }
  }
}
