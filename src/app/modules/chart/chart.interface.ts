export interface IChartBar {
  name: string;
  values: IChartBarValue[];
}

export interface IChartBarValue {
  color: string;
  value: number;
  valueString: string;
}
