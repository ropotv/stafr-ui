import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TooltipModule } from '../tooltip';

import { ChartComponent } from './chart.component';

@NgModule({
  imports: [CommonModule, TooltipModule],
  declarations: [ChartComponent],
  exports: [ChartComponent],
})
export class ChartModule {}
