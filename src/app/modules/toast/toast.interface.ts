export interface IToast {
  id: number;
  type: ToastType;
  caption: string;
  messages: string[];
}

export type ToastType = 'warning' | 'error' | 'info' | 'success';
