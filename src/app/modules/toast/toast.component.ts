import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewEncapsulation,
} from '@angular/core';

import { IToast } from './toast.interface';
import { ToastService } from './toast.service';
import { ToastAnimations } from './toast.animations';

@Component({
  selector: 'stafr-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.scss'],
  animations: ToastAnimations,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToastComponent {
  public toasts: IToast[] = [];
  public gap = 24;

  constructor(toastSvc: ToastService, private _cdr: ChangeDetectorRef) {
    toastSvc.onToast.subscribe((toast) => {
      this.toasts.unshift(toast);
      this._cdr.detectChanges();
      setTimeout(() => this.removeToast(toast.id), 5000);
    });
  }

  public removeToast(id: number): void {
    this.toasts = this.toasts.filter((t) => t.id !== id);
    this._cdr.detectChanges();
  }
}
