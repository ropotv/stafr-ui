import { animate, style, transition, trigger } from '@angular/animations';

const animation = '0.35s linear';
const startState = { opacity: 1, marginTop: '0px' };
const endState = { opacity: 0, marginTop: '-{{toastHeight}}px' };

export const ToastAnimations = [
  trigger('toastTransition', [
    transition(
      ':enter',
      [style(endState), animate(animation, style(startState))],
      { params: { toastHeight: 0 } }
    ),
    transition(
      ':leave',
      [style(startState), animate(animation, style(endState))],
      { params: { toastHeight: 0 } }
    ),
  ]),
];
