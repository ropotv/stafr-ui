import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IconModule } from '../icon';
import { ButtonModule } from '../button';
import { ToastComponent } from './toast.component';

@NgModule({
  imports: [CommonModule, IconModule, ButtonModule],
  declarations: [ToastComponent],
  exports: [ToastComponent],
})
export class ToastModule {}
