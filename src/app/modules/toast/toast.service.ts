import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

import { IToast } from './toast.interface';

@Injectable({
  providedIn: 'root',
})
export class ToastService {
  public static id: number = 0;
  private _toast = new Subject<IToast>();

  public get onToast(): Observable<IToast> {
    return this._toast.asObservable();
  }

  public success(caption: string, ...messages: string[]): void {
    this._toast.next({
      type: 'success',
      caption,
      messages,
      id: ToastService.id++,
    });
  }

  public error(caption: string, ...messages: string[]): void {
    this._toast.next({
      type: 'error',
      caption,
      messages,
      id: ToastService.id++,
    });
  }

  public warning(caption: string, ...messages: string[]): void {
    this._toast.next({
      type: 'warning',
      caption,
      messages,
      id: ToastService.id++,
    });
  }

  public info(caption: string, ...messages: string[]): void {
    this._toast.next({
      type: 'info',
      caption,
      messages,
      id: ToastService.id++,
    });
  }
}
