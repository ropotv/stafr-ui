import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewEncapsulation,
} from '@angular/core';

const ITEMS_SHOWN = 10;

@Component({
  selector: 'table-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TablePaginationComponent implements OnChanges {
  public totalItems: number;
  public items: number[] = [];

  @Input()
  public current: number;

  @Input()
  public total: number;

  @Input()
  public size: number;

  @Output()
  public onPaginationChange = new EventEmitter<number>();

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.total || changes.size || changes.current) {
      this.totalItems = Math.ceil(this.total / this.size);
      const items = Array.from(Array(this.totalItems).keys(), (n) => n + 1);
      const index = this.current - 1;
      const isEnd = index + ITEMS_SHOWN / 2 > items.length;
      const isStart = index - ITEMS_SHOWN / 2 < 0;

      const start = isEnd
        ? items.length - ITEMS_SHOWN
        : isStart
        ? 0
        : index - ITEMS_SHOWN / 2;
      const end = isStart
        ? ITEMS_SHOWN
        : isEnd
        ? items.length
        : index + ITEMS_SHOWN / 2;
      this.items = items.slice(start, end);
    }
  }
}
