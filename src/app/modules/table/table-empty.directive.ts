import { Directive, TemplateRef } from '@angular/core';

@Directive({ selector: '[tableEmpty]' })
export class TableEmptyDirective {
  constructor(public template: TemplateRef<any>) {}
}
