import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewEncapsulation,
} from '@angular/core';

@Component({
  selector: 'table-pager',
  templateUrl: './pager.component.html',
  styleUrls: ['./pager.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TablePagerComponent {
  @Input()
  public options: number[];

  @Input()
  public current: number;

  @Input()
  public total: number;

  @Output()
  public onPagerChange = new EventEmitter<number>();
}
