import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewEncapsulation,
} from '@angular/core';

import { TimeService } from '@stafr/modules';

import { ITableFilterEvent, TableFilterOperand } from '../filter.interface';

@Component({
  selector: 'table-time-filter',
  templateUrl: './time-filter.component.html',
  styleUrls: ['./time-filter.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TableTimeFilterComponent implements OnChanges {
  public innerValue: string;

  @Input()
  public operand: TableFilterOperand = 'eq';

  @Input()
  public value: number;

  @Output()
  public onFilterChange = new EventEmitter<ITableFilterEvent>();

  constructor(private _time: TimeService) {}

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.value) {
      this.innerValue = this._time.toHuman(this.value);
    }
  }

  public onValueChange(value: string): void {
    this.innerValue = value;
    this.value = this._time.fromHuman(this.innerValue);

    this.onFilterChange.emit({
      type: 'time',
      value: this.value,
      operand: this.operand,
    });
  }

  public onOperandChange(): void {
    switch (this.operand) {
      case 'eq':
        this.operand = 'lte';
        break;
      case 'lte':
        this.operand = 'gte';
        break;
      case 'gte':
        this.operand = 'neq';
        break;
      case 'neq':
        this.operand = 'eq';
        break;
    }
  }
}
