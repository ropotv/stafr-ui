import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewEncapsulation,
} from '@angular/core';
import { ITableFilterEvent } from '../filter.interface';

@Component({
  selector: 'table-string-filter',
  templateUrl: './string-filter.component.html',
  styleUrls: ['./string-filter.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TableStringFilterComponent {
  @Input()
  public value: string;

  @Output()
  public onFilterChange = new EventEmitter<ITableFilterEvent>();

  public onValueChange(value: string): void {
    this.value = value;
    this.onFilterChange.emit({
      type: 'string',
      value: this.value,
      operand: 'like',
    });
  }
}
