export type TableFilterType = 'string' | 'number' | 'date' | 'select' | 'time';

export type TableFilterOperand = 'ieq' | 'eq' | 'neq' | 'like' | 'lte' | 'gte';

export interface ITableFilterEvent {
  operand: TableFilterOperand;
  value: any;
  type: TableFilterType;
}
