import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewEncapsulation,
} from '@angular/core';
import { Subject } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

import { PerforateUtilsService } from '@stafr/modules/utils/perforate';
import { ITableFilterEvent } from '../filter.interface';

@Component({
  selector: 'table-select-filter',
  templateUrl: './select-filter.component.html',
  styleUrls: ['./select-filter.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TableSelectFilterComponent {
  public opened: boolean;
  public term$ = new Subject<string>();
  public searched$ = this.term$.pipe(
    startWith(''),
    map((term) =>
      this.items?.filter((item) =>
        this._perforate
          .transform(item, this.renderKey)
          ?.toLowerCase()
          ?.includes(term?.toLowerCase())
      )
    )
  );

  @Input()
  public value: any[] = [];

  @Output()
  public onFilterChange = new EventEmitter<ITableFilterEvent>();

  @Input()
  public valueKey: string;

  @Input()
  public renderKey: string;

  @Input()
  public items: any[];

  public findFn = (item) => (value) =>
    value === this._perforate.transform(item, this.valueKey);

  constructor(private _perforate: PerforateUtilsService) {}

  public onCheckboxChange(item: any, checked: boolean): void {
    this.value = this.value?.filter(
      (val) => this._perforate.transform(item, this.valueKey) !== val
    );
    if (checked) {
      this.value.push(this._perforate.transform(item, this.valueKey));
    }
  }

  public onValueChange(): void {
    this.onFilterChange.emit({
      type: 'select',
      value: this.value,
      operand: 'eq',
    });
  }
}
