import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewEncapsulation,
} from '@angular/core';

import {
  ITableFilterEvent,
  TableFilterOperand,
  TableFilterType,
} from './filter.interface';

@Component({
  selector: 'table-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TableFilterComponent {
  @Input()
  public type: TableFilterType;

  @Input()
  public value: any;

  @Input()
  public operand: TableFilterOperand;

  @Input()
  public items: any[];

  @Input()
  public valueKey: string;

  @Input()
  public renderKey: string;

  @Output()
  public onFilterChange = new EventEmitter<ITableFilterEvent>();
}
