import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewEncapsulation,
} from '@angular/core';
import {
  ITableFilterEvent,
  TableFilterOperand,
} from '@stafr/modules/table/filter';

@Component({
  selector: 'table-date-filter',
  templateUrl: './date-filter.component.html',
  styleUrls: ['./date-filter.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TableDateFilterComponent {
  @Input()
  public operand: TableFilterOperand = 'eq';

  @Input()
  public value: Date;

  @Output()
  public onFilterChange = new EventEmitter<ITableFilterEvent>();

  public onValueChange(value: string): void {
    this.value = value ? new Date(value) : null;
    this.onFilterChange.emit({
      type: 'date',
      value: this.value,
      operand: this.operand,
    });
  }

  public onOperandChange(): void {
    switch (this.operand) {
      case 'eq':
        this.operand = 'lte';
        break;
      case 'lte':
        this.operand = 'gte';
        break;
      case 'gte':
        this.operand = 'neq';
        break;
      case 'neq':
        this.operand = 'eq';
        break;
    }
  }
}
