import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewEncapsulation,
} from '@angular/core';
import { ITableFilterEvent, TableFilterOperand } from '../filter.interface';

@Component({
  selector: 'table-number-filter',
  templateUrl: './number-filter.component.html',
  styleUrls: ['./number-filter.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TableNumberFilterComponent {
  @Input()
  public operand: TableFilterOperand = 'eq';

  @Input()
  public value: number;

  @Output()
  public onFilterChange = new EventEmitter<ITableFilterEvent>();

  public onValueChange(value: string): void {
    this.value = value ? Number(value) : null;
    this.onFilterChange.emit({
      type: 'number',
      value: this.value,
      operand: this.operand,
    });
  }

  public onOperandChange(): void {
    switch (this.operand) {
      case 'eq':
        this.operand = 'lte';
        break;
      case 'lte':
        this.operand = 'gte';
        break;
      case 'gte':
        this.operand = 'neq';
        break;
      case 'neq':
        this.operand = 'eq';
        break;
    }
  }
}
