import {
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  ContentChildren,
  EventEmitter,
  Input,
  OnInit,
  Output,
  QueryList,
  ViewEncapsulation,
} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { startWith, switchMap } from 'rxjs/operators';

import { Paginated } from '@stafr/core';

import { TableHeadDirective } from './table-head.directive';
import { TableColumnDirective } from './table-column.directive';
import { TableActionDirective } from './table-action.directive';
import { TableSource } from './table.source';
import { ITableRowClick } from './table.interface';
import { TableEmptyDirective } from './table-empty.directive';
import { ITableFilterEvent } from './filter';

@Component({
  selector: 'stafr-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class TableComponent implements OnInit {
  private _refresh = new Subject();

  public pagerOptions = [10, 25, 50];
  public size = 10;
  public page = 1;
  public order: string[] = [];
  public filter: string[] = [];

  public vm$: Observable<Paginated<any>>;

  @Input()
  public source: TableSource<any>;

  @Output()
  public onRowClick = new EventEmitter<ITableRowClick>();

  @ContentChildren(TableColumnDirective)
  public columns: QueryList<TableColumnDirective>;

  @ContentChildren(TableActionDirective)
  public actions: QueryList<TableActionDirective>;

  @ContentChild(TableEmptyDirective)
  public emptyTpl: TableEmptyDirective;

  public ngOnInit(): void {
    this.vm$ = this._refresh.pipe(
      startWith(''),
      switchMap(() =>
        this.source({
          page: this.page - 1,
          size: this.size,
          order: this.order.join(';'),
          filter: this.filter.join(';'),
        })
      )
    );
  }

  public refresh(): void {
    this._refresh.next();
  }

  public onPageSizeChange(value: number): void {
    this.size = value;
    this.page = 1;
    this.refresh();
  }

  public onPageNumberChange(value: number): void {
    this.page = value;
    this.refresh();
  }

  public onColumnSort(head: TableHeadDirective): void {
    switch (head.tableHeadSortState) {
      case 'ASC':
        head.tableHeadSortState = null;
        break;
      case 'DESC':
        head.tableHeadSortState = 'ASC';
        break;
      default:
        head.tableHeadSortState = 'DESC';
        break;
    }
    this.order = this.order.filter(
      (el) => !el.startsWith(head.tableHeadSortBy)
    );
    if (head.tableHeadSortState) {
      this.order.push(`${head.tableHeadSortBy}~${head.tableHeadSortState}~AND`);
    }
    this.refresh();
  }

  public onFilter(head: TableHeadDirective, event: ITableFilterEvent): void {
    head.tableHeadFilterValue = event.value;
    head.tableHeadFilterOperand = event.operand;

    const field = `${head.tableHeadFilterBy}`;

    this.page = 1;
    this.filter = this.filter.filter((el) => !el.startsWith(field));

    if (event.value) {
      switch (event.type) {
        case 'date':
          this.filter.push(
            `${field}~${event.operand}~${event.value.toISOString()}~AND`
          );
          break;
        case 'time':
        case 'number':
        case 'string':
          this.filter.push(`${field}~${event.operand}~${event.value}~AND`);
          break;
        case 'select':
          for (const value of event.value) {
            if (value) {
              this.filter.push(`${field}~${event.operand}~${value}~OR`);
            }
          }
          break;
      }
    }

    this.refresh();
  }

  public get hasFilters(): boolean {
    return this.filter.length > 0;
  }
}
