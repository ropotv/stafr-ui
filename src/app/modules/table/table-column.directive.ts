import { ContentChild, Directive } from '@angular/core';

import { TableCellDirective } from './table-cell.directive';
import { TableHeadDirective } from './table-head.directive';

@Directive({ selector: '[tableColumn]' })
export class TableColumnDirective {
  @ContentChild(TableHeadDirective)
  public head: TableHeadDirective;

  @ContentChild(TableCellDirective)
  public cell: TableCellDirective;
}
