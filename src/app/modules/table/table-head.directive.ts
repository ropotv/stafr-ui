import { Directive, Input, TemplateRef } from '@angular/core';

import { TableSort } from './table.interface';
import { TableFilterOperand, TableFilterType } from './filter';

@Directive({ selector: '[tableHead]' })
export class TableHeadDirective {
  @Input()
  public tableHead: string;

  @Input()
  public tableHeadSortBy: string;

  @Input()
  public tableHeadSortState: TableSort;

  @Input()
  public tableHeadFilterBy: string;

  @Input()
  public tableHeadFilterType: TableFilterType;

  @Input()
  public tableHeadFilterValue: any;

  @Input()
  public tableHeadFilterOperand: TableFilterOperand;

  @Input()
  public tableHeadFilterItems: any[];

  @Input()
  public tableHeadFilterRenderKey: string;

  @Input()
  public tableHeadFilterValueKey: string;

  constructor(public template: TemplateRef<any>) {}
}
