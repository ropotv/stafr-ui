export interface ITableRowClick<T = any> {
  data: T;
  index: number;
  event: MouseEvent;
}

export type TableSort = 'ASC' | 'DESC' | null;
