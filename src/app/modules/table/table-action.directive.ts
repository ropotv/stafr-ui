import { Directive, TemplateRef } from '@angular/core';

@Directive({ selector: '[tableAction]' })
export class TableActionDirective {
  constructor(public template: TemplateRef<any>) {}
}
