import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewEncapsulation,
} from '@angular/core';

@Component({
  selector: 'table-position',
  templateUrl: './position.component.html',
  styleUrls: ['./position.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TablePositionComponent {
  @Input()
  public current: number;

  @Input()
  public total: number;

  @Input()
  public size: number;
}
