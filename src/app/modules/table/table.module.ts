import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';

import { IconModule } from '../icon';
import { ButtonModule } from '../button';
import { OverlayModule } from '../overlay';
import { PerforateUtilsModule } from '../utils';

import { TableComponent } from './table.component';
import { TableCellDirective } from './table-cell.directive';
import { TableColumnDirective } from './table-column.directive';
import { TableHeadDirective } from './table-head.directive';
import { TableActionDirective } from './table-action.directive';
import { TablePagerComponent } from './pager';
import { TablePaginationComponent } from './pagination';
import { TablePositionComponent } from './position';
import {
  TableDateFilterComponent,
  TableFilterComponent,
  TableNumberFilterComponent,
  TableSelectFilterComponent,
  TableStringFilterComponent,
  TableTimeFilterComponent,
} from './filter';
import { TableEmptyDirective } from './table-empty.directive';

@NgModule({
  imports: [
    CommonModule,
    IconModule,
    TranslateModule,
    ButtonModule,
    OverlayModule,
    FormsModule,
    PerforateUtilsModule,
  ],
  declarations: [
    TableComponent,
    TableCellDirective,
    TableColumnDirective,
    TableHeadDirective,
    TableActionDirective,
    TablePagerComponent,
    TablePaginationComponent,
    TableEmptyDirective,
    TableFilterComponent,
    TableDateFilterComponent,
    TableNumberFilterComponent,
    TableSelectFilterComponent,
    TableStringFilterComponent,
    TableTimeFilterComponent,
    TablePositionComponent,
  ],
  exports: [
    TableComponent,
    TableCellDirective,
    TableColumnDirective,
    TableHeadDirective,
    TableActionDirective,
    TableEmptyDirective,
    TableFilterComponent,
  ],
})
export class TableModule {}
