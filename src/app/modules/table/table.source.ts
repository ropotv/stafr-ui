import { Observable } from 'rxjs';

import { IQuery, Paginated } from '@stafr/core';

export type TableSource<T> = (query: IQuery) => Observable<Paginated<T>>;
