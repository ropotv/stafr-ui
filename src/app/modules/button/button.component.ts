import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  HostBinding,
  Input,
  Output,
  ViewEncapsulation,
} from '@angular/core';

import { IconType } from '../icon';
import { ButtonType } from './button.type';

@Component({
  selector: 'stafr-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ButtonComponent {
  @Input()
  public title: string;

  @Input()
  public text: string;

  @Input()
  public type: ButtonType = 'white';

  @Input()
  public disabled: boolean;

  @Input()
  public fluid: boolean;

  @Input()
  public rounded: boolean;

  @Input()
  public icon: IconType;

  @Input()
  public iconSize = 16;

  @Input()
  public size: 'normal' | 'small' = 'normal';

  @Output()
  public clicked = new EventEmitter<MouseEvent>();

  @HostBinding('style.width')
  public get width(): string {
    return this.fluid ? '100%' : 'auto';
  }
}
