export type ButtonType =
  | 'primary'
  | 'secondary'
  | 'white'
  | 'warn'
  | 'gray-outlined'
  | 'secondary-outlined'
  | 'warn-outlined';
