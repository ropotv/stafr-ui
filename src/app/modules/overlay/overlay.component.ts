import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  ViewEncapsulation,
} from '@angular/core';
import { fromEvent } from 'rxjs';
import { filter } from 'rxjs/operators';

import { KeyCodeType } from '@stafr/modules/utils';
import { Subscribable } from '@stafr/core';

import { OverlayService } from './overlay.service';
import { OverlayAnimations } from './overlay.animations';
import { IOverlay } from './overlay.interface';

@Component({
  selector: 'stafr-overlay',
  templateUrl: './overlay.component.html',
  styleUrls: ['./overlay.component.scss'],
  animations: OverlayAnimations,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OverlayComponent extends Subscribable implements OnDestroy {
  public overlays: IOverlay[] = [];

  constructor(
    private _service: OverlayService,
    private _cdr: ChangeDetectorRef
  ) {
    super();
    this.subscription
      .add(
        fromEvent(document.body, 'keydown')
          .pipe(
            filter(
              (event: KeyboardEvent) => event.keyCode === KeyCodeType.Escape
            )
          )
          .subscribe(() => this.closeLast())
      )
      .add(
        this._service.onOpen$.subscribe((overlay) => {
          this.overlays.push(overlay);
          this._cdr.detectChanges();
        })
      )
      .add(
        this._service.onClose$.subscribe((id) => {
          const overlay = this.overlays.find((el) => el.id === id);
          const { trigger } = overlay;
          trigger?.focus({ preventScroll: true });
          this.overlays = this.overlays.filter((el) => el.id !== id);
          this._cdr.detectChanges();
        })
      );
  }

  public ngOnDestroy(): void {
    this.overlays = [];
    super.ngOnDestroy();
  }

  public closeLast(): void {
    const last = this.overlays.last();
    if (last) {
      this.close(last.id);
    }
  }

  public close(id: number): void {
    this._service.close(id);
  }
}
