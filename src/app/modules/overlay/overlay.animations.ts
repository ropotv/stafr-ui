import { animate, style, transition, trigger } from '@angular/animations';

const startStyle = { opacity: '0' };
const endStyle = { opacity: '1' };
const animation = '0.2s ease-in-out';

export const OverlayAnimations = [
  trigger('overlayWrapper', [
    transition(':enter', [
      style(startStyle),
      animate(animation, style(endStyle)),
    ]),
    transition(':leave', [
      style(endStyle),
      animate(animation, style(startStyle)),
    ]),
  ]),
];
