import { Directive, Input, Output, TemplateRef } from '@angular/core';
import { filter } from 'rxjs/operators';

import { IPositionFallback, PositionX, PositionY } from '@stafr/modules/utils';

import { OverlayService } from './overlay.service';

@Directive({
  selector: '[overlay]',
  exportAs: 'overlay',
})
export class OverlayDirective {
  private _overlayId: number;

  @Input()
  public overlayX: PositionX;

  @Input()
  public overlayY: PositionY;

  @Input()
  public originX: PositionX;

  @Input()
  public originY: PositionY;

  @Input()
  public offsetX: number;

  @Input()
  public offsetY: number = 0;

  @Input()
  public overlayClass: string;

  @Input()
  public overlayClose: boolean = true;

  @Input()
  public fallback: IPositionFallback;

  @Output()
  public closedOverlay = this._service.onClose$.pipe(
    filter((id) => id === this._overlayId)
  );

  constructor(
    private _template: TemplateRef<any>,
    private _service: OverlayService
  ) {}

  public open(trigger?: HTMLElement): void {
    this._overlayId = this._service.open({
      trigger,
      template: this._template,
      position: {
        originX: this.originX,
        originY: this.originY,
        overlayX: this.overlayX,
        overlayY: this.overlayY,
        offsetX: Number(this.offsetX),
        offsetY: Number(this.offsetY),
      },
      fallback: this.fallback,
      overlayClass: this.overlayClass,
      overlayClose: this.overlayClose,
    });
  }

  public close(): void {
    this._service.close(this._overlayId);
  }
}
