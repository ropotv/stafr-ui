import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PositionUtilsModule } from '@stafr/modules/utils';

import { OverlayComponent } from './overlay.component';
import { OverlayDirective } from './overlay.directive';

@NgModule({
  imports: [CommonModule, PositionUtilsModule],
  declarations: [OverlayComponent, OverlayDirective],
  exports: [OverlayComponent, OverlayDirective],
})
export class OverlayModule {}
