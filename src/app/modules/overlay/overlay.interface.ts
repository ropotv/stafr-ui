import { TemplateRef } from '@angular/core';

import { IPositionFallback, IPositionStrategy } from '@stafr/modules/utils';

export interface IOverlay {
  id?: number;
  trigger?: HTMLElement;
  position?: IPositionStrategy;
  fallback?: IPositionFallback;
  template: TemplateRef<any>;
  overlayClass?: string;
  overlayClose?: boolean;
}
