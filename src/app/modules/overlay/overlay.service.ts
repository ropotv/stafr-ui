import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { IOverlay } from './overlay.interface';

@Injectable({ providedIn: 'root' })
export class OverlayService {
  public static id = 0;
  public onOpen$ = new Subject<IOverlay>();
  public onClose$ = new Subject<number>();

  public open(config: IOverlay): number {
    const id = OverlayService.id++;
    this.onOpen$.next({ ...config, id });
    return id;
  }

  public close(id: number): void {
    this.onClose$.next(id);
  }
}
