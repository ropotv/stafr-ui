import { Directive, ElementRef, HostListener, Input } from '@angular/core';

import { CalculatePosition, IPositionStrategy } from '../utils/position';

const TooltipBoundsY = 120;
const TooltipBoundsX = 240;

@Directive({
  selector: '[tooltip]',
})
export class TooltipDirective {
  private _tooltipElement: HTMLSpanElement;

  @Input()
  public tooltip: string;

  @HostListener('mouseover', ['$event'])
  private _onMouseOver(e: MouseEvent): void {
    if (!this._tooltipElement) {
      this._tooltipElement = document.createElement('span');
      this._tooltipElement.className = 'stafr-tooltip';
      this._tooltipElement.style.position = 'fixed';
      this._tooltipElement.innerHTML = this.tooltip;
      this._tooltipElement.onmouseover = (_e) => _e.stopPropagation();
      this._element.appendChild(this._tooltipElement);
    }
    this._positionTooltip(e);
  }

  @HostListener('mouseleave')
  private _onMouseLeave(): void {
    this._hideTooltip();
  }

  @HostListener('window:scroll')
  private _onWindowScroll(): void {
    this._hideTooltip();
  }

  constructor(private _elRef: ElementRef) {}

  private _positionTooltip(e: MouseEvent): void {
    const target = e.target as HTMLElement;
    const bounds = target.getBoundingClientRect();
    const overflowTop = TooltipBoundsY > bounds.top;
    const overflowLeft = TooltipBoundsX > bounds.left;

    const strategy: IPositionStrategy = {
      originX: 'left',
      originY: 'top',
      overlayX: 'left',
      overlayY: 'bottom',
      offsetY: 9,
      offsetX: bounds.width / 2 - 17,
    };

    if (overflowTop) {
      strategy.overlayY = 'top';
      strategy.originY = 'bottom';
      this._tooltipElement.className = 'stafr-tooltip inverse';
    }
    if (overflowLeft) {
      strategy.overlayX = 'right';
      strategy.originX = 'right';
    }

    CalculatePosition(strategy, this._tooltipElement, target, {
      left: 300,
    });

    this._showTooltip();
  }

  private _showTooltip(): void {
    if (this._tooltipElement) {
      this._tooltipElement.hidden = false;
    }
  }

  private _hideTooltip(): void {
    if (this._tooltipElement) {
      this._tooltipElement.hidden = true;
    }
  }

  private get _element(): HTMLElement {
    return this._elRef.nativeElement;
  }
}
