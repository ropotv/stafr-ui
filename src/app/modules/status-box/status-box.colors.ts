import {
  AccountRole,
  CandidateStatus,
  ContractStatus,
  OrderStatus,
  UserRole,
} from '@stafr/core';

export const StatusBoxColors = {
  [CandidateStatus.Unavailable]: '#E72A2A',
  [CandidateStatus.Unknown]: '#656C7A',
  [CandidateStatus.Interested]: '#2BA62C',
  [CandidateStatus.InReview]: '#FF8A00',
  [CandidateStatus.Employee]: '#055ffc',
  [ContractStatus.Active]: '#055ffc',
  [ContractStatus.Closed]: '#656C7A',
  [OrderStatus.Active]: '#055ffc',
  [OrderStatus.Done]: '#2BA62C',
  [AccountRole.Admin]: '#5200FF',
  [AccountRole.Manager]: '#FF8A00',
  [AccountRole.HR]: '#2BA62C',
  [UserRole.Admin]: '#5200FF',
  [UserRole.Account]: '#2BA62C',
};

export const StatusBoxBackgrounds = {
  [CandidateStatus.Unavailable]: '#FFF8F8',
  [CandidateStatus.Unknown]: '#F8FAFF',
  [CandidateStatus.Interested]: '#EEFFEE',
  [CandidateStatus.InReview]: '#FFFAF4',
  [CandidateStatus.Employee]: '#EBF8FF',
  [ContractStatus.Active]: '#EBF8FF',
  [ContractStatus.Closed]: '#F8FAFF',
  [OrderStatus.Active]: '#EBF8FF',
  [OrderStatus.Done]: '#EEFFEE',
  [AccountRole.Admin]: '#F6F4FF',
  [AccountRole.Manager]: '#FFFAF4',
  [AccountRole.HR]: '#EEFFEE',
  [UserRole.Admin]: '#F6F4FF',
  [UserRole.Account]: '#EEFFEE',
};
