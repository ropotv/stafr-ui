import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StatusBoxComponent } from './status-box.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [CommonModule, TranslateModule],
  declarations: [StatusBoxComponent],
  exports: [StatusBoxComponent],
})
export class StatusBoxModule {}
