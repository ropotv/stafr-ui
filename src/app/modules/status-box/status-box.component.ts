import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewEncapsulation,
} from '@angular/core';

import {
  AccountRole,
  CandidateStatus,
  ContractStatus,
  OrderStatus,
  UserRole,
} from '@stafr/core';

import { StatusBoxBackgrounds, StatusBoxColors } from './status-box.colors';

@Component({
  selector: 'stafr-status-box',
  templateUrl: './status-box.component.html',
  styleUrls: ['./status-box.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StatusBoxComponent {
  public colors = StatusBoxColors;
  public backgrounds = StatusBoxBackgrounds;

  @Input()
  public status:
    | ContractStatus
    | OrderStatus
    | CandidateStatus
    | AccountRole
    | UserRole;
}
