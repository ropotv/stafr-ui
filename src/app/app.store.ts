import { Injectable } from '@angular/core';

import {
  CurrencyType,
  IAccountSettings,
  IJwtAccount,
  IJwtPrincipal,
} from '@stafr/core';
import { ComponentStore } from '@stafr/core/utils/store';
import { ExchangeRate } from '@stafr/modules';

export interface IStoreState {
  authToken: string;
  principal: IJwtPrincipal;
  accounts: IJwtAccount[];
  settings: IAccountSettings;
  exchangeRate: ExchangeRate;
  serverTimezoneOffset: number;
}

function getDefaultState(): IStoreState {
  return {
    authToken: null,
    principal: null,
    accounts: [],
    settings: null,
    exchangeRate: null,
    serverTimezoneOffset: 0,
  };
}

@Injectable({
  providedIn: 'root',
})
export class AppStore extends ComponentStore<IStoreState> {
  constructor() {
    super(getDefaultState());
  }

  public get currency(): CurrencyType {
    return this.get?.principal?.currency || CurrencyType.EUR;
  }
}
